<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\Auth\RegisterController;
use App\Http\Controllers\API\Auth\LoginController;
use App\Http\Controllers\API\RestaurantController;
use App\Http\Controllers\API\Auth\ForgotPasswordController;



/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('register', [RegisterController::class, 'register']);
Route::post('login', [LoginController::class, 'login']);

Route::post('password/email', [ForgotPasswordController::class,'forgot']);
Route::post('password/reset', [ForgotPasswordController::class ,'reset']);

Route::get('home-page-data',[RestaurantController::class,'homePageData']);
Route::get('restaurants/{city}',[RestaurantController::class,'getHotelBycity']);
Route::get('restaurants', [App\Http\Controllers\API\RestaurantController::class, 'getAllRestaurants'])->name('restaurants');
Route::get('restaurants_details/{slug}', [App\Http\Controllers\API\RestaurantController::class, 'restaurantDetails']);
Route::get('offers', [App\Http\Controllers\API\RestaurantController::class, 'offers'])->name('offers');
Route::post('order-store',[App\Http\Controllers\API\OrderController::class, 'orderStore'])->name('order-store');
Route::post('payment-success', [App\Http\Controllers\API\OrderController::class, 'paymentSuccess'])->name('payment-success');
Route::get('search-restaurants/{query}', [App\Http\Controllers\API\RestaurantController::class, 'searchRestaurants'])->name('search-restaurants/{query}');
Route::get('search-item/{query}', [App\Http\Controllers\API\RestaurantController::class, 'searchItem'])->name('search-item/{query}');
// Route::middleware('auth:api')->group( function () {
    
// });
