<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\CuisineController;
use App\Http\Controllers\Admin\CuisineTypeController;
use App\Http\Controllers\Admin\HotelController;
use App\Http\Controllers\Admin\CityController;
use App\Http\Controllers\Admin\SliderController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\OrderController;
use App\Http\Controllers\Admin\AlertController;
use App\Http\Controllers\Admin\RoleandUser\RoleController;
use App\Http\Controllers\Admin\RoleandUser\UserController;
use App\Http\Controllers\Admin\HotelOption\HotelCategoryController;
use App\Http\Controllers\Admin\HotelOption\HotelItemController;
use App\Http\Controllers\Admin\HotelOption\HotelDealsController;
use App\Http\Controllers\Admin\HotelOption\HotelGalleryController;
use App\Http\Controllers\Admin\HotelOption\HotelTimingController;
use App\Http\Controllers\Admin\HotelOption\HotelReviewController;




/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\RestaurantController::class, 'index']);

Auth::routes();
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/profile', [App\Http\Controllers\HomeController::class, 'profileDetails'])->name('profile');
Route::post('/profile-update', [App\Http\Controllers\HomeController::class, 'profileUpdate'])->name('profile-update');
Route::post('/user-update', [App\Http\Controllers\HomeController::class, 'userUpdate'])->name('user-update');
Route::get('/restaurants', [App\Http\Controllers\RestaurantController::class, 'restaurants'])->name('restaurants');
Route::get('/groceries', [App\Http\Controllers\RestaurantController::class, 'restaurants'])->name('groceries');
Route::get('/restaurants/{slug}', [App\Http\Controllers\RestaurantController::class, 'restaurantDetails'])->name('restaurants');
Route::get('/offers', [App\Http\Controllers\RestaurantController::class, 'offers'])->name('offers');
Route::get('/add-to-cart/{id}', [App\Http\Controllers\OrderController::class, 'addToCart'])->name('add-to-cart');
Route::get('/clear-all-cart', [App\Http\Controllers\OrderController::class, 'clearAllCart'])->name('clear-all-cart');
Route::post('/update-cart', [App\Http\Controllers\OrderController::class, 'updateCart'])->name('update-cart');
Route::get('/checkout', [App\Http\Controllers\OrderController::class, 'checkout'])->name('checkout');
Route::get('/my-orders', [App\Http\Controllers\HomeController::class, 'profileDetails'])->name('my-orders');
Route::post('/confirm', [App\Http\Controllers\OrderController::class, 'orderConfirm'])->name('confirm');
Route::get('/payment/{orderId}', [App\Http\Controllers\OrderController::class, 'paymentConfirm'])->name('payment');
Route::post('/payment-success', [App\Http\Controllers\OrderController::class, 'paymentSuccess'])->name('payment-success');
Route::get('/thanks', [App\Http\Controllers\OrderController::class, 'OrderThanks'])->name('thanks');

// Static pages routes
Route::get('/about-us', [App\Http\Controllers\StaticPageController::class, 'aboutUs'])->name('about-us');
Route::get('/careers', [App\Http\Controllers\StaticPageController::class, 'Careers'])->name('careers');
Route::get('/privacy', [App\Http\Controllers\StaticPageController::class, 'Privacy'])->name('privacy');
Route::get('/terms-and-conditions', [App\Http\Controllers\StaticPageController::class, 'TermsCondition'])->name('terms-and-conditions');
Route::get('/contact-us', [App\Http\Controllers\StaticPageController::class, 'ContactUs'])->name('contact-us');
Route::get('/business', [App\Http\Controllers\StaticPageController::class, 'Business'])->name('business');
Route::get('/advertise', [App\Http\Controllers\StaticPageController::class, 'Advertise'])->name('advertise');
Route::get('/faqs', [App\Http\Controllers\StaticPageController::class, 'Faqs'])->name('faqs');
Route::get('/share', [App\Http\Controllers\StaticPageController::class, 'Share'])->name('share');

// facebook url
Route::get('auth/facebook', [App\Http\Controllers\FacebookController::class, 'redirectToFacebook']);
Route::get('auth/facebook/callback', [App\Http\Controllers\FacebookController::class, 'handleFacebookCallback']);
// Google URL
Route::prefix('google')->name('google.')->group( function(){
    Route::get('login', [App\Http\Controllers\GoogleController::class, 'loginWithGoogle'])->name('login');
    Route::any('callback', [App\Http\Controllers\GoogleController::class, 'callbackFromGoogle'])->name('callback');
});

// api reset password
// Route::view('forgot_password', 'auth.reset_password')->name('password.reset');

// admin panel
Route::group(['prefix' =>'admin','middleware'=>['role:admin|superadmin','auth']],function($role){
    Route::get('console', [App\Http\Controllers\Admin\IndexController::class, 'consloeIndex']);
    Route::get('kiosk', [App\Http\Controllers\Admin\IndexController::class, 'kioskIndex']);
    Route::get('analytics', [App\Http\Controllers\Admin\AnalyticController::class, 'index']);
    Route::get('generate-hotel-bill/{id}', [App\Http\Controllers\Admin\AnalyticController::class, 'generateHotelBill']);
    Route::get('view-hotel-bill/{id}', [App\Http\Controllers\Admin\AnalyticController::class, 'viewHotelBill']);
    Route::resource('manage-cuisine',CuisineController::class);
    Route::resource('manage-cuisine-type',CuisineTypeController::class);
    Route::get('manage-cuisine-type_status/{id}/{status}',[App\Http\Controllers\Admin\CuisineTypeController::class,'status_update']);
    Route::get('manage-cuisine_status/{id}',[App\Http\Controllers\Admin\CuisineController::class,'status_update']);
    Route::resource('manage-hotel',HotelController::class);
    Route::get('manage-hotel_status/{id}/{status}',[App\Http\Controllers\Admin\HotelController::class,'status_update']);
    Route::resource('manage-city',CityController::class);
    Route::get('manage-city_status/{id}/{status}',[App\Http\Controllers\Admin\CityController::class,'status_update']);
    // hotel categories
    Route::get('manage-hotel-category_index/{id}',[App\Http\Controllers\Admin\HotelOption\HotelCategoryController::class,'index']);
    Route::get('manage-hotel-category_create/{hotel_id}',[App\Http\Controllers\Admin\HotelOption\HotelCategoryController::class,'create']);
    Route::get('manage-hotel-category_status/{id}/{status}',[App\Http\Controllers\Admin\HotelOption\HotelCategoryController::class,'status_update']);
    Route::resource('manage-hotel-category',HotelCategoryController::class);
    Route::get('manage-hotel-category_show/{hotel_id}/{id}',[App\Http\Controllers\Admin\HotelOption\HotelCategoryController::class,'show']);
    Route::get('manage-hotel-category_edit/{hotel_id}/{id}',[App\Http\Controllers\Admin\HotelOption\HotelCategoryController::class,'edit']);
    // hotel items
    Route::resource('manage-hotel-item',HotelItemController::class);
    Route::get('manage-hotel-item_status/{id}/{status}',[App\Http\Controllers\Admin\HotelOption\HotelItemController::class,'status_update']);
    Route::get('manage-hotel-item_index/{id}',[App\Http\Controllers\Admin\HotelOption\HotelItemController::class,'index']);
    Route::get('manage-hotel-item_create/{hotel_id}',[App\Http\Controllers\Admin\HotelOption\HotelItemController::class,'create']);
    Route::get('manage-hotel-item_show/{hotel_id}/{id}',[App\Http\Controllers\Admin\HotelOption\HotelItemController::class,'show']);
    Route::get('manage-hotel-item_edit/{hotel_id}/{id}',[App\Http\Controllers\Admin\HotelOption\HotelItemController::class,'edit']);
    
    // hotel delas
    Route::resource('manage-hotel-deals',HotelDealsController::class);
    Route::get('manage-hotel-deals_status/{id}/{status}',[App\Http\Controllers\Admin\HotelOption\HotelDealsController::class,'status_update']);
    Route::get('manage-hotel-deals_index/{id}',[App\Http\Controllers\Admin\HotelOption\HotelDealsController::class,'index']);
    Route::get('manage-hotel-deals_create/{hotel_id}',[App\Http\Controllers\Admin\HotelOption\HotelDealsController::class,'create']);
    Route::get('manage-hotel-deals_show/{hotel_id}/{id}',[App\Http\Controllers\Admin\HotelOption\HotelDealsController::class,'show']);
    // hotel galleries
    Route::resource('manage-hotel-gallery',HotelGalleryController::class);
    Route::get('manage-hotel-gallery_status/{id}/{status}',[App\Http\Controllers\Admin\HotelOption\HotelGalleryController::class,'status_update']);
    Route::get('manage-hotel-gallery_index/{id}',[App\Http\Controllers\Admin\HotelOption\HotelGalleryController::class,'index']);
    Route::get('manage-hotel-gallery_create/{hotel_id}',[App\Http\Controllers\Admin\HotelOption\HotelGalleryController::class,'create']);
    Route::get('manage-hotel-gallery_show/{hotel_id}/{id}',[App\Http\Controllers\Admin\HotelOption\HotelGalleryController::class,'show']);
    // hotel timings
    Route::resource('manage-hotel-timings',HotelTimingController::class);
    Route::get('manage-hotel-timings_status/{id}/{status}',[App\Http\Controllers\Admin\HotelOption\HotelTimingController::class,'status_update']);
    Route::get('manage-hotel-timings_index/{id}',[App\Http\Controllers\Admin\HotelOption\HotelTimingController::class,'index']);
    Route::get('manage-hotel-timings_create/{hotel_id}',[App\Http\Controllers\Admin\HotelOption\HotelTimingController::class,'create']);
    Route::get('manage-hotel-timings_show/{hotel_id}/{id}',[App\Http\Controllers\Admin\HotelOption\HotelTimingController::class,'show']);
    // hotel galleries
    Route::resource('manage-hotel-review',HotelReviewController::class);
    Route::get('manage-hotel-review_status/{id}/{status}',[App\Http\Controllers\Admin\HotelOption\HotelReviewController::class,'status_update']);
    Route::get('manage-hotel-review_index/{id}',[App\Http\Controllers\Admin\HotelOption\HotelReviewController::class,'index']);
    // Route::get('manage-hotel-review_create/{hotel_id}',[App\Http\Controllers\Admin\HotelOption\HotelReviewController::class,'create']);
    Route::get('manage-hotel-review_show/{hotel_id}/{id}',[App\Http\Controllers\Admin\HotelOption\HotelReviewController::class,'show']);
    // manage slider
    Route::resource('manage-slider',SliderController::class);
    Route::get('manage-slider_status/{id}/{status}',[App\Http\Controllers\Admin\SliderController::class,'status_update']);
    // maneg Category
    Route::resource('manage-category',CategoryController::class);
    // status update
    Route::get('manage-category_status/{id}/{status}',[App\Http\Controllers\Admin\CategoryController::class,'status_update']);
    // Manage Roles
    Route::resource('manage-role',RoleController::class);
    // Manage admin Users
    Route::resource('manage-users',UserController::class);
    Route::get('manage-users_status/{id}/{status}',[App\Http\Controllers\Admin\RoleandUser\UserController::class,'status_update']);
    // manage alerts
    Route::resource('manage-alerts',AlertController::class);
    Route::get('manage-alert_status/{id}/{status}',[App\Http\Controllers\Admin\AlertController::class,'status_update']);
    // Export and Import routes
    Route::get('manage-hotel-item_export/{hotel_id}',[App\Http\Controllers\Admin\HotelOption\HotelItemController::class,'exportItems']);
    Route::post('manage-hotel-item_import/{hotel_id}',[App\Http\Controllers\Admin\HotelOption\HotelItemController::class,'importItems']); 
    Route::Post('hotels_import',[App\Http\Controllers\Admin\HotelController::class,'importHotels']);

});
// admin panel
Route::group(['prefix' =>'admin','middleware'=>['role:admin|superadmin|csr|rider','auth']],function($role){
    Route::get('dashboard', [App\Http\Controllers\Admin\IndexController::class, 'dashboardIndex']);
        // oreder details
    // Route::resource('manage-orders',OrderController::class);
    Route::get('manage-orders/{page}',[App\Http\Controllers\Admin\OrderController::class,'index']);
    Route::get('order_view/{id}',[App\Http\Controllers\Admin\OrderController::class,'show']);
    Route::get('manage-orders_citywise/{page}/{city}/{start_date}/{end_date}',[App\Http\Controllers\Admin\OrderController::class,'cityWiseOrders']);
    Route::get('delivery-status',[App\Http\Controllers\Admin\OrderController::class,'deliveyStatus']);
    Route::post('select-order_rider',[App\Http\Controllers\Admin\OrderController::class,'selectRider']);
    Route::put('orders_csr_comments/{id}',[App\Http\Controllers\Admin\OrderController::class,'csrComments']);
});
