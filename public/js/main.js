/*
Author: Punith H T
Author URI: https://taatomitho.com
Version: 1.0
*/

$('.popupMsg').click(function() {
    Swal.fire('Stay tuned coming soon!');
});

$(function(){
    var dtToday = new Date();
    
    var month = dtToday.getMonth() + 1;
    var day = dtToday.getDate();
    var year = dtToday.getFullYear();
    if(month < 10)
        month = '0' + month.toString();
    if(day < 10)
        day = '0' + day.toString();
    
    var maxDate = year + '-' + month + '-' + day;
    $('#order_date').attr('min', maxDate);
});

$("#search-dishes").keyup(function() {
    // Search text
    var text = $(this).val().toLowerCase();
    // Hide all content class element
    $('.content').hide();
    // Search 
    $('.content .title').each(function() {
        if($(this).text().toLowerCase().indexOf(""+text+"") != -1 ) {
            $(this).closest('.content').show();
        }
    });
});

$('#search-close').click(function() {
    var text = $('#search-dishes').val('');
    $('.content').show();
});


function addToCart(itemId) {
    var currentHotel = $('#current-hotel-name').val();
    var currentHotelId = $('#current-hotel-id').val();
    var existingHotel = $('#existing-hotel-name').val();
    var existingHotelId = $('#existing-hotel-id').val();
    if(existingHotel && existingHotel != '') {
        if(currentHotelId != existingHotelId) {
            Swal.fire({
                title: 'Create new basket?',
                text: "This will clear your existing basket with "+existingHotel+" and start a new basket with "+currentHotel+".",
                // icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Create new basket'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: "GET",
                        url: '/add-to-cart/'+itemId,        
                        beforeSend: function() {
                            $(".modal1").show();
                        },
                        success: function(result, textStatus, jqXHR) {
                            $(".modal1").hide();
                            location.reload();
                            $('#your-cart').append(
                                // '<p class="mb-4 text-white">6 ITEMS</p>'
                                +'<div class="bg-white rounded shadow-sm mb-2">'
                                +'<div class="gold-members p-2 border-bottom">'
                                    +'<p class="text-gray mb-0 float-right ml-2">NPR 340</p>'
                                    +'<span class="count-number float-right">'
                                        +'<button class="btn btn-outline-secondary  btn-sm left dec"> <i class="icofont-minus"></i> </button>'
                                        +'<input class="count-number-input" type="text" value="2" readonly="">'
                                        +'<button class="btn btn-outline-secondary btn-sm right inc"> <i class="icofont-plus"></i> </button>'
                                    +'</span>'
                                    +'<div class="media">'
                                        +'<div class="media-body">'
                                           +'<p class="mt-1 mb-0 text-black">'+result.data.name+'</p>'
                                        +'</div>'
                                    +'</div>'
                                +'</div>'
                                +'</div>'
                                +'<div class="mb-2 bg-white rounded p-2 clearfix">'
                                   +'<img class="img-fluid float-left" src="/img/wallet-icon.png">'
                                   +'<p class="text-right mb-2">Subtotal (VAT Included) : NPR 450</p>'
                                   +'<p class="seven-color mb-1 text-right">Delivery Charge: NPR 0.00</p>'
                                   +'<h6 class="font-weight-bold text-right mb-2">Total : <span class="text-danger">NPR 450</span></h6>'
                                +'</div>'
                                +'<a href="/checkout" class="btn btn-success btn-block btn-lg">Checkout <i class="icofont-long-arrow-right"></i></a>'
                            );
                            $('#cart-empty-div').hide();
                            $('#header-cart').html(
                                '<div class="dropdown-cart-top-header p-4">'
                                    +'<img class="img-fluid mr-3" alt="osahan" src="/img/cart.jpg">'
                                    +'<h6 class="mb-0">Name here</h6>'
                                    +'<p class="text-secondary mb-0">310 S Front St, Memphis, USA</p>'
                                    +'<small><a class="text-primary font-weight-bold" href="#">View Full Menu</a></small>'
                                +'</div>'
                                +'<div class="dropdown-cart-top-body border-top p-4">'
                                    +'<p class="mb-2"> chicken X 2<span class="float-right text-secondary">NPR 450</span></p>'
                                +'</div>'
                                +'<div class="dropdown-cart-top-footer border-top p-4">'
                                    +'<p class="mb-0 font-weight-bold text-secondary">Sub Total <span class="float-right text-dark">NPR 450</span></p>'
                                    +'<small class="text-info">Extra charges may apply</small>'
                                +'</div>'
                                +'<div class="dropdown-cart-top-footer border-top p-2">'
                                    +'<a class="btn btn-success btn-block btn-lg" href="/checkout"> Checkout</a>'
                                +'</div>'
                            );
                            $('#header-cart-count').text('1');
                        },
                        error: function(jqXHR, textStatus, errorThrown){
                            $(".modal1").hide();
                        }
                    });
                }
            });
        } else {
            $.ajax({
                type: "GET",
                url: '/add-to-cart/'+itemId,        
                beforeSend: function() {
                    $(".modal1").show();
                },
                success: function(result, textStatus, jqXHR) {
                    $(".modal1").hide();
                    location.reload();
                    $('#your-cart').append(
                        // '<p class="mb-4 text-white">6 ITEMS</p>'
                        +'<div class="bg-white rounded shadow-sm mb-2">'
                        +'<div class="gold-members p-2 border-bottom">'
                            +'<p class="text-gray mb-0 float-right ml-2">NPR 340</p>'
                            +'<span class="count-number float-right">'
                                +'<button class="btn btn-outline-secondary  btn-sm left dec"> <i class="icofont-minus"></i> </button>'
                                +'<input class="count-number-input" type="text" value="2" readonly="">'
                                +'<button class="btn btn-outline-secondary btn-sm right inc"> <i class="icofont-plus"></i> </button>'
                            +'</span>'
                            +'<div class="media">'
                                +'<div class="media-body">'
                                   +'<p class="mt-1 mb-0 text-black">'+result.data.name+'</p>'
                                +'</div>'
                            +'</div>'
                        +'</div>'
                        +'</div>'
                        +'<div class="mb-2 bg-white rounded p-2 clearfix">'
                           +'<img class="img-fluid float-left" src="/img/wallet-icon.png">'
                           +'<p class="text-right mb-2">Subtotal (VAT Included) : NPR 450</p>'
                           +'<p class="seven-color mb-1 text-right">Delivery Charge: NPR 0.00</p>'
                           +'<h6 class="font-weight-bold text-right mb-2">Total : <span class="text-danger">NPR 450</span></h6>'
                        +'</div>'
                        +'<a href="/checkout" class="btn btn-success btn-block btn-lg">Checkout <i class="icofont-long-arrow-right"></i></a>'
                    );
                    $('#cart-empty-div').hide();
                    $('#header-cart').html(
                        '<div class="dropdown-cart-top-header p-4">'
                            +'<img class="img-fluid mr-3" alt="osahan" src="/img/cart.jpg">'
                            +'<h6 class="mb-0">Name here</h6>'
                            +'<p class="text-secondary mb-0">310 S Front St, Memphis, USA</p>'
                            +'<small><a class="text-primary font-weight-bold" href="#">View Full Menu</a></small>'
                        +'</div>'
                        +'<div class="dropdown-cart-top-body border-top p-4">'
                            +'<p class="mb-2"> chicken X 2<span class="float-right text-secondary">NPR 450</span></p>'
                        +'</div>'
                        +'<div class="dropdown-cart-top-footer border-top p-4">'
                            +'<p class="mb-0 font-weight-bold text-secondary">Sub Total <span class="float-right text-dark">NPR 450</span></p>'
                            +'<small class="text-info">Extra charges may apply</small>'
                        +'</div>'
                        +'<div class="dropdown-cart-top-footer border-top p-2">'
                            +'<a class="btn btn-success btn-block btn-lg" href="/checkout"> Checkout</a>'
                        +'</div>'
                    );
                    $('#header-cart-count').text('1');
                },
                error: function(jqXHR, textStatus, errorThrown){
                    $(".modal1").hide();
                }
            });
        }
    } else {
        $.ajax({
            type: "GET",
            url: '/add-to-cart/'+itemId,        
            beforeSend: function() {
                $(".modal1").show();
            },
            success: function(result, textStatus, jqXHR) {
                $(".modal1").hide();
                location.reload();
                $('#your-cart').append(
                    // '<p class="mb-4 text-white">6 ITEMS</p>'
                    +'<div class="bg-white rounded shadow-sm mb-2">'
                    +'<div class="gold-members p-2 border-bottom">'
                        +'<p class="text-gray mb-0 float-right ml-2">NPR 340</p>'
                        +'<span class="count-number float-right">'
                            +'<button class="btn btn-outline-secondary  btn-sm left dec"> <i class="icofont-minus"></i> </button>'
                            +'<input class="count-number-input" type="text" value="2" readonly="">'
                            +'<button class="btn btn-outline-secondary btn-sm right inc"> <i class="icofont-plus"></i> </button>'
                        +'</span>'
                        +'<div class="media">'
                            +'<div class="media-body">'
                               +'<p class="mt-1 mb-0 text-black">'+result.data.name+'</p>'
                            +'</div>'
                        +'</div>'
                    +'</div>'
                    +'</div>'
                    +'<div class="mb-2 bg-white rounded p-2 clearfix">'
                       +'<img class="img-fluid float-left" src="/img/wallet-icon.png">'
                       +'<p class="text-right mb-2">Subtotal (VAT Included) : NPR 450</p>'
                       +'<p class="seven-color mb-1 text-right">Delivery Charge: NPR 0.00</p>'
                       +'<h6 class="font-weight-bold text-right mb-2">Total : <span class="text-danger">NPR 450</span></h6>'
                    +'</div>'
                    +'<a href="/checkout" class="btn btn-success btn-block btn-lg">Checkout <i class="icofont-long-arrow-right"></i></a>'
                );
                $('#cart-empty-div').hide();
                $('#header-cart').html(
                    '<div class="dropdown-cart-top-header p-4">'
                        +'<img class="img-fluid mr-3" alt="osahan" src="/img/cart.jpg">'
                        +'<h6 class="mb-0">Name here</h6>'
                        +'<p class="text-secondary mb-0">310 S Front St, Memphis, USA</p>'
                        +'<small><a class="text-primary font-weight-bold" href="#">View Full Menu</a></small>'
                    +'</div>'
                    +'<div class="dropdown-cart-top-body border-top p-4">'
                        +'<p class="mb-2"> chicken X 2<span class="float-right text-secondary">NPR 450</span></p>'
                    +'</div>'
                    +'<div class="dropdown-cart-top-footer border-top p-4">'
                        +'<p class="mb-0 font-weight-bold text-secondary">Sub Total <span class="float-right text-dark">NPR 450</span></p>'
                        +'<small class="text-info">Extra charges may apply</small>'
                    +'</div>'
                    +'<div class="dropdown-cart-top-footer border-top p-2">'
                        +'<a class="btn btn-success btn-block btn-lg" href="/checkout"> Checkout</a>'
                    +'</div>'
                );
                $('#header-cart-count').text('1');
            },
            error: function(jqXHR, textStatus, errorThrown){
                $(".modal1").hide();
            }
        });
    }
}

$('#clearCart').click(function() {
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, clear it!'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "GET",
                url: '/clear-all-cart',        
                beforeSend: function() {
                    $(".modal1").show();
                },
                success: function(result, textStatus, jqXHR) {
                    $(".modal1").hide();
                    $('#existing-hotel-name').val('');
                    $('#existing-hotel-id').val('');
                    $('#header-cart').empty();
                    $('#header-cart').html(
                        '<div class="dropdown-cart-top-header p-4">'
                            +'<div class="bg-white rounded shadow-sm mb-2 p-5 text-center">'
                                +'<i class="fa text-muted fa-shopping-bag fa-3x"></i>'
                                +'<br><br>Cart empty!'
                            +'</div>'
                        +'</div>'
                    );
                    $('#header-cart-count').text('0');
                    $('#your-cart').empty();
                    $('#your-cart').html(
                        '<div class="dropdown-cart-top-header p-4" id="cart-empty-div">'
                            +'<div class="bg-white rounded shadow-sm mb-2 p-5 text-center">'
                                +'<i class="fa text-muted fa-shopping-bag fa-3x"></i>'
                                +'<br><br>Cart empty!'
                            +'</div>'
                        +'</div>'
                    );
                    Swal.fire(
                      'Deleted!',
                      'Your cart has been cleared.',
                      'success'
                    )
                },
                error: function(jqXHR, textStatus, errorThrown){
                    $(".modal1").hide();
                }
            });
        }
    })
});

$(".update-add-count").click(function (e) {
    var ele = $(this);
    var cartId = ele.parents("span").attr("data-id");
    var quantity = $('.cart-quantity-'+cartId).val();
    quantity++
    $('.cart-quantity-'+cartId).val(quantity);
    $.ajax({
        url: '/update-cart',
        method: "post",
        data: {
            _token: csrf.token, 
            id: cartId, 
            quantity: quantity
        },
        beforeSend: function() {
            $(".modal1").show();
            amountUpdate(cartId, quantity);
        },
        success: function (response) {
            $(".modal1").hide();
            window.location.reload();
        }
    });
});

$(".update-less-count").click(function (e) {
    var ele = $(this);
    var cartId = ele.parents("span").attr("data-id");
    var num = $('.cart-quantity-'+cartId).val();
    if (num >= 1) {
    num--
    }
    $('.cart-quantity-'+cartId).val(num);
    $.ajax({
        url: '/update-cart',
        method: "post",
        data: {
            _token: csrf.token, 
            id: cartId, 
            quantity: num
        },
        beforeSend: function() {
            $(".modal1").show();
        },
        success: function (response) {
            $(".modal1").hide();
            window.location.reload();
        }
    });
});

function amountUpdate(cartId, quantity) {
    var currentAmount = $('#amount-'+cartId).attr('data-amount');
    var subTotal = $('.subTotal').attr('data-subTotal');
    var totalAmount = parseFloat($('.totalAmount').attr('data-total'));
    $('#amount-'+cartId).text(parseFloat(currentAmount*quantity, 10).toFixed(2));
    var amount = parseFloat(currentAmount);
    $('.totalAmount').text(parseFloat(totalAmount+amount, 10).toFixed(2));
}

$('.checkout-btn').click(function() {
    var totalAmount = $('#totalAmount').attr('data-amount');
    var remaining = 500 - totalAmount;
    if (totalAmount < 500) {
        Swal.fire({
          icon: 'error',
          title: '',
          html: 'Spend another '+remaining+'/- for delivery.<br>Minimum order amount is 500/-',
        })
    } else {
        location.href="/checkout";
    }
});

function checkValidation() {
    var name = $.trim($('#name').val());
    var email = $.trim($('#email').val());
    var phone = $.trim($('#phoneNumber').val());
    var address = $('#completeAddress').val();
    var deliveryTime = $('#deliveryTime').find(":selected").val();
    var finalAmount = $('#finalAmount').val();

    if (finalAmount < 500) {
        Swal.fire({
          icon: 'error',
          title: '',
          html: 'Spend another '+remaining+'/- for delivery.<br>Minimum order amount is 500/-',
        });
        return false;
    }
    if (name === '') {
        Swal.fire('Please enter the name');
        return false;
    }
    if (email === '') {
        Swal.fire('Please enter the email');
        return false;
    }
    if (email != '') {
        var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;
        if(!pattern.test(email)) {
            Swal.fire('Please enter valid email');
            return false;
        }
    }
    if (phone === '') {
        Swal.fire('Please enter the phone number');
        return false;
    }
    // if (phone != '') {
    //     var regEx = /^[0]?[789]\d{9}$/;
    //     if(!phone.match(regEx)) {
    //         Swal.fire('Please enter valid phone number');
    //         return false;
    //     }
    // }
    if (address === '') {
        Swal.fire('Please enter the complete address');
        return false;
    }
    if (deliveryTime === '') {
        Swal.fire('Please select timing');
        return false;
    }

    return true;
}

$('#confirmOrder').click(function() {
    var totalAmount = $('.totalAmount').attr('data-total');
    var remaining = 500 - totalAmount;
    if (totalAmount < 500) {
        Swal.fire({
          icon: 'error',
          title: '',
          html: 'Spend another '+remaining+'/- for delivery.<br>Minimum order amount is 500/-',
        });
        return false;
    }
    if(checkValidation()) {
        var name = $.trim($('#name').val());
        var email = $('#email').val();
        var phone = $.trim($('#phoneNumber').val());
        var address = $('#completeAddress').val();
        var delivery_instruct = $('#delivery_instructions').val();
        var deliver_note = $('#deliver_note').val();
        var zip_code = $('#zip_code').val();
        var tag = $('#tag').val();
        var deliveryTime = $('#deliveryTime').find(":selected").val();
        var finalAmount = $('#finalAmount').val();
        var order_date = $('#order_date').val();
        $.ajax({
            type: "POST",
            url: url.order_confirm,
            dataType: "json",
            data: {
                "_token": csrf.token,
                "name" : name,
                "email" : email,
                "phone" : phone,
                "delivery_address" : address,
                "delivery_instructions" : delivery_instruct,
                "delivery_note" : deliver_note,
                "zip_code" : zip_code,
                "tag" : tag,
                "deliveryTime" : deliveryTime,
                "order_date" : order_date,
                "finalAmount" : finalAmount
            },
            beforeSend: function() {
                $(".modal1").show();
            },
            success: function(result, textStatus, jqXHR) {
                $(".modal1").hide();
                if(result.status == 0) {
                    var amountRemain = 500 - result.data;
                    Swal.fire({
                        icon: 'error',
                        title: '',
                        html: 'Spend another '+amountRemain+'/- for delivery.<br>Minimum order amount is 500/-',
                    });
                }
                if(result.status == 1) {
                   window.location.href = "/payment/"+result.data.orderId;
                }
            },
            error: function(jqXHR, textStatus, errorThrown){
                $(".modal1").hide();
            }
        });
    }
});

$('#cashOnDelivery').click(function() {
    var totalAmount = $('#totalAmount').val();
    var orderId = $('#orderId').val();
    $.ajax({
        type: "POST",
        url: url.payment_success,
        dataType: "json",
        data: {
            "_token": csrf.token,
            "order_type" : 'cash',
            "order_id" : orderId,
            "amount" : totalAmount
        },
        beforeSend: function() {
            $(".modal1").show();
        },
        success: function(result, textStatus, jqXHR) {
            $(".modal1").hide();
            if (result.status == 1) {
               window.location.href = "/thanks";
            }
        },
        error: function(jqXHR, textStatus, errorThrown){
            $(".modal1").hide();
        }
    });
});

var owl = $('.owl-cuisine');
owl.owlCarousel({
    items:3,
    loop:true,
    margin:10,
    autoWidth:true,
    autoHeight:true,
    autoplay:true,
    // autoplayTimeout:1000,
    autoplayHoverPause:true
});

$('.owl-city').owlCarousel({
    autoplay: true,
    // center: true,
    // loop: true,    
    items:1,
    margin:10,
    autoWidth:true,
    autoHeight:true,
    autoplayTimeout:2000,
    autoplayHoverPause:true,
  });

$(function() {
    window.onscroll = function() {
        if (window.pageYOffset > sticky) {
            header.classList.add("sticky");
        } else {
            if(header) {
                header.classList.remove("sticky");
            }
        }
    };
    var header = document.getElementById("cart-box");
    if(header) {
        var sticky = header.offsetTop;
    }
});

const togglePassword = document.querySelector('#togglePassword');
const password = document.querySelector('#inputPassword');

if(togglePassword) {
    togglePassword.addEventListener('click', function (e) {
        // toggle the type attribute
        const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
        password.setAttribute('type', type);
        // toggle the eye slash icon
        this.classList.toggle('fa-eye-slash');
    });
}

function locationSearch(city, key) {
    if($('.locationValue'+key).prop('checked') == true) {
        // Search text
        var text = city.toLowerCase();
        // Hide all content class element
        $('.content').hide();
        // Search
        $('.content .title').each(function() {
            if($(this).text().toLowerCase().indexOf(""+text+"") != -1 ) {
                $(this).closest('.content').show();
            }
        });
    } else {
        $('.content').show();
    }
}

function cuisineSearch(cuisine, key) {
    if($('.cuisineVal'+key).prop('checked') == true) {
        // Search text
        var text = cuisine.toLowerCase();
        // Hide all content class element
        $('.content').hide();
        // Search
        $('.content .title').each(function() {
            if($(this).text().toLowerCase().indexOf(""+text+"") != -1 ) {
                $(this).closest('.content').show();
            }
        });
    } else {
        $('.content').show();
    }
}

$(".radio").on('click', function() {
    // in the handler, 'this' refers to the box clicked on
    var $box = $(this);
    if ($box.is(":checked")) {
        // the name of the box is retrieved using the .attr() method
        // as it is assumed and expected to be immutable
        var group = "input:checkbox[name='" + $box.attr("name") + "']";
        // the checked state of the group/box on the other hand will change
        // and the current value is retrieved using .prop() method
        $(group).prop("checked", false);
        $box.prop("checked", true);
    } else {
        $box.prop("checked", false);
    }
});