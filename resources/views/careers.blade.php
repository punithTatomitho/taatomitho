@extends('layouts.site_layout')
@section('content')
<section class="section pt-5 pb-5 products-section">
   <div class="container">
      <div class="row justify-content-center"><div class="col-lg-8"><div class="card card-default"><div class="card-header">Careers</div> <div class="card-body terms-of-service">
                    We connect professional people who has talent and capability to build up the careers in marketing and organizing themselves as per the company norms. We do not create opportunities but we create a healthy career as per the proven talent.
                    <br><br>
                    Follow simple three steps.
                    <br> <ul><li>1. Check out for the current openings below.</li> <li>2. Send us your resume at <a href="mailto:info@taatomitho.com">info@taatomitho.com</a>.</li> <li>3. Get called by our Human Resource team.</li></ul></div></div> <div class="card bg-dark my-5 shadow text-white"><img src="/img/jobs.jpg" class="rounded card-img"></div></div></div>
   </div>
 </div>
</section>
@endsection