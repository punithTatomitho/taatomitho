<!DOCTYPE html>
<html>
<head>
    <title>Taatomitho.com</title>
</head>
<body>
    <b>Restaurant </b>: {{$orderbookings['hotel_name']}}<br>
    @php $orderDetasils = json_decode($orderbookings->item_details); @endphp
    @foreach($orderDetasils as $details)
    <h2>Order Details</h2>
    ItemName : {{$details->name}}<br>
    Quantity : {{$details->quantity}}<br>
    @endforeach
    Total Amount : {{$orderbookings->final_amount}}<br>
    Paymount Mode : {{$orderbookings['payment_mode']}}<br>
    Order Status : {{$orderbookings['order_status']}}<br><br>
    <h2>Customer Details</h2>
    Name : {{$orderbookings->name}}<br>
    Email : {{$orderbookings->email}}<br>
    Phone Number : {{$orderbookings->phone_number}}<br>
    Delivery Address : {{$orderbookings->delivery_address}}<br>
    Delivery Instruction : {{$orderbookings->delivery_instruction}}<br>
    <p>Thank you</p>
</body>
</html>