@extends('layouts.site_layout')
@section('content')
	<section class="offer-dedicated-body mt-4 mb-4 pt-2 pb-2">
         <div class="container">
            <div class="row">
               <div class="col-md-8">
                  <div class="offer-dedicated-body-left">
					 <div class="pt-2"></div>
					 <div class="bg-white rounded shadow-sm p-4 mb-4">
                        <h6 class="mb-3 text-black-50">Confirm your address and delivery time</h6>
                        <div class="row">
                           <div class="col-md-6">
                              <div class="bg-white card addresses-item mb-4 border border-success">
                                 <div class="gold-members p-4">
                                    <div class="media">
                                       <div class="mr-3"><i class="icofont-location-pin icofont-3x"></i></div>
                                       <div class="media-body">
                                          <h6 class="mb-1 text-black">DELIVER HERE</h6>
                                       </div>
                                    </div>
                                    <div class="media">
                                       <div class="media-body">
                                          <label>Name *</label>
                                          <input type="text" name="name" id="name" class="form-control" value="{{$userDetails && $userDetails->user_details ? $userDetails->user_details->user_name : ''}}">
                                          <label>Email *</label>
                                          <input type="email" name="email" id="email" class="form-control" value="{{$userDetails && $userDetails->user_details ? $userDetails->email : ''}}">
                                          <label>Mobile Number *</label>
                                          <input type="number" name="phone_nymber" id="phoneNumber" class="form-control" value="{{$userDetails && $userDetails->user_details ? $userDetails->user_details->phone_number : ''}}">
                                          <label>Complete Address *</label>
                                          <textarea name="complete_address" id="completeAddress" class="form-control" placeholder="Complete Address e.g. house number, street name, landmark">{{$userDetails && $userDetails->user_details ? $userDetails->user_details->user_address : ''}}</textarea>
                                          <label>Delivery Instructions</label>
                                          <textarea name="delivery_instructions" id="delivery_instructions" class="form-control" placeholder="Delivery Instructions e.g. Opposite Gold Souk Mall"></textarea>
                                          <div class="form-group">
                                             <div class="row">
                                                <div class="col-md-6">
                                                   <label>Zip Code</label>
                                                   <input type="text" class="form-control input-sm" name="zip_code" id="zip_code" value="{{$userDetails && $userDetails->user_details ? $userDetails->user_details->zip_code : ''}}">
                                                </div>
                                                <div class="col-md-6">
                                                   <label>Tag</label>
                                                   <input type="text" class="form-control input-sm" name="tag" id="tag">
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="bg-white card addresses-item mb-4 border border-success">
                                 <div class="gold-members p-4">
                                    <div class="media">
                                       <div class="mr-3"><i class="icofont-clock-time icofont-3x"></i></div>
                                       <div class="media-body">
                                          <h6 class="mb-1 text-secondary">Confirm your delivery time</h6>
                                       </div>
                                    </div>
                                    <div class="media">
                                       <div class="media-body">
                                          <label>Select Date</label>
                                          <input type="date" name="order_date" id="order_date" value="{{Carbon\Carbon::now()->format('Y-m-d')}}" class="form-control">
                                          <label>Select Time</label>
                                          <select name="delivery_time" id="deliveryTime" class="form-control">
                                             <option value="">Select</option>
                                             @foreach($timings as $timing)
                                             <option value="{{$timing}}">{{$timing}}</option>
                                             @endforeach
                                          </select>
                                          <div class="form-group">
                                             <label>Leave a note <small>(Optional)</small></label>
                                             <textarea placeholder="food allergies / delivery instructions" rows="4" name="delivery_note" id="deliver_note" class="form-control" spellcheck="false"></textarea>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="pt-2"></div>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="generator-bg rounded shadow-sm mb-4 p-4 osahan-cart-item" id="your-cart">
                     @php $total = 0 @endphp
                     @if(session('cart'))
                        <div class="d-flex mb-4 osahan-cart-item-profile">
                           <img class="img-fluid mr-3 rounded-pill" alt="osahan" src="{{ URL::to('storage/hotel_thumbnail_image', $hotel->hotel_thumbnail_image) }}">
                           <div class="d-flex flex-column">
                              <h6 class="mb-1 text-white">{{$hotel->name}}
                              </h6>
                              <p class="mb-0 text-white"><i class="icofont-location-pin"></i> {{$hotel->address}}</p>
                           </div>
                        </div>
                        <div class="bg-white rounded shadow-sm mb-2">
                           @foreach(session('cart') as $id => $details)
                              @php $total += $details['price'] * $details['quantity'] @endphp
                              <div class="gold-members p-2 border-bottom">
                                 <p class="text-gray mb-0 float-right ml-2">NPR <span id="amount-{{$id}}" data-amount="{{$details['price']}}">{{number_format(($details['price'] * $details['quantity']), 2)}}</span></p>
                                 <span data-id="{{$id}}" class="count-number float-right">
                                    <button class="btn btn-outline-secondary update-less-count  btn-sm left dec"> <i class="icofont-minus"></i> </button>
                                    <input class="count-number-input cart-quantity-{{$id}}" type="text" value="{{ $details['quantity'] }}" readonly="">
                                    <button class="btn btn-outline-secondary update-add-count btn-sm right inc"> <i class="icofont-plus"></i> </button>
                                 </span>
                                 <div class="media">
                                    <div class="mr-2"><i class="icofont-ui-press text-danger food-item"></i></div>
                                    <div class="media-body">
                                       <p class="mt-1 mb-0 text-black">{{ $details['name'] }}</p>
                                    </div>
                                 </div>
                              </div>
                           @endforeach
                        </div>
                        @php
                           $serviceCharge = 0;
                           if($hotel->service_charge != null) {
                              if(is_numeric($hotel->service_charge)) {
                                 $serviceCharge += ($hotel->service_charge * $total) / 100;
                              }
                           }
                           $finalAmount = $total + $serviceCharge;
                        @endphp
                        <div class="mb-2 bg-white rounded p-2 clearfix">
                           <p class="mb-1">Subtotal (VAT Included) <span class="float-right text-dark">NPR <span class="subTotal" data-subTotal="{{$total}}">{{number_format($total, 2)}}</span></span></p>
                           <p class="mb-1">Service Charges <span class="float-right text-dark">{{ $hotel->service_charge != null ? $hotel->service_charge : 0 }} %</span></p>
                           <p class="mb-1">Delivery Fee <span class="text-info" data-toggle="tooltip" data-placement="top" title="Total discount breakup">
                              <i class="icofont-info-circle"></i>
                              </span> <span class="float-right text-dark">NPR 00.00</span>
                           </p>
                           <!-- <p class="mb-1 text-success">Total Discount 
                              <span class="float-right text-success">NPR {{number_format(1884, 2)}}</span>
                           </p> -->
                           <hr />
                           <h6 class="font-weight-bold mb-0">TO PAY  <span class="float-right">NPR <span class="totalAmount" data-total="{{$total}}">{{number_format($finalAmount, 2)}}</span></span></h6>
                        </div>
                        <a href="javascript:void(0)" id="confirmOrder" class="btn btn-success btn-block btn-lg">Confirm
                        <i class="icofont-long-arrow-right"></i></a>
                     @endif
                  </div>
               </div>
            </div>
         </div>
      </section>
@endsection