@extends('layouts.site_layout')
@section('content')
	<section class="offer-dedicated-body mt-4 mb-4 pt-2 pb-2">
        <div class="container">
            <div class="row">
            	<div class="col-md-2"></div>
            	<div class="col-md-8">
	            	<div class="bg-white rounded shadow-sm p-4 osahan-payment">
		                <h5 class="mb-1">Choose payment method</h5>
		                <h6 class="mb-3 text-black-50">Credit/Debit Cards</h6>
		                <div class="row">
		                   <div class="col-sm-4 pr-0">
		                      <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
		                         <a class="nav-link active" id="v-pills-cash-tab" data-toggle="pill" href="#v-pills-cash" role="tab" aria-controls="v-pills-cash" aria-selected="false"><i class="icofont-money"></i> Pay on Delivery</a>
		                         <a class="nav-link" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true"><img src="/img/khalti.png" style="width: 20%;"> Khalti</a>
		                         <!-- <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false"><img src="/img/visa_master.png" style="width: 60%;"></a> -->
		                      </div>
		                   </div>
		                   <div class="col-sm-8 pl-0">
		                      <div class="tab-content h-100" id="v-pills-tabContent">
		                         <div class="tab-pane fade show active" id="v-pills-cash" role="tabpanel" aria-labelledby="v-pills-cash-tab">
		                            <h6 class="mb-3 mt-0 mb-3">Cash</h6>
		                            <p>Please keep exact change handy to help us serve you better</p>
		                            <hr>
									<input type="hidden" name="order_type" value="cash">
									<input type="hidden" name="order_id" id="orderId" value="{{$orderId}}">
									<input type="hidden" name="amount" id="totalAmount" value="{{$totalAmount}}">
									<button id="cashOnDelivery" class="btn btn-success btn-block btn-lg">Confirm cash on delivery <br>PAY NPR <span id="totalAmount">{{number_format($totalAmount, 2)}}</span>
									<i class="icofont-long-arrow-right"></i></button>
		                            <div class="mt-2">
		                               <small class="text-muted font-weight-lighter">
		                                  By clicking confirm, you agree to our <a target="_blank" href="/terms">Terms of Service</a> and <a target="_blank" href="/privacy">Privacy Policy</a>.
		                               </small>
		                            </div>
		                         </div>
		                         <div class="tab-pane fade" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
		                            <h6 class="mb-3 mt-0 mb-3">Khalti Payment Gateway</h6>
		                            <p>Click the button below to proceed</p>
		                            <hr>
		                            <form>
		                               <a href="javascript:void(0)" class="btn btn-success btn-block btn-lg" id="payment-button">Confirm & Pay with Khalti <br>PAY NPR <span id="totalAmount">{{number_format($totalAmount, 2)}}</span>
		                               <i class="icofont-long-arrow-right"></i></a>
		                            </form>
		                            <div class="mt-2">
		                               <small class="text-muted font-weight-lighter">
		                                  By clicking confirm, you agree to our <a target="_blank" href="/terms">Terms of Service</a> and <a target="_blank" href="/privacy">Privacy Policy</a>.
		                               </small>
		                            </div>
		                         </div>
		                         <!-- <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-home-tab">
		                            <h6 class="mb-3 mt-0 mb-3">Card Payment</h6>
		                            <p>Click the button below to proceed</p>
		                            <hr>
		                            <p><img src="/img/nic-cards.png" alt="" class="img-fluid rounded"></p>
		                            <form>
		                               <a href="thanks.html" class="btn btn-success btn-block btn-lg">Confirm & Pay with Card <br>PAY NPR 1329
		                               <i class="icofont-long-arrow-right"></i></a>
		                            </form>
		                            <div class="mt-2">
		                               <small class="text-muted font-weight-lighter">
		                                  By clicking confirm, you agree to our <a target="_blank" href="/terms">Terms of Service</a> and <a target="_blank" href="/privacy">Privacy Policy</a>.
		                               </small>
		                            </div>
		                         </div> -->
		                      </div>
		                		<div id="payment-error"></div>
		                   </div>
		                </div>
		            </div>
		        </div>
		        <div class="col-md-2"></div>
            </div>
        </div>
    </section>
    <script type="text/javascript">
    	var config = {
		    // replace the publicKey with yours
		    "publicKey":"{{env('KHALTI_PUBLIC_KEY')}}",
		    "productIdentity": '{{$orderId}}',
		    "productName": "pizza",
		    "productUrl": "http://TaatoMitho.test/order/order_id",
		    "paymentPreference": [
		        "KHALTI",
		        "EBANKING",
		        "MOBILE_BANKING",
		        "CONNECT_IPS",
		        "SCT",
		    ],
		    "eventHandler": {
		        onSuccess (payload) {
		            // hit merchant api for initiating verfication
		            $.ajax({
		                type: "POST",
		                url: url.payment_success,
		                dataType: "json",
		                data: {
		                    "_token": csrf.token,
		                    "order_type" : 'khalti_online',
		                    "mobile" : payload.mobile,
		                    "paymentId" : payload.idx,
		                    "amount" : payload.amount,
		                    "order_id" : payload.product_identity,
		                    "token" : payload.token
		                },
		                beforeSend: function() {
		                    $(".modal1").show();
		                },
		                success: function(result, textStatus, jqXHR) {
		                    $(".modal1").hide();
		                    if (result.status == 1) {
		                       window.location.href = "/thanks";
		                    }
		                    if (result.status == 0) {
		                       $('#payment-error').text(result.msg).css('color', 'red');
		                    }
		                },
		                error: function(jqXHR, textStatus, errorThrown){
		                    $(".modal1").hide();
		                }
		            });
		       },
		       onError (error) {
		          console.log(error);
		          //error conatins below response
		          // {
		          //    "action": "WALLET_PAYMENT_CONFIRM",
		          //    "message": undefined,
		          //    "payload": {
		          //      "detail": "Confirmation code or transaction pin does not match."
		          //    },
		          //    "status_code": 400
		          // }
		       },
		       onClose () {
		          console.log('widget is closing');
		       }
		    }
		};

		var checkout = new KhaltiCheckout(config);
    	var btn = document.getElementById("payment-button");
    	btn.onclick = function () {
		    var totalAmount = {{$totalAmount}};
		    var orderId = {{$orderId}};
		    // minimum transaction amount must be 10, i.e 1000 in paisa.
		    checkout.show({amount: totalAmount*100});
		}
    </script>
@endsection