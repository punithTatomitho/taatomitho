@extends('layouts.site_layout')
@section('content')
<section class="section pt-5 pb-5 products-section">
   <div class="container">
      <div class="row justify-content-center"><div class="col-lg-8"><div class="card card-default"><div class="card-header">Contact us</div> <div class="card-body terms-of-service"><div class="row"><div class="col-12 col-md-6"><p><b>Email</b>: <a href="info@taatomitho.com">info@taatomitho.com</a> <br> <b>Innerworld Enterprises Pvt Ltd </b><br><br> <b>Branch: Kathmandu</b><br>
                                Behind UTL Tower , Mitrapark Chowk ,<br>
                                Chabahil ,<br>
                                Kathmandu, Nepal.<br> <a href="tel:9801877977">9801877977</a>, <a href="tel:015907944">01-5907944/45</a></p></div> <div class="col-12 col-md-6"><p><br><br> <br> <b>Branch: Pokhara</b><br>
                                Street # 9A<br>
                                Lakeside,<br>
                                Pokhara, Nepal.<br> <a href="tel:9801862888">9801862888</a></p></div></div> <p><small class="text-muted">
                            Have a query? Email us your details (your name, email, phone Number and details of your query) at <a href="info@taatomitho.com">Info@taatomitho.com</a> and we will get in touch with you soon.
                        </small></p></div></div></div></div>
 </div>
</section>
@endsection