<div id="layoutSidenav">
    <div id="layoutSidenav_nav">
        <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
            <div class="sb-sidenav-menu">
                <div class="nav">
                    <div class="sb-sidenav-menu-heading">Core</div>
                    <a class="nav-link" href="{{url('/home')}}">
                        <div class="sb-nav-link-icon"><i class="feather-home"></i></div>
                        Home
                    </a>
                    <a class="nav-link" href="{{url('admin/manage-hotel')}}">
                        <div class="sb-nav-link-icon"><i class="feather-book-open"></i></div>
                        Manage Hotel
                    </a>
                    <a class="nav-link" href="{{url('admin/manage-cuisine-type')}}">
                        <div class="sb-nav-link-icon"><i class="feather-calendar"></i></div>
                        Manage Cuisines Types
                    </a>
                    <a class="nav-link" href="{{url('admin/manage-cuisine')}}">
                        <div class="sb-nav-link-icon"><i class="feather-message-square"></i></div>
                       Manage Cuisines
                    </a>
                   
                    <a class="nav-link" href="{{url('admin/manage-city')}}">
                        <div class="sb-nav-link-icon"><i class="feather-shopping-cart"></i></div>
                        Manage City
                    </a>
                    <a class="nav-link" href="{{url('admin/manage-category')}}">
                        <div class="sb-nav-link-icon"><i class="feather-shopping-cart"></i></div>
                        Manage Category
                    </a>
                    <a class="nav-link" href="{{url('admin/manage-slider')}}">
                        <div class="sb-nav-link-icon"><i class="feather-shopping-cart"></i></div>
                        Manage Slider
                    </a>
                    <a class="nav-link" href="{{url('admin/manage-alerts')}}">
                        <div class="sb-nav-link-icon"><i class="feather-alert-triangle"></i></div>
                        Manage Alerts
                    </a>
                </div>
            </div>
        </nav>
    </div>