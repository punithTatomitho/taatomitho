<nav class="sb-topnav navbar navbar-expand navbar-light bg-white shadow-sm">
<div class="container">
      <a class="navbar-brand" href="/"><img alt="logo" src="{{asset('img/logo-color.png')}}" style="width: 49%;"></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNavDropdown">
         <ul class="navbar-nav" style="font-size: larger;">
            <li class="nav-item active">
               <a class="nav-link" href="/">Home</a>
            </li>
            <li class="nav-item">
               <a class="nav-link" href="{{url('offers')}}"><i class="icofont-sale-discount"></i> Offers <span class="badge badge-danger">New</span></a>
            </li>
            <li class="nav-item">
               <a class="nav-link" href="{{url('restaurants')}}"><i class="icofont-motor-biker" style="font-size: large;"></i> Free Delivery</a>
            </li>
         </ul>
         <ul class="navbar-nav ml-auto" style="font-size: larger;">
            <!-- <li class="nav-item">
               <a target="_blank" href="https://blog.taatomitho.com" class="nav-link"><i aria-hidden="true" class="fa fa-newspaper-o" style="font-size: 1.2em; color: rgb(158, 158, 158);"></i>&nbsp;Blog</a>
            </li> -->
            @if (Route::has('login'))
               @auth
                  <li class="nav-item dropdown">
                     <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                     <img alt="Generic placeholder image" src="{{asset('img/user/4.png')}}" class="nav-osahan-pic rounded-pill"> {{Auth::user()->user_details->user_name}}
                     </a>
                     <div class="dropdown-menu dropdown-menu-right shadow-sm border-0">
                        <!-- <a class="dropdown-item" href="orders.html#orders"><i class="icofont-food-cart"></i> Orders</a> -->
                        <!-- <a class="dropdown-item" href="orders.html#offers"><i class="icofont-sale-discount"></i> Offers</a> -->
                        @if(Auth::user()->roles[0]['name'] != 'user')
                           @if(Auth::user()->hasRole('csr') or Auth::user()->hasRole('rider'))
                              <h6 class="dropdown-header">Manage</h6>
                              <a class="dropdown-item" href="{{url('admin/manage-orders/index')}}"><i class="fa fa-list-alt mr-3 align-self-center fa-lg"></i>Dashboard</a>
                           @endif
                           @if(Auth::user()->hasRole('admin') or Auth::user()->hasRole('superadmin'))
                              <h6 class="dropdown-header">Manage</h6>
                              <a class="dropdown-item" href="{{url('admin/kiosk')}}"><i class="fa fa-flag mr-3 align-self-center fa-lg"></i>Kiosk</a>
                              <a class="dropdown-item" href="{{url('admin/console')}}"><i class="fa fa-align-center mr-3 align-self-center fa-lg"></i>Console</a>
                              <a class="dropdown-item" href="{{url('admin/analytics')}}"><i class="fa fa-bars mr-3 align-self-center fa-lg"></i>Analytics</a>
                              <a class="dropdown-item" href="{{url('admin/manage-orders/index')}}"><i class="fa fa-list-alt mr-3 align-self-center fa-lg"></i>Dashboard</a>
                           @endif
                              <div class="dropdown-divider"></div>
                              <h6 class="dropdown-header">Setting</h6>
                              <a class="dropdown-item" href="{{url('/home')}}"><i class="fa fa-home text-left fa-btn fa-cog"></i> Your Account</a>
                              <a class="dropdown-item" href="{{url('profile')}}#settings"><i class="fa fa-fw text-left fa-btn fa-cog"></i> Your settings</a>
                        @else
                        <a class="dropdown-item" href="{{url('my-orders')}}"><i class="icofont-food-cart"></i> My Orders</a>
                        <a class="dropdown-item" href="{{url('/home')}}"><i class="fa fa-home text-left fa-btn fa-cog"></i> Your Account</a>
                        <a class="dropdown-item" href="{{url('profile')}}#settings"><i class="fa fa-fw text-left fa-btn fa-cog"></i> Your settings</a>
                        @endif
                        <a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();"><i class="icofont-logout"></i> Logout</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                     </div>
                  </li>
               @else
                  <li class="nav-item">
                     <a class="nav-link" href="{{ route('login') }}" role="button">
                        Login
                     </a>
                  </li>
               @endauth
            @endif
         </ul>
      </div>
   </div>
</nav>