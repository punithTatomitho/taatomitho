<?php 
use App\Models\Hotel;
$id = request()->segment(3);
$hotel = Hotel::findOrFail($id);
?>
<div id="layoutSidenav">
    <div id="layoutSidenav_nav">
        <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
            <div class="sb-sidenav-menu">
                <div class="nav">
                    <div class="sb-sidenav-menu-heading">Core</div>
                    <a class="nav-link" href="{{url('admin/manage-hotel')}}">
                        <div class="sb-nav-link-icon"><i class="feather-home"></i></div>
                        {{$hotel->slug}}
                    </a>
                    <a class="nav-link" href="{{url('admin/manage-hotel-category_index/'.request()->segment(3))}}">
                        <div class="sb-nav-link-icon"><i class="feather-message-square"></i></div>
                       Manage Category
                    </a>
                    <a class="nav-link" href="{{url('admin/manage-hotel-item_index/'.request()->segment(3))}}">
                        <div class="sb-nav-link-icon"><i class="feather-calendar"></i></div>
                        Manage Items
                    </a>
                    <a class="nav-link" href="{{url('admin/manage-hotel-deals_index/'.request()->segment(3))}}">
                        <div class="sb-nav-link-icon"><i class="feather-book-open"></i></div>
                        Manage Deals
                    </a>
                    <a class="nav-link" href="{{url('admin/manage-hotel-gallery_index/'.request()->segment(3))}}">
                        <div class="sb-nav-link-icon"><i class="feather-shopping-cart"></i></div>
                        Manage Gallery
                    </a>
                    <a class="nav-link" href="{{url('admin/manage-hotel-timings_index/'.request()->segment(3))}}">
                        <div class="sb-nav-link-icon"><i class="feather-shopping-cart"></i></div>
                        Manage Timings
                    </a>
                    <a class="nav-link" href="{{url('admin/manage-hotel-review_index/'.request()->segment(3))}}">
                        <div class="sb-nav-link-icon"><i class="feather-shopping-cart"></i></div>
                        Manage Reviews
                    </a>
                </div>
            </div>
        </nav>
    </div>