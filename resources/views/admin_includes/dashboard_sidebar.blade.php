<div id="layoutSidenav">
    <div id="layoutSidenav_nav">
        <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
            <div class="sb-sidenav-menu">
                <div class="nav">
                    <div class="sb-sidenav-menu-heading">Core</div>
                    <a class="nav-link" href="{{url('/home')}}">
                        <div class="sb-nav-link-icon"><i class="feather-home"></i></div>
                        Home
                    </a>
                    <a class="nav-link" href="{{url('admin/manage-orders/'.'index')}}">
                        <div class="sb-nav-link-icon"><i class="feather-calendar"></i></div>
                        Manage Orders
                    </a>
                </div>
            </div>
        </nav>
    </div>