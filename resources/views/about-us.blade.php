@extends('layouts.site_layout')
@section('content')
<section class="section pt-5 pb-5 products-section">
   <div class="container">
      <div class="row justify-content-center">
         <div class="col-lg-8"><div class="card card-default">
            <div class="card-header">About us</div>
            <div class="card-body terms-of-service">
               TaatoMitho is an online system where we serve our customers, giving various options to select their favourite food from multiple restaurants in and around Nepal. To be specific, TaatoMitho is the Smartest Online food ordering system offering Hotel rooms booking and Restaurant table booking platform, which makes life easy. Hassle-Free one-stop solution for your food and stay powered by Innerworld Enterprises Pvt Ltd .
            </div>
         </div>
      </div>
   </div>
 </div>
</section>
@endsection