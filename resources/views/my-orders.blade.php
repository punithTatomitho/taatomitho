@extends('layouts.site_layout')
@section('content')
	      <!-- Modal -->
      <div class="modal fade" id="edit-profile-modal" tabindex="-1" role="dialog" aria-labelledby="edit-profile" aria-hidden="true">
         <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <h5 class="modal-title" id="edit-profile">Edit profile</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
               </div>
               <form action="{{url('user-update')}}" method="POST">
                  @csrf
                  <div class="modal-body">
                     <div class="form-row">
                        <div class="form-group col-md-12">
                           <label>Email id
                           </label>
                           <input type="text" name="email" value="{{$user->email}}" class="form-control" placeholder="Enter Email id
                              ">
                        </div>
                        <div class="form-group col-md-12 mb-0">
                           <label>Password
                           </label>
                           <input type="password" name="password" placeholder="*********" class="form-control" placeholder="Enter password
                              ">
                        </div>
                     </div>
                  </div>
                  <div class="modal-footer">
                     <button type="button" class="btn d-flex w-50 text-center justify-content-center btn-outline-primary" data-dismiss="modal">CANCEL
                     </button>
                     <button type="submit" class="btn d-flex w-50 text-center justify-content-center btn-primary">UPDATE</button>
                  </div>
               </form>
            </div>
         </div>
      </div>
      <!-- Modal -->
      <div class="modal fade" id="add-address-modal" tabindex="-1" role="dialog" aria-labelledby="add-address" aria-hidden="true">
         <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <h5 class="modal-title" id="add-address">Add Delivery Address</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
               </div>
               <div class="modal-body">
                  <form>
                     <div class="form-row">
                        <div class="form-group col-md-12">
                           <label for="inputPassword4">Delivery Area</label>
                           <div class="input-group">
                              <input type="text" class="form-control" placeholder="Delivery Area">
                              <div class="input-group-append">
                                 <button class="btn btn-outline-secondary" type="button" id="button-addon2"><i class="icofont-ui-pointer"></i></button>
                              </div>
                           </div>
                        </div>
                        <div class="form-group col-md-12">
                           <label for="inputPassword4">Complete Address
                           </label>
                           <input type="text" class="form-control" placeholder="Complete Address e.g. house number, street name, landmark">
                        </div>
                        <div class="form-group col-md-12">
                           <label for="inputPassword4">Delivery Instructions
                           </label>
                           <input type="text" class="form-control" placeholder="Delivery Instructions e.g. Opposite Gold Souk Mall">
                        </div>
                        <div class="form-group mb-0 col-md-12">
                           <label for="inputPassword4">Nickname
                           </label>
                           <div class="btn-group btn-group-toggle d-flex justify-content-center" data-toggle="buttons">
                              <label class="btn btn-info active">
                              <input type="radio" name="options" id="option1" autocomplete="off" checked> Home
                              </label>
                              <label class="btn btn-info">
                              <input type="radio" name="options" id="option2" autocomplete="off"> Work
                              </label>
                              <label class="btn btn-info">
                              <input type="radio" name="options" id="option3" autocomplete="off"> Other
                              </label>
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
               <div class="modal-footer">
                  <button type="button" class="btn d-flex w-50 text-center justify-content-center btn-outline-primary" data-dismiss="modal">CANCEL
                  </button><button type="button" class="btn d-flex w-50 text-center justify-content-center btn-primary">SUBMIT</button>
               </div>
            </div>
         </div>
      </div>
      <!-- Modal -->
      <div class="modal fade" id="delete-address-modal" tabindex="-1" role="dialog" aria-labelledby="delete-address" aria-hidden="true">
         <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <h5 class="modal-title" id="delete-address">Delete</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
               </div>
               <div class="modal-body">
                  <p class="mb-0 text-black">Are you sure you want to delete this xxxxx?</p>
               </div>
               <div class="modal-footer">
                  <button type="button" class="btn d-flex w-50 text-center justify-content-center btn-outline-primary" data-dismiss="modal">CANCEL
                  </button><button type="button" class="btn d-flex w-50 text-center justify-content-center btn-primary">DELETE</button>
               </div>
            </div>
         </div>
      </div>
      <section class="section pt-4 pb-4 osahan-account-page">
         @if (\Session::has('user-success'))
             <div class="alert alert-dismissable alert-success">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
               </button>
                 <p>{!! \Session::get('user-success') !!}</p>
             </div>
         @endif
         <div class="container">
            <div class="row">
               <div class="col-md-3">
                  <div class="osahan-account-page-left shadow-sm bg-white h-100">
                     <div class="border-bottom p-4">
                        <div class="osahan-user text-center">
                           <div class="osahan-user-media">
                              @if($user->user_details && $user->user_details->profile_image != null)
                                 <img class="mb-3 rounded-pill shadow-sm mt-1" src="{{ URL::to('storage/profile_image', $user->user_details->profile_image) }}" alt="{{$user->name}}">
                              @else
                              <img class="mb-3 rounded-pill shadow-sm mt-1" src="img/user/4.png" alt="empty-img">
                              @endif
                              <div class="osahan-user-media-body">
                                 <h6 class="mb-2">{{ucfirst($user->name)}}</h6>
                                 <p class="mb-1">{{$user->user_details ? $user->user_details->phone_number : ''}}</p>
                                 <p>{{$user->email}}</p>
                                 <p class="mb-0 text-black font-weight-bold"><a class="text-primary mr-3" data-toggle="modal" data-target="#edit-profile-modal" href="#"><i class="icofont-ui-edit"></i> EDIT</a></p>
                              </div>
                           </div>
                        </div>
                     </div>
                     <ul class="nav nav-tabs flex-column border-0 pt-4 pl-4 pb-4" id="myTab" role="tablist">
                        <li class="nav-item">
                           <a class="nav-link active" id="settings-tab" data-toggle="tab" href="#settings" role="tab" aria-controls="settings" aria-selected="false"><i class="icofont-sale-discount"></i> Settings</a>
                        </li>
                        <li class="nav-item">
                           <a class="nav-link" id="orders-tab" data-toggle="tab" href="#orders" role="tab" aria-controls="orders" aria-selected="true"><i class="icofont-food-cart"></i> Orders</a>
                        </li>
                        <!-- <li class="nav-item">
                           <a class="nav-link" id="addresses-tab" data-toggle="tab" href="#addresses" role="tab" aria-controls="addresses" aria-selected="false"><i class="icofont-location-pin"></i> Addresses</a>
                        </li> -->
                     </ul>
                  </div>
               </div>
               <div class="col-md-9">
                  <div class="osahan-account-page-right shadow-sm bg-white p-4 h-100">
                     <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade" id="orders" role="tabpanel" aria-labelledby="orders-tab">
                           <h5 class="font-weight-bold mt-0 mb-4">My Orders</h5>
                           @if(count($orders) > 0)
                              @foreach($orders as $order)
                              <div class="bg-white card mb-4 order-list shadow-sm">
                                 <div class="gold-members p-4">
                                    <a href="javascript::void(0);">
                                       <div class="media">
                                          <img class="mr-4" src="{{ URL::to('storage/hotel_thumbnail_image', $order->hotel->hotel_thumbnail_image) }}" alt="{{$order->hotel_name}}">
                                          <div class="media-body">
                                             @if($order->delivery_status != null)
                                                <span class="float-right text-info">{{ucfirst($order->delivery_status)}} on {{$order->delivery_date}}<i class="icofont-check-circled text-success"></i></span>
                                             @endif
                                             <h6 class="mb-2">
                                                <a href="{{url('restaurants', $order->hotel->slug)}}" class="text-black">{{$order->hotel_name}}
                                                </a>
                                             </h6>
                                             <p class="text-gray mb-1"><i class="icofont-location-arrow"></i> {{$order->hotel->address}}
                                             <p class="text-gray mb-3">
                                                   <i class="icofont-phone-circle"></i>
                                                   {{$order->hotel->phone}}
                                             </p>
                                             </p>
                                             <p class="text-gray mb-3"><i class="icofont-list"></i> ORDER #NPTTM{{$order->order_id}} <i class="icofont-clock-time ml-2"></i>{{$order->order_date}}</p>
                                             @php $items = json_decode($order->item_details); @endphp
                                             <p class="text-dark">
                                                @foreach($items as $id => $item)
                                                   {{$item->name}} - {{$item->quantity}} <br>
                                                @endforeach
                                             </p>
                                             <hr>
                                             <div class="float-right">
                                                <a class="btn btn-sm btn-outline-primary" href="mailto:info@taatomitho.com"><i class="icofont-headphone-alt"></i> HELP</a>
                                             </div>
                                             <p class="mb-0 text-black text-primary pt-2"><span class="text-black font-weight-bold"> Total Paid:</span>  NPR {{number_format($order->final_amount, 2)}}
                                             </p>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              @endforeach
                           @else
                              <p>No order placed yet.!</p>
                           @endif
                        </div>
                        <div class="tab-pane fade show active" id="settings" role="tabpanel" aria-labelledby="settings-tab">
                           <h6 class="font-weight-bold mt-0 mb-4">Profile Photo</h6>
                           @if (\Session::has('upload-success'))
                               <div class="alert alert-dismissable alert-success">
                                 <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                 </button>
                                   <p>{!! \Session::get('upload-success') !!}</p>
                               </div>
                           @endif
                           <div class="row">
                              <div class="col-md-12">
                                 <div class="bg-white card addresses-item mb-4 border border-primary shadow">
                                    <div class="gold-members p-4">
                                       <div class="media">
                                          <!-- <div class="mr-4">
                                             <i class="icofont-ui-home icofont-3x"></i>
                                          </div> -->
                                          <div class="media-body">
                                             <form action="{{url('profile-update')}}" method="POST" enctype="multipart/form-data">
                                                @csrf
                                                <input type="file" name="profile_image" class="form-control" required="">
                                                <br>
                                                <button type="submit" style="float:right;" class="btn btn-success">Upload</button>
                                             </form>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <h6 class="font-weight-bold mt-0 mb-4">Profile Details</h6>
                           @if (\Session::has('field-success'))
                               <div class="alert alert-dismissable alert-success">
                                 <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                 </button>
                                   <p>{!! \Session::get('field-success') !!}</p>
                               </div>
                           @endif
                           <div class="row">
                              <div class="col-md-12">
                                 <div class="bg-white card addresses-item mb-4 border border-primary shadow">
                                    <div class="gold-members p-4">
                                       <div class="media">
                                          <div class="media-body">
                                             <form action="{{url('profile-update')}}" method="POST">
                                                @csrf
                                                <label>User Name</label>
                                                <input type="text" name="user_name" class="form-control" value="{{$user->user_details->user_name}}" required="">
                                                <label>Phone Number</label>
                                                <input type="text" name="phone_number" class="form-control" value="{{$user->user_details->phone_number}}" required="">
                                                <label>Address</label>
                                                <textarea name="user_address" class="form-control" required="">{{$user->user_details->user_address}}</textarea>
                                                <label>Zip Code</label>
                                                <input type="text" name="zip_code" class="form-control" value="{{$user->user_details->zip_code}}">
                                                <br>
                                                <button type="submit" style="float:right;" class="btn btn-success">Save</button>
                                             </form>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="tab-pane fade" id="addresses" role="tabpanel" aria-labelledby="addresses-tab">
                           <h5 class="font-weight-bold mt-0 mb-4">Manage Addresses</h5>
                           <div class="row">
                              <div class="col-md-6">
                                 <div class="bg-white card addresses-item mb-4 border border-primary shadow">
                                    <div class="gold-members p-4">
                                       <div class="media">
                                          <div class="mr-3"><i class="icofont-ui-home icofont-3x"></i></div>
                                          <div class="media-body">
                                             <h6 class="mb-1 text-secondary">Home</h6>
                                             <p class="text-black">Osahan House, Jawaddi Kalan, Ludhiana, Punjab 141002, India
                                             </p>
                                             <p class="mb-0 text-black font-weight-bold"><a class="text-primary mr-3" data-toggle="modal" data-target="#add-address-modal"  href="#"><i class="icofont-ui-edit"></i> EDIT</a> <a class="text-danger" data-toggle="modal" data-target="#delete-address-modal" href="#"><i class="icofont-ui-delete"></i> DELETE</a></p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
@endsection