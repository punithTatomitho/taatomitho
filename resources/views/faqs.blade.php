@extends('layouts.site_layout')
@section('content')
<section class="section pt-5 pb-5 products-section">
   <div class="container">
      <div class="row justify-content-center"><div class="col-lg-8"><div class="card card-default"><div class="card-header">FAQ's</div> <div class="card-body"><p><b>1. How do I use TaatoMitho?</b><br>
                        Just click on the link <a href="/">www.taatomitho.com</a> and then you can search in the box according to your need if you are searching for Food to be delivered at your door step or to book a room just to make your travelling ease .
                    </p> <p><b>2. How do I contact TaatoMitho?</b><br>
                        You can contact / reach us by going to Home page footer or you can directly contact on the given number in the contact us page.
                    </p> <p><b>3. Can I access TaatoMitho from my mobile?</b><br>
                        Yes….!! You can access the apps <a href="/share">here (iOS and Android)</a>, Alternatively you can search for TaatoMitho on Google Playstore or Apple App store.
                    </p> <p><b>4. Do we have to open an account?</b><br>
                        Yes, if you opt for using our mobile application you need to register yourself, but dont worry it just takes 2 minutes to register. If you are using our website then you have an option to book or order food as a one-time guest.
                    </p> <p><b>5. What are the mode of payments?</b><br>
                        As of now we do have only one payment option that is CASH payment, it depends on your requirements if it’s for food delivery you can pay cash to our delivery representative and if it’s for the room booking or a table booking you can pay by cash at the property.
                    </p> <p><b>6. Can I use TaatoMitho when I am travelling?</b><br>
                        Yes, you can use/access it anywhere while travelling!! To book your rooms but for food delivery we will be serving only in Nepal (Kathmandu). Coming soon in other cities as well.
                    </p> <p><b>7. How will I find the Peak and off-peak timings?</b><br>
                        TaatoMitho has only one low rate at all time of the day.
                    </p> <p><b>8. Can i get full information of Nepal's best restaurants and hotels?</b><br>
                        TaatoMitho would provide all the details about the restaurants and hotels which would be listed or registered with TaatoMitho , However we would try to get almost all the renowned properties into our service.
                    </p> <p><b>9. How many times we can access TaatoMitho?</b><br>
                        You can access TaatoMitho whenever you want without any hassles.
                    </p> <p><b>10. How many options do TaatoMitho has for ordering food online?</b><br>
                        TaatoMitho has two options for food orders, you can order for a door delivery or you can also order food where you can pick it up from the restaurant.
                    </p></div></div></div></div>
 </div>
</section>
@endsection