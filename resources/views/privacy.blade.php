@extends('layouts.site_layout')
@section('content')
<section class="section pt-5 pb-5 products-section">
   <div class="container">
      <div class="row justify-content-center"><div class="col-lg-8"><div class="card card-default"><div class="card-header">Privacy</div> <div class="card-body terms-of-service"><p><b>Privacy Statement</b><br>
                        TaatoMitho is an online Food Ordering, Hotel aggregator and Restaurant table booking system, hereinafter referred to as ‘TaatoMitho,’ is committed to respect your privacy and choices while using our website or mobile application. The statement highlights our privacy practices regarding Personal data that we hold about you.
                    </p> <p><b>Personal Information that you give to TaatoMitho</b><br>
                        We take the utmost care to ensure that the personal information we obtain from you is used only for the purposes we describe in this Privacy Policy or at the time of collection. When you are asked to sign in or to register with us, you may be asked to provide your name, telephone number, email address, and/or delivery address. In the event you opt to provide us with personal information, we will only use it for the purpose specified.
                    </p> <p><b>How TaatoMitho uses your Personal information</b><br>
                        We will always have a lawful basis when we collect and use your personal information, such as necessity to perform a contract that we have with you, because you consented to our use of your personal information. Specifically, we may use your personal information to:
                        </p><ul><li>1. To Process and manage your Food Orders/Hotel Room Bookings/Restaurant table reservations.</li> <li>2. Respond to and/or fulfill your enquiries or requests with TaatoMitho.</li> <li>3. Create and deliver personalized promotions to grab our offers.</li> <li>4. Communicate with you about TaatoMitho , including communications about your reservations or food orders</li> <li>5. Communicate with you in connection with our marketing efforts to improve our service on a daily go.</li></ul> <p></p> <p><b>Data Recipients, Disclosure of Personal data</b><br>
                        TaatoMitho does not share your Personal data with third parties for marketing purposes without seeking your prior permission. TaatoMitho will seek your consent prior to use or share Personal data for any purpose beyond the requirement for which it was originally collected. When required, TaatoMitho may disclose Personal data to external law enforcement bodies or regulatory authorities, in order to comply with legal obligations.
                    </p> <p><b>Data Security</b><br>
                        This site has security measures in place to protect the loss, misuse, and/or alteration of information under our control. The data resides behind a firewall, with access restricted to authorized TaatoMitho personnel.
                    </p></div></div></div></div>
 </div>
</section>
@endsection