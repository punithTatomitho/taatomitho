@extends('layouts.site_layout')
@section('content')
	<section class="restaurant-detailed-banner">
        <div class="text-center">
			<img class="img-fluid cover" src="{{ URL::to('storage/hotel_banner_image', $hotel->hotel_banner_image) }}">
        </div>
         <div class="restaurant-detailed-header">
            <div class="container">
               <div class="row d-flex align-items-end">
                  <div class="col-md-8">
                     <div class="restaurant-detailed-header-left">
                        <img class="img-fluid mr-3 float-left" alt="osahan" src="{{ URL::to('storage/hotel_thumbnail_image', $hotel->hotel_thumbnail_image) }}">
                        <h2 class="text-white">{{$hotel->name}}</h2>
                        <p class="text-white mb-1"><i class="icofont-location-pin"></i> {{$hotel->address}} - {{$hotel->city}}
                           @if($hotel->hotel_status == 1)
                              <span class="badge badge-success">OPEN</span>
                           @else
                              <span class="badge badge-danger">Closed</span>
                           @endif
                        </p>
                        @php
                           $cuisins = json_decode($hotel->cuisines, true);
                           $myArray = array();
                        @endphp
                        @foreach($cuisins as $cuisi)
                           @php $myArray[] = $cuisi@endphp
                        @endforeach
                        <p class="text-white mb-0"><i class="icofont-food-cart"></i>{{implode(' • ', $myArray)}}
                        </p>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="restaurant-detailed-header-right text-right">
                        <button class="btn btn-success" type="button"><i class="icofont-clock-time"></i> {{$hotel->accept_deliveries}} – {{$hotel->accept_collections}} min
                        </button>
                        <h6 class="text-white mb-0 restaurant-detailed-ratings"><span class="generator-bg rounded text-white" style="width: 12%;"><i class="icofont-star"></i> 4.1</span> 23 Ratings  <i class="ml-3 icofont-speech-comments"></i> 91 reviews</h6>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         </div> 
      </section>
      <section class="offer-dedicated-nav bg-white border-top-0 shadow-sm">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <span class="restaurant-detailed-action-btn float-right">
                  <!-- <button class="btn btn-light btn-sm border-light-btn" type="button"><i class="icofont-heart text-danger"></i> Mark as Favourite</button>
                  <button class="btn btn-light btn-sm border-light-btn" type="button"><i class="icofont-cauli-flower text-success"></i>  Pure Veg</button>
                  <button class="btn btn-outline-danger btn-sm" type="button"><i class="icofont-sale-discount"></i>  OFFERS</button> -->
                  </span>
                  <ul class="nav" id="pills-tab" role="tablist">
                     <li class="nav-item">
                        <a class="nav-link active" id="pills-order-online-tab" data-toggle="pill" href="#pills-order-online" role="tab" aria-controls="pills-order-online" aria-selected="true">Order Online</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" id="pills-gallery-tab" data-toggle="pill" href="#pills-gallery" role="tab" aria-controls="pills-gallery" aria-selected="false">Gallery</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" id="pills-restaurant-info-tab" data-toggle="pill" href="#pills-restaurant-info" role="tab" aria-controls="pills-restaurant-info" aria-selected="false">Restaurant Info</a>
                     </li>
                     <!-- <li class="nav-item">
                        <a class="nav-link" id="pills-book-tab" data-toggle="pill" href="#pills-book" role="tab" aria-controls="pills-book" aria-selected="false">Book A Table</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" id="pills-reviews-tab" data-toggle="pill" href="#pills-reviews" role="tab" aria-controls="pills-reviews" aria-selected="false">Ratings & Reviews</a>
                     </li> -->
                  </ul>
               </div>
            </div>
         </div>
      </section>
      <section class="offer-dedicated-body pt-2 pb-2 mt-4 mb-4">
         <div class="container">
            @php $currentTime = Carbon\Carbon::now(); @endphp
            @if($hotel->hotel_status == 0)
               <b style="color:red;">This restaurant is closed now.</b>
            @endif
            @if($hotel->hotel_status == 1)
               @if($hotel->timings == null && $currentTime->toTimeString() > "20:00:00")
                  <b style="color:red;">This restaurant is closed now.</b>
               @endif
            @endif
            <div class="row">
               <div class="col-md-8">
                  <div class="offer-dedicated-body-left">
                     <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-order-online" role="tabpanel" aria-labelledby="pills-order-online-tab">
                           <div id="#menu" class="bg-white rounded shadow-sm p-4 mb-4 explore-outlets">
                              <h5 class="mb-4">Recommended</h5>
                              <form class="explore-outlets-search mb-4">
                                 <div class="input-group">
                                    <input type="text" id="search-dishes" placeholder="Search for dishes..." class="form-control">
                                    <div class="input-group-append">
                                       <button type="button" id="search-close" class="btn btn-link">
                                       <i class="icofont-close-circled"></i>
                                       </button>
                                    </div>
                                 </div>
                              </form>
                           </div>
                           @foreach($hotel_items as $hotel_item)
                           <input type="hidden" id="current-hotel-name" value="{{$hotel->name}}">
                           <input type="hidden" id="current-hotel-id" value="{{$hotel->id}}">
                           <div class="row px-3 content">
                              <h5 class="mb-4 mt-3">{{$hotel_item->name}} <small class="h6 text-black-50">{{count($hotel_item->items)}} ITEMS</small></h5>
                              <div class="row px-2" style="width: 110%;">
                                 @foreach($hotel_item->items as $item)
                                 <div class="col-12 col-md-6 px-1 title">
                                    <div class="bg-white rounded border shadow-sm mb-2">
                                       <div class="gold-members p-3">
                                          <div class="row no-gutters">
                                             <div class="col-10">
                                                <h6 class="mb-2 text-black">
                                                   <b>{{$item->name}}</b>
                                                </h6>
                                                <p class="m-0 text-muted">
                                                   <small>{{$item->description}}</small>
                                                </p>
                                                <p class="text-gray mb-0">NPR&nbsp;{{number_format($item->item_price, 2)}}
                                                   @if($item->item_oldprice != 0)
                                                      <small style="text-decoration: line-through;">NPR {{number_format($item->item_oldprice, 2)}}</small>
                                                   @endif
                                                </p>
                                             </div>
                                             @if($hotel->hotel_status == 1)
                                                @if($hotel->timings == null && $currentTime->toTimeString() < "20:00:00")
                                                   <div class="col-2 px-3">
                                                      <a href="javascript:void(0);" class="btn btn-outline-secondary btn-sm media-right" onclick="addToCart('{{$item->id}}');">ADD</a>
                                                   </div>
                                                @endif
                                                @if($hotel->timings != null)
                                                   <div class="col-2 px-3">
                                                      <a href="javascript:void(0);" class="btn btn-outline-secondary btn-sm media-right" onclick="addToCart('{{$item->id}}');">ADD</a>
                                                   </div>
                                                @endif
                                             @endif
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 @endforeach
                              </div>
                           </div>
                           @endforeach
                        </div>
                        <div class="tab-pane fade" id="pills-gallery" role="tabpanel" aria-labelledby="pills-gallery-tab">
                           <div id="gallery" class="bg-white rounded shadow-sm p-4 mb-4">
                              <div class="restaurant-slider-main position-relative homepage-great-deals-carousel">
                                 @if(count($galleries) > 0)
                                    <div class="owl-carousel owl-theme homepage-ad">
                                       @foreach($galleries as $gallery)
                                       <div class="item">
                                          <img class="img-fluid" src="{{ URL::to('storage/hotel_gallery_image', $gallery->image) }}">
                                       </div>
                                       @endforeach
                                    </div>
                                    <div class="position-absolute restaurant-slider-pics bg-dark text-white">{{count($galleries)}} Photos</div>
                                    <div class="position-absolute restaurant-slider-view-all"><button type="button" class="btn btn-light bg-white">See all Photos</button></div>
                                 @else
                                    <p>No photos available!</p>
                                 @endif
                              </div>
                           </div>
                        </div>
                        <div class="tab-pane fade" id="pills-restaurant-info" role="tabpanel" aria-labelledby="pills-restaurant-info-tab">
                           <div id="restaurant-info" class="bg-white rounded shadow-sm p-4 mb-4">
                              <div class="address-map float-right ml-5">
                                 <div class="mapouter">
                                    <div class="gmap_canvas"><iframe width="300" height="170" id="gmap_canvas" src="https://maps.google.com/maps?q=27.724288323239374, 85.36253994127074&t=&z=9&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe></div>
                                 </div>
                              </div>
                              <h5 class="mb-4">{{$hotel->name}}</h5>
                              <p class="mb-3">{{$hotel->address}}
                              </p>
                              <p class="mb-2 text-black"><i class="icofont-phone-circle text-primary mr-2"></i> 9801877977, 01-5907944/45</p>
                              <p class="mb-2 text-black"><i class="icofont-email text-primary mr-2"></i> info@taatomitho.com</p>
                              @if($hotel->timings != null)
                                 @php
                                    $start = lcfirst(date('l')).'_start_at';
                                    $end = lcfirst(date('l')).'_end_at';
                                 @endphp
                                 <p class="mb-2 text-black"><i class="icofont-clock-time text-primary mr-2"></i>
                                    Today
                                    {{$hotel->timings->$start}} - {{$hotel->timings->$end}}
                                    <span class="badge badge-success"> OPEN NOW </span>
                                 </p>
                              @endif
                              <hr class="clearfix">
                              <!-- <p class="text-black mb-0">You can also check the 3D view by using our menue map clicking here &nbsp;&nbsp;&nbsp; <a class="text-info font-weight-bold" href="#">Venue Map</a></p> -->
                              <!-- <hr class="clearfix"> -->
                              <h5 class="mt-4 mb-4">More Info</h5>
                              <div class="border-btn-main mb-4">
                                 @php $otherDetails = json_decode($hotel->other_details, true); @endphp
                                 @if($otherDetails)
                                    @if(in_array('Serves Breakfast', $otherDetails))
                                       <a class="border-btn text-success mr-2" href="#"><i class="icofont-check-circled"></i> Breakfast</a>
                                    @else
                                       <a class="border-btn text-danger mr-2" href="#"><i class="icofont-close-circled"></i> No Breakfast</a>
                                    @endif
                                    @if(in_array('Serves Alcohol', $otherDetails))
                                       <a class="border-btn text-success mr-2" href="#"><i class="icofont-check-circled"></i> Alcohol Available</a>
                                    @else
                                       <a class="border-btn text-danger mr-2" href="#"><i class="icofont-close-circled"></i> No Alcohol Available</a>
                                    @endif
                                    @if(in_array('Indoor Seating Available', $otherDetails))
                                       <a class="border-btn text-success mr-2" href="#"><i class="icofont-check-circled"></i> Indoor Seating</a>
                                    @else
                                       <a class="border-btn text-danger mr-2" href="#"><i class="icofont-close-circled"></i> No Indoor Seating</a>
                                    @endif
                                    @if(in_array('Outdoor Seating Available', $otherDetails))
                                       <a class="border-btn text-success mr-2" href="#"><i class="icofont-check-circled"></i> Outdoor Seating</a>
                                    @else
                                       <a class="border-btn text-danger mr-2" href="#"><i class="icofont-close-circled"></i> No Outdoor Seating</a>
                                    @endif
                                    @if(in_array('Pure Vegetarian', $otherDetails))
                                       <a class="border-btn text-success mr-2" href="#"><i class="icofont-check-circled"></i> Vegetarian Only</a>
                                    @else
                                       <a class="border-btn text-success mr-2" href="#"><i class="icofont-check-circled"></i> Veg and Non veg</a>
                                    @endif
                                 @else
                                    <p>Not available</p>
                                 @endif
                              </div>
                              <hr class="clearfix">
                              <h5 class="mt-4 mb-4">Allergy Indications</h5>
                              @if ($hotel->allergy_indication != null)
                                 <p>{{$hotel->allergy_indication}}</p>
                              @else
                                 <p>None</p>
                              @endif
                              <hr class="clearfix">
                              <h5 class="mt-4 mb-4">Timings</h5>
                              @if($hotel->timings != null)
                                 <table class="table">
                                    <thead>
                                       <th>Day</th>
                                       <th>Timings</th>
                                    </thead>
                                    <tbody>
                                       <tr>
                                          <td>Monday</td>
                                          <td>{{$hotel->timings->monday_start_at}} - {{$hotel->timings->monday_end_at}}</td>
                                       </tr>
                                       <tr>
                                          <td>Tuesday</td>
                                          <td>{{$hotel->timings->tuesday_start_at}} - {{$hotel->timings->tuesday_end_at}}</td>
                                       </tr>
                                       <tr>
                                          <td>Wednesday</td>
                                          <td>{{$hotel->timings->wednesday_start_at}} - {{$hotel->timings->wednesday_end_at}}</td>
                                       </tr>
                                       <tr>
                                          <td>Thursday</td>
                                          <td>{{$hotel->timings->thursday_start_at}} - {{$hotel->timings->thursday_end_at}}</td>
                                       </tr>
                                       <tr>
                                          <td>Friday</td>
                                          <td>{{$hotel->timings->friday_start_at}} - {{$hotel->timings->friday_end_at}}</td>
                                       </tr>
                                       <tr>
                                          <td>Saturday</td>
                                          <td>{{$hotel->timings->saturday_start_at}} - {{$hotel->timings->saturday_end_at}}</td>
                                       </tr>
                                       <tr>
                                          <td>Sunday</td>
                                          <td>{{$hotel->timings->sunday_start_at}} - {{$hotel->timings->sunday_end_at}}</td>
                                       </tr>
                                    </tbody>
                                 </table>
                              @else
                                 <p>11:AM - 08:PM</p>
                              @endif
                           </div>
                        </div>
                        <div class="tab-pane fade" id="pills-book" role="tabpanel" aria-labelledby="pills-book-tab">
                           <div id="book-a-table" class="bg-white rounded shadow-sm p-4 mb-5 rating-review-select-page">
                              <h5 class="mb-4">Book A Table</h5>
                              <form>
                                 <div class="row">
                                    <div class="col-sm-6">
                                       <div class="form-group">
                                          <label>Full Name</label>
                                          <input class="form-control" type="text" placeholder="Enter Full Name">
                                       </div>
                                    </div>
                                    <div class="col-sm-6">
                                       <div class="form-group">
                                          <label>Email Address</label>
                                          <input class="form-control" type="text" placeholder="Enter Email address">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-sm-6">
                                       <div class="form-group">
                                          <label>Mobile number</label>
                                          <input class="form-control" type="text" placeholder="Enter Mobile number">
                                       </div>
                                    </div>
                                    <div class="col-sm-6">
                                       <div class="form-group">
                                          <label>Date And Time</label>
                                          <input class="form-control" type="text" placeholder="Enter Date And Time">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="form-group text-right">
                                    <button class="btn btn-primary" type="button"> Submit </button>
                                 </div>
                              </form>
                           </div>
                        </div>
                        <div class="tab-pane fade" id="pills-reviews" role="tabpanel" aria-labelledby="pills-reviews-tab">
                           <div id="ratings-and-reviews" class="bg-white rounded shadow-sm p-4 mb-4 clearfix restaurant-detailed-star-rating">
                              <span class="star-rating float-right">
                              <a href="#"><i class="icofont-ui-rating icofont-2x active"></i></a>
                              <a href="#"><i class="icofont-ui-rating icofont-2x active"></i></a>
                              <a href="#"><i class="icofont-ui-rating icofont-2x active"></i></a>
                              <a href="#"><i class="icofont-ui-rating icofont-2x active"></i></a>
                              <a href="#"><i class="icofont-ui-rating icofont-2x"></i></a>
                              </span>
                              <h5 class="mb-0 pt-1">Rate this Place</h5>
                           </div>
                           <div class="bg-white rounded shadow-sm p-4 mb-4 clearfix graph-star-rating">
                              <h5 class="mb-0 mb-4">Ratings and Reviews</h5>
                              <div class="graph-star-rating-header">
                                 <div class="star-rating">
                                    <a href="#"><i class="icofont-ui-rating active"></i></a>
                                    <a href="#"><i class="icofont-ui-rating active"></i></a>
                                    <a href="#"><i class="icofont-ui-rating active"></i></a>
                                    <a href="#"><i class="icofont-ui-rating active"></i></a>
                                    <a href="#"><i class="icofont-ui-rating"></i></a>  <b class="text-black ml-2">334</b>
                                 </div>
                                 <p class="text-black mb-4 mt-2">Rated 3.5 out of 5</p>
                              </div>
                              <div class="graph-star-rating-body">
                                 <div class="rating-list">
                                    <div class="rating-list-left text-black">
                                       5 Star
                                    </div>
                                    <div class="rating-list-center">
                                       <div class="progress">
                                          <div style="width: 56%" aria-valuemax="5" aria-valuemin="0" aria-valuenow="5" role="progressbar" class="progress-bar bg-primary">
                                             <span class="sr-only">80% Complete (danger)</span>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="rating-list-right text-black">56%</div>
                                 </div>
                                 <div class="rating-list">
                                    <div class="rating-list-left text-black">
                                       4 Star
                                    </div>
                                    <div class="rating-list-center">
                                       <div class="progress">
                                          <div style="width: 23%" aria-valuemax="5" aria-valuemin="0" aria-valuenow="5" role="progressbar" class="progress-bar bg-primary">
                                             <span class="sr-only">80% Complete (danger)</span>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="rating-list-right text-black">23%</div>
                                 </div>
                                 <div class="rating-list">
                                    <div class="rating-list-left text-black">
                                       3 Star
                                    </div>
                                    <div class="rating-list-center">
                                       <div class="progress">
                                          <div style="width: 11%" aria-valuemax="5" aria-valuemin="0" aria-valuenow="5" role="progressbar" class="progress-bar bg-primary">
                                             <span class="sr-only">80% Complete (danger)</span>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="rating-list-right text-black">11%</div>
                                 </div>
                                 <div class="rating-list">
                                    <div class="rating-list-left text-black">
                                       2 Star
                                    </div>
                                    <div class="rating-list-center">
                                       <div class="progress">
                                          <div style="width: 2%" aria-valuemax="5" aria-valuemin="0" aria-valuenow="5" role="progressbar" class="progress-bar bg-primary">
                                             <span class="sr-only">80% Complete (danger)</span>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="rating-list-right text-black">02%</div>
                                 </div>
                              </div>
                              <div class="graph-star-rating-footer text-center mt-3 mb-3">
                                 <button type="button" class="btn btn-outline-primary btn-sm">Rate and Review</button>
                              </div>
                           </div>
                           <div class="bg-white rounded shadow-sm p-4 mb-4 restaurant-detailed-ratings-and-reviews">
                              <a href="#" class="btn btn-outline-primary btn-sm float-right">Top Rated</a>
                              <h5 class="mb-1">All Ratings and Reviews</h5>
                              <div class="reviews-members pt-4 pb-4">
                                 <div class="media">
                                    <a href="#"><img alt="Generic placeholder image" src="{{asset('img/user/1.png')}}" class="mr-3 rounded-pill"></a>
                                    <div class="media-body">
                                       <div class="reviews-members-header">
                                          <span class="star-rating float-right">
                                          <a href="#"><i class="icofont-ui-rating active"></i></a>
                                          <a href="#"><i class="icofont-ui-rating active"></i></a>
                                          <a href="#"><i class="icofont-ui-rating active"></i></a>
                                          <a href="#"><i class="icofont-ui-rating active"></i></a>
                                          <a href="#"><i class="icofont-ui-rating"></i></a>
                                          </span>
                                          <h6 class="mb-1"><a class="text-black" href="#">Singh Osahan</a></h6>
                                          <p class="text-gray">Tue, 20 Mar 2020</p>
                                       </div>
                                       <div class="reviews-members-body">
                                          <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections </p>
                                       </div>
                                       <div class="reviews-members-footer">
                                          <a class="total-like" href="#"><i class="icofont-thumbs-up"></i> 856M</a> <a class="total-like" href="#"><i class="icofont-thumbs-down"></i> 158K</a> 
                                          <span class="total-like-user-main ml-2" dir="rtl">
                                          <a data-toggle="tooltip" data-placement="top" title="Gurdeep Osahan" href="#"><img alt="Generic placeholder image" src="{{asset('img/user/5.png')}}" class="total-like-user rounded-pill"></a>
                                          <a data-toggle="tooltip" data-placement="top" title="Gurdeep Singh" href="#"><img alt="Generic placeholder image" src="{{asset('img/user/2.png')}}" class="total-like-user rounded-pill"></a>
                                          <a data-toggle="tooltip" data-placement="top" title="Askbootstrap" href="#"><img alt="Generic placeholder image" src="{{asset('img/user/3.png')}}" class="total-like-user rounded-pill"></a>
                                          <a data-toggle="tooltip" data-placement="top" title="Osahan" href="#"><img alt="Generic placeholder image" src="{{asset('img/user/4.png')}}" class="total-like-user rounded-pill"></a>
                                          </span>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <hr>
                              <div class="reviews-members pt-4 pb-4">
                                 <div class="media">
                                    <a href="#"><img alt="Generic placeholder image" src="{{asset('img/user/6.png')}}" class="mr-3 rounded-pill"></a>
                                    <div class="media-body">
                                       <div class="reviews-members-header">
                                          <span class="star-rating float-right">
                                          <a href="#"><i class="icofont-ui-rating active"></i></a>
                                          <a href="#"><i class="icofont-ui-rating active"></i></a>
                                          <a href="#"><i class="icofont-ui-rating active"></i></a>
                                          <a href="#"><i class="icofont-ui-rating active"></i></a>
                                          <a href="#"><i class="icofont-ui-rating"></i></a>
                                          </span>
                                          <h6 class="mb-1"><a class="text-black" href="#">Gurdeep Singh</a></h6>
                                          <p class="text-gray">Tue, 20 Mar 2020</p>
                                       </div>
                                       <div class="reviews-members-body">
                                          <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.</p>
                                       </div>
                                       <div class="reviews-members-footer">
                                          <a class="total-like" href="#"><i class="icofont-thumbs-up"></i> 88K</a> <a class="total-like" href="#"><i class="icofont-thumbs-down"></i> 1K</a> 
                                          <span class="total-like-user-main ml-2" dir="rtl">
                                          <a data-toggle="tooltip" data-placement="top" title="Gurdeep Osahan" href="#"><img alt="Generic placeholder image" src="{{asset('img/user/5.png')}}" class="total-like-user rounded-pill"></a>
                                          <a data-toggle="tooltip" data-placement="top" title="Gurdeep Singh" href="#"><img alt="Generic placeholder image" src="{{asset('img/user/2.png')}}" class="total-like-user rounded-pill"></a>
                                          <a data-toggle="tooltip" data-placement="top" title="Askbootstrap" href="#"><img alt="Generic placeholder image" src="{{asset('img/user/3.png')}}" class="total-like-user rounded-pill"></a>
                                          <a data-toggle="tooltip" data-placement="top" title="Osahan" href="#"><img alt="Generic placeholder image" src="{{asset('img/user/4.png')}}" class="total-like-user rounded-pill"></a>
                                          </span>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <hr>
                              <a class="text-center w-100 d-block mt-4 font-weight-bold" href="#">See All Reviews</a>
                           </div>
                           <div class="bg-white rounded shadow-sm p-4 mb-5 rating-review-select-page">
                              <h5 class="mb-4">Leave Comment</h5>
                              <p class="mb-2">Rate the Place</p>
                              <div class="mb-4">
                                 <span class="star-rating">
                                 <a href="#"><i class="icofont-ui-rating icofont-2x"></i></a>
                                 <a href="#"><i class="icofont-ui-rating icofont-2x"></i></a>
                                 <a href="#"><i class="icofont-ui-rating icofont-2x"></i></a>
                                 <a href="#"><i class="icofont-ui-rating icofont-2x"></i></a>
                                 <a href="#"><i class="icofont-ui-rating icofont-2x"></i></a>
                                 </span>
                              </div>
                              <form>
                                 <div class="form-group">
                                    <label>Your Comment</label>
                                    <textarea class="form-control"></textarea>
                                 </div>
                                 <div class="form-group">
                                    <button class="btn btn-primary btn-sm" type="button"> Submit Comment </button>
                                 </div>
                              </form>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="cart-box" id="cart-box">
                     <div class="generator-bg rounded shadow-sm mb-4 p-4 osahan-cart-item" id="your-cart">
                        <h5 class="mb-1 text-white">Your Order</h5>
                        @php $total = 0 @endphp
                        @if(session('cart'))
                           <!-- <p class="mb-4 text-white">6 ITEMS</p> -->
                           <div class="bg-white rounded shadow-sm mb-2">
                              @foreach(session('cart') as $id => $details)
                              @php $total += $details['price'] * $details['quantity'] @endphp
                                 <div class="gold-members p-2 border-bottom">
                                    <p class="text-gray mb-0 float-right ml-2">NPR <span id="amount-{{$id}}" data-amount="{{$details['price']}}">{{number_format(($details['price'] * $details['quantity']), 2)}}</span></p>
                                    <span data-id="{{$id}}" class="count-number float-right">
                                       <button class="btn btn-outline-secondary update-less-count  btn-sm left dec"> <i class="icofont-minus"></i> </button>
                                       <input class="count-number-input cart-quantity-{{$id}}" type="text" value="{{ $details['quantity'] }}" readonly="">
                                       <button class="btn btn-outline-secondary update-add-count btn-sm right inc"> <i class="icofont-plus"></i> </button>
                                    </span>
                                    <div class="media">
                                       <div class="media-body">
                                          <p class="mt-1 mb-0 text-black">{{ $details['name'] }}</p>
                                       </div>
                                    </div>
                                 </div>
                              @endforeach
                           </div>
                           @php
                              $serviceCharge = 0;
                              if($hotel->service_charge != null) {
                                 if(is_numeric($hotel->service_charge)) {
                                    $serviceCharge += ($hotel->service_charge * $total) / 100;
                                 }
                              }
                              $finalAmount = $total + $serviceCharge;
                           @endphp
                           <div class="mb-2 bg-white rounded p-2 clearfix">
                              <span id="totalAmount" data-amount="{{$total}}"></span>
                              <img class="img-fluid float-left" src="{{asset('img/wallet-icon.png')}}">
                              <p class="text-right mb-2">Subtotal (VAT Included) : NPR <span class="subTotal" data-subTotal="{{$total}}">{{number_format($total, 2)}}</span></p>
                              <p class="seven-color mb-1 text-right">Service Charge {{ $hotel->service_charge != null ? $hotel->service_charge : 0 }} %</p>
                              <p class="seven-color mb-1 text-right">Delivery Charge: NPR 0.00</p>
                              <h6 class="font-weight-bold text-right mb-2">Total : <span class="text-danger">NPR <span class="totalAmount" data-total="{{$total}}">{{number_format($finalAmount, 2)}}</span></span></h6>
                           </div>
                           <a href="javascript:void(0);" class="btn btn-success checkout-btn btn-block btn-lg">Checkout <i class="icofont-long-arrow-right"></i></a>
                        @else
                           <div class="dropdown-cart-top-header p-4" id="cart-empty-div">
                              <div class="bg-white rounded shadow-sm mb-2 p-5 text-center"><i class="fa text-muted fa-shopping-bag fa-3x"></i><br><br>
                              Cart empty!
                              </div>
                           </div>
                        @endif
                     </div>
                     <!-- <div class="text-center pt-2 mb-4">
                     <img class="img-fluid" src="https://dummyimage.com/352x600/ccc/ffffff.png&text=Google+ads">
                     </div> -->
                  </div>
               </div>
            </div>
         </div>
      </section>
@endsection