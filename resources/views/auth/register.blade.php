@extends('layouts.site_layout')

@section('content')
<div class="container-fluid">
    <div class="row no-gutter">
        <div class="d-none d-md-flex col-md-4 col-lg-6 bg-register-img"></div>
        <div class="col-md-8 col-lg-6">
            <div class="login d-flex align-items-center py-5">
                <div class="container">
                    <div class="row">
                    <div class="col-md-9 col-lg-8 mx-auto pl-5 pr-5">
                        <h3 class="login-heading mb-4">Welcome to TaatoMitho</h3>
                        <form method="POST" action="{{ route('register') }}">
                        @csrf
                        <div class="form-label-group">
                            <input type="text" id="inputName" class="form-control @error('name') is-invalid @enderror" name="name" placeholder="Name" value="{{ old('name') }}" autocomplete="name" autofocus>
                            <label for="inputName">Name</label>
                            @if ($errors->has('name'))
                                <span class="text-danger">{{ $errors->first('name') }}</span>
                            @endif
                        </div>
                        <div class="form-label-group">
                            <input type="email" id="inputEmail" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="Email address" value="{{ old('email') }}" autocomplete="email">
                            <label for="inputEmail">Email address</label>
                            @if($errors->has('email'))
                                    <span class="text-danger">{{ $errors->first('email') }}</span>
                            @endif
                        </div>
                        <div class="form-label-group">
                            <input type="password" id="inputPassword" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Password" autocomplete="new-password">
                            <label for="inputPassword">Password</label>
                            <i class="far fa-eye eye-pass" id="togglePassword"></i>
                            @if ($errors->has('password'))
                                    <span class="text-danger">{{ $errors->first('password') }}</span>
                            @endif
                        </div>
                        <div class="form-label-group mb-4">
                            <input type="password" id="password-confirm" class="form-control" name="password_confirmation" placeholder="Confirm passowrd" autocomplete="new-password">
                            <label for="password-confirm">Confirm passowrd</label>
                        </div>
                        <div class="form-group ">
                                <button type="submit" class="btn btn-lg btn-outline-primary btn-block btn-login text-uppercase font-weight-bold mb-2">
                                    {{ __('Register') }}
                                </button>
                            
                        </div>
                        <div class="text-center pt-3">
                                Already have an Account? <a class="font-weight-bold" href="{{ route('login') }}">Sign In</a>
                        </div>
                    </form>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
