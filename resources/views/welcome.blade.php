@extends('layouts.site_layout')
@section('content')
    <section class="pt-5 pb-5 homepage-search-block position-relative">
         <div class="banner-overlay"></div>
         <div class="container">
            <div class="row d-flex align-items-center">
               <div class="col-md-8">
                  <div class="homepage-search-title">
                     <h1 class="mb-2 font-weight-normal">Food delivered from Nepal's Best Restaurants</h1>
                     <h4 class="mb-5 font-weight-normal">Delivering fresh food right now, near you</h4>
                  </div>
                  <div class="homepage-search-form">
                     <form class="form-noborder" method="GET" action="{{url('restaurants')}}">
                        <div class="form-row">
                           <div class="col-lg-7 col-md-7 col-sm-12 form-group">
                              <input type="text" name="query" placeholder="Restaurant name or cuisine" class="form-control form-control-lg">
                              <!-- <a class="locate-me" href="#"><i class="icofont-ui-pointer"></i> Locate Me</a> -->
                           </div>
                           <div class="col-lg-2 col-md-2 col-sm-12 form-group">
                              <!-- <a href="listing.html" class="btn btn-primary btn-block btn-lg btn-gradient">Search</a> -->
                              <button type="submit" class="btn btn-primary btn-block btn-lg btn-gradient">Search</button>
                           </div>
                        </div>
                     </form>
                  </div>
                  <h4 class="mt-4 text-shadow font-weight-normal" style="color:#ffff;">Service In</h4>
                  <div class="owl-carousel owl-city owl-theme">
                     <!-- @foreach($cities as $city)
                        <div class="item" style="width:136px;">
                           <div class="osahan-city-item">
                              <a href="{{url('restaurants')}}?query={{$city->name}}">
                                 <img class="img-fluid" src="{{ URL::to('storage/city_image', $city->image) }}" alt="">
                                 <h6>{{$city->name}}</h6>
                              </a>
                           </div>
                        </div>
                     @endforeach -->
                     <div class="item" style="width:136px;">
                        <div class="osahan-city-item">
                           <a href="{{url('restaurants')}}?query=Kathmandu">
                              <img class="img-fluid" src="{{ URL::to('/img/kathmandu.jpeg') }}" alt="">
                              <h6>Kathmandu</h6>
                           </a>
                        </div>
                     </div>
                     <div class="item" style="width:136px;">
                        <div class="osahan-city-item">
                           <a href="{{url('restaurants')}}?query=Lalitpur">
                              <img class="img-fluid" src="{{ URL::to('img/lalithpur.jpeg') }}" alt="">
                              <h6>Lalitpur</h6>
                           </a>
                        </div>
                     </div>
                     <div class="item" style="width:136px;">
                        <div class="osahan-city-item">
                           <a href="{{url('restaurants')}}?query=Pokhara">
                              <img class="img-fluid" src="{{ URL::to('img/pokhara.jpeg') }}" alt="">
                              <h6>Pokhara</h6>
                           </a>
                        </div>
                     </div>
                     <div class="item" style="width:136px;">
                        <div class="osahan-city-item">
                           <a href="{{url('restaurants')}}?query=Chitwan">
                              <img class="img-fluid" src="{{ URL::to('img/chitwan.jpg') }}" alt="">
                              <h6>Chitwan</h6>
                           </a>
                        </div>
                     </div>
                     <div class="item" style="width:136px;">
                        <div class="osahan-city-item">
                           <a href="{{url('restaurants')}}?query=Bhaktapur">
                              <img class="img-fluid" src="{{ URL::to('img/bhaktapur.jpg') }}" alt="">
                              <h6>Bhaktapur</h6>
                           </a>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- <div class="col-md-4">
                  <div class="osahan-slider pl-4 pt-3">
                     <div class="owl-carousel homepage-ad owl-theme">
                        <div class="item">
                           <a href="listing.html"><img class="img-fluid rounded" src="img/header.jpg"></a>
                        </div>
                        <div class="item">
                           <a href="listing.html"><img class="img-fluid rounded" src="img/header.jpg"></a>
                        </div>
                        <div class="item">
                           <a href="listing.html"><img class="img-fluid rounded" src="img/header.jpg"></a>
                        </div>
                     </div>
                  </div>
               </div> -->
            </div>
         </div>
      </section>
      <section class="section pt-5 pb-5 homepage-add-section" style="background: linear-gradient(rgb(255, 255, 255) 0%, rgb(41, 158, 158) 100%, rgb(243, 247, 248) 100%);">
         <div class="container">
            <div class="row">
               <div class="col-md-6">
                  <a href="https://www.merostay.com" target="_blank"><img loading="auto" src="{{url('img/mero.jfif')}}" style="border-radius: 10px; margin-top: 5px; width: 100%; height: auto;"></a>
               </div>
               <div class="col-md-6">
                  <a href="https://taatomitho.com/restaurants?query=tm">
                     <img loading="auto" src="{{url('img/taatomitho-banner.jpeg')}}" style="border-radius: 10px; margin-top: 5px; width: 100%; height: auto;">
                  </a>
               </div>
            </div>
         </div>
         <br>
         <div class="container">
            <h3>What would you like to have today?</h3>
            <div class="row">
               <div class="owl-carousel owl-cuisine owl-theme">
                  @foreach($cuisines as $cuisine)
                     <div class="item" style="width:200px;">
                        <div class="osahan-category-item">
                           <a href="{{url('restaurants')}}?query={{$cuisine->name}}">
                              <img class="img-fluid" src="{{ URL::to('storage/cuisine_image', $cuisine->image) }}" alt="">
                              <h6>{{$cuisine->name}}</h6>
                              <!-- <p>156</p> -->
                           </a>
                        </div>
                     </div>
                  @endforeach
               </div>
            </div>
         </div>
      </section>
      <section class="section pt-5 pb-5 bg-white homepage-add-section">
         <div class="container">
            <h3>Speacial Deals</h3>
            <div class="row">
               @foreach($special_deals as $deals)
                  <div class="col-md-3 col-6 py-3 shadow">
                     <div class="products-box">
                        <a href="{{$deals->redirect_to}}"><img alt="" src="{{ URL::to('storage/slider_bg_image', $deals->icon_image) }}" class="img-fluid rounded"></a>
                     </div>
                  </div>
               @endforeach
            </div>
         </div>
      </section>
      <section class="section pt-5 pb-5 products-section">
         <div class="container">
            <div class="section-header text-center">
               <h3>Online Groceries Shopping and Delivery is Coming Soon <!-- now --> <!-- Available -->!</h3>
               <p>Buy groceries online from TaatoMitho's, the same great quality, freshness and choice you'd find in store.</p>
               <span class="line"></span>
            </div>
            <div class="section-header text-center">
                <img src="/img/groceries.jfif" style="width:100%">
            </div>
            <!-- <div class="row">
               <div class="col-md-12">
                    <div class="section-header text-center">
                        <h3>Select a department?</h3>
                    </div>
                  <div class="owl-carousel owl-carousel-four owl-theme">
                     <div class="item">
                        <div class="list-card bg-white h-100 rounded overflow-hidden position-relative shadow-sm">
                           <div class="list-card-image">
                              <a href="detail.html">
                              <img src="img/list/1.png" class="img-fluid item-img">
                              </a>
                           </div>
                           <div class="p-3 position-relative">
                              <div class="list-card-body">
                                 <h6 class="mb-1"><a href="detail.html" class="text-black">World Famous</a></h6>
                                 <p class="text-gray mb-3">North Indian • American • Pure veg</p>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="item">
                        <div class="list-card bg-white h-100 rounded overflow-hidden position-relative shadow-sm">
                           <div class="list-card-image">
                              <a href="detail.html">
                              <img src="img/list/3.png" class="img-fluid item-img">
                              </a>
                           </div>
                           <div class="p-3 position-relative">
                              <div class="list-card-body">
                                 <h6 class="mb-1"><a href="detail.html" class="text-black">Bite Me Sandwiches</a></h6>
                                 <p class="text-gray mb-3">North Indian • Indian • Pure veg</p>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="item">
                        <div class="list-card bg-white h-100 rounded overflow-hidden position-relative shadow-sm">
                           <div class="list-card-image">
                              <a href="detail.html">
                              <img src="img/list/6.png" class="img-fluid item-img">
                              </a>
                           </div>
                           <div class="p-3 position-relative">
                              <div class="list-card-body">
                                 <h6 class="mb-1"><a href="detail.html" class="text-black">The osahan Restaurant
                                    </a>
                                 </h6>
                                 <p class="text-gray mb-3">North • Hamburgers • Pure veg</p>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="item">
                        <div class="list-card bg-white h-100 rounded overflow-hidden position-relative shadow-sm">
                           <div class="list-card-image">
                              <a href="detail.html">
                              <img src="img/list/8.png" class="img-fluid item-img">
                              </a>
                           </div>
                           <div class="p-3 position-relative">
                              <div class="list-card-body">
                                 <h6 class="mb-1"><a href="detail.html" class="text-black">Polo Lounge
                                    </a>
                                 </h6>
                                 <p class="text-gray mb-3">North Indian • Indian • Pure veg</p>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="item">
                        <div class="list-card bg-white h-100 rounded overflow-hidden position-relative shadow-sm">
                           <div class="list-card-image">
                              <a href="detail.html">
                              <img src="img/list/9.png" class="img-fluid item-img">
                              </a>
                           </div>
                           <div class="p-3 position-relative">
                              <div class="list-card-body">
                                 <h6 class="mb-1"><a href="detail.html" class="text-black">Jack Fry's
                                    </a>
                                 </h6>
                                 <p class="text-gray mb-3">North Indian • Indian • Pure veg</p>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div> -->
         </div>
      </section>
        <section class="section pt-5 pb-5 bg-white becomemember-section border-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-5 offset-md-1 hidden-xs">
                        <div class="text-center" style="overflow: hidden;">
                            <img loading="auto" src="/img/apps.jpg" class="phone img-fluid item-img" style="width: 500px; margin-bottom: -10px;">
                        </div>
                    </div>
                    <div class="col-md-5 mb-3">
                        <div class="description">
                            <h3 class="mt-">Find us soon?</h3>
                            <br>
                            <h5>Available soon on the iOS and Android app stores!</h5>
                        </div>
                        <div class="buttons my-3 px-2">
                            <div class="app">
                                <a rel="noreferrer" class="popupMsg" href="javascript:void(0);">
                                    <img loading="auto" src="/img/google.png" class="img-fluid">
                                </a>
                                <a rel="noreferrer" class="popupMsg" href="javascript:void(0);">
                                    <img loading="auto" src="/img/apple.png" class="img-fluid">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- <section class="section pt-5 pb-5 text-center bg-white">
            <div class="container">
                <div class="row">
                    <div class="col-6 col-md-2 mb-2">
                        <div class="shadow-lg">
                            <img loading="auto" src="/img/Group1.jpg" class="img-fluid img-custom">
                        </div>
                    </div>
                    <div class="col-6 col-md-2 mb-2">
                        <img loading="auto" src="/img/Group2.jpg" class="img-fluid img-custom">
                    </div>
                    <div class="col-6 col-md-2 mb-2">
                        <img loading="auto" src="/img/Group3.jpg" class="img-fluid img-custom">
                    </div>
                    <div class="col-6 col-md-2 mb-2">
                        <img loading="auto" src="/img/Group4.jpg" class="img-fluid img-custom">
                    </div>
                    <div class="col-6 col-md-2 mb-2">
                        <img loading="auto" src="/img/Group5.jpg" class="img-fluid img-custom">
                    </div>
                    <div class="col-6 col-md-2 mb-2">
                        <img loading="auto" src="/img/Group6.jpg" class="img-fluid img-custom">
                    </div>
                </div>
            </div>
        </section> -->
        <section class="section pt-5 pb-5 text-center bg-white">
            <div class="container">
                <div class="row">
                   <div class="col-sm-12">
                      <h5 class="m-0">Operate food store or restaurants? <a href="mailto:info@taatomitho.com">Work With Us</a></h5>
                   </div>
                </div>
            </div>
        </section>
@endsection