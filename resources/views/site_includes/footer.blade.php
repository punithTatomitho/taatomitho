<section class="footer pt-5 pb-5">
   <div class="container">
      <div class="row">
         <div class="col-md-4 col-12 col-sm-12">
            <div class="app">
               <p class="mb-2">DOWNLOAD APP</p>
               <a href="javascript:void(0);" class="popupMsg">
               <img class="img-fluid" src="{{asset('img/google.png')}}">
               </a>
               <a href="javascript:void(0);" class="popupMsg">
               <img class="img-fluid" src="{{asset('img/apple.png')}}">
               </a>
            </div>
         </div>
         <div class="col-md-2 col-6 col-sm-4">
            <h6 class="mb-3">About Us</h6>
            <ul>
               <li><a href="/about-us">About Us</a></li>
               <li><a href="/offers">Offers</a></li>
               <li><a href="/careers">Careers</a></li>
            </ul>
         </div>
         <div class="col-md-2 col-6 col-sm-4">
            <h6 class="mb-3">Legal</h6>
            <ul>
               <li><a href="/privacy">Privacy</a></li>
               <li><a href="/terms-and-conditions">Terms & conditions</a></li>
            </ul>
         </div>
         <div class="col-md-2 m-none col-4 col-sm-4">
            <h6 class="mb-3">Help</h6>
            <ul>
               <li><a href="/contact-us">Contact</a></li>
               <li><a href="/faqs">FAQ's</a></li>
               <li><a href="/business">For Businesses</a></li>
               <li><a href="/advertise">Advertise</a></li>
            </ul>
         </div>
         <div class="col-md-2 m-none col-4 col-sm-4">
            <h6 class="mb-3">Social</h6>
            <ul>
               <li>
                  <a rel="noreferrer" target="_blank" href="https://facebook.com/tatomithonepal"><i class="icofont-facebook"></i> Facebook</a>
               </li>
               <li>
                  <a rel="noreferrer" target="_blank" href="https://instagram.com/taatomitho"><i class="icofont-instagram"></i> Instagram</a>
               </li>
            </ul>
         </div>
      </div>
   </div>
</section>
<section class="footer-bottom-search pt-5 pb-5 bg-white">
   <div class="container">
      <div class="row">
         <div class="col-xl-12">
            <p class="text-black">POPULAR CITIES</p>
            <div class="search-links">
               @php
                  $num_of_cities = count($popularCities);
                  $num_count = 0;
               @endphp
               @foreach($popularCities as $city)
                  <a href="{{url('restaurants')}}?query={{$city->name}}">{{$city->name}}</a>
                  @php $num_count = $num_count + 1; @endphp
                  @if ($num_count < $num_of_cities)
                     |
                  @endif
               @endforeach
            </div>
            <p class="mt-4 text-black">POPULAR FOOD</p>
            <div class="search-links">
               @php
                  $num_of_cuisine = count($cuisines);
                  $num_cuisine = 0;
               @endphp
               @foreach($cuisines as $cuisine)
                  <a href="{{url('restaurants')}}?query={{$cuisine->name}}">{{$cuisine->name}}</a>
                  @php $num_cuisine = $num_cuisine + 1; @endphp
                  @if ($num_cuisine < $num_of_cuisine)
                     |
                  @endif
               @endforeach
            </div>
         </div>
      </div>
   </div>
</section>
<footer class="pt-4 pb-4 text-center">
   <div class="container">
      <p class="mt-0 mb-0">2021 © TaatoMitho. All Rights Reserved.</p>
   </div>
</footer>
<div class="modal1">
   <img src="{{URL::asset('/img/loading.gif')}}" class="center" alt="loading gif" height="50" width="50">
</div>
<script type="text/javascript">
   // Don't delete this. Below url are used in main.js
   window.url = {
          order_confirm: "{{ route('confirm') }}",
          payment_success: "{{ route('payment-success') }}",
      };
   window.csrf = {
      token: "{{ csrf_token() }}"
   };
</script>
<!-- jQuery -->
<script src="{{asset('vendor/jquery/jquery-3.6.0.min.js')}}"></script>
<!-- Bootstrap core JavaScript-->
<script src="{{asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- Select2 JavaScript-->
<script src="{{asset('vendor/select2/js/select2.min.js')}}"></script>
<!-- Owl Carousel -->
<script src="{{asset('vendor/owl-carousel/owl.carousel.js')}}"></script>
<!-- Custom scripts for all pages-->
<script src="{{asset('js/custom.js')}}"></script>
<script src="{{asset('js/main.js')}}"></script>
<script src="{{asset('vendor/swal/sweet-alert.js')}}"></script>
@if($alert != null)
<script type="text/javascript">
    $(document).ready(function() {
        if(localStorage.getItem('alertState') != 'shown'){
            Swal.fire({
                title: '{{$alert->title}}',
                icon: 'info',
                html: "{!!$alert->description!!}",
            });
            localStorage.setItem('alertState','shown')
        }
    });
</script>
@endif