<!doctype html>
<html lang="en">
   <head>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="Askbootstrap">
      <meta name="author" content="Askbootstrap">
      <title>{{ config('app.name', 'Taatomitho') }} | {{ Request::path() }}</title>
      <!-- Favicon Icon -->
      <link rel="icon" type="image/png" href="{{asset('img/fevicon.png')}}">
      <!-- Bootstrap core CSS-->
      <link href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
      <!-- Font Awesome-->
      <link href="{{asset('vendor/fontawesome/css/all.min.css')}}" rel="stylesheet">
      <!-- Font Awesome-->
      <link href="{{asset('vendor/icofont/icofont.min.css')}}" rel="stylesheet">
      <!-- Select2 CSS-->
      <link href="{{asset('vendor/select2/css/select2.min.css')}}" rel="stylesheet">
      <!-- Main styles for this template-->
      <link href="{{asset('css/main.css')}}" rel="stylesheet">
      <!-- Custom styles for this template-->
      <link href="{{asset('css/custom.css')}}" rel="stylesheet">
      <!-- Owl Carousel -->
      <link rel="stylesheet" href="{{asset('vendor/owl-carousel/owl.carousel.css')}}">
      <link rel="stylesheet" href="{{asset('vendor/owl-carousel/owl.theme.css')}}">
      <script src="https://khalti.s3.ap-south-1.amazonaws.com/KPG/dist/2020.12.17.0.0.0/khalti-checkout.iffe.js"></script>
      <!-- <script src="https://khalti.com/static/khalti-checkout.js"></script> -->
   </head>
   <body>
<body>
   <div class="d-none d-md-block" style="padding: 0px 20px; margin-bottom: 0px; border-bottom: 1px solid #FFC108; background-color: #FFC108; color: rgb(0, 0, 0);">
        <div class="row">
            <div class="container px-3 my-2">
                <div class="pull-left" style="text-align: center;">
                    <span style="color: #d60103!important"><i class="fa fa-lg fa-phone fa-rotate-90" aria-hidden="true"></i>&nbsp;Kathmandu:</span><a class="text-black" href="tel:015907944">&nbsp;01-5907944/45</a>,
                    <a class="text-black mr-3" href="tel:9801877977">9801877977</a>
                    <span style="color: #d60103!important"><i class="fa fa-lg fa-phone fa-rotate-90"></i>&nbsp;Pokhara:</span><a class="text-black" href="tel:061-590555">061-590555</a>, <a class="text-black mr-2" href="tel:015907944">&nbsp;9801862888</a>
                </div>
                <!-- <div class="pull-right">
                    <a style="background-color: #FF300A; border-radius: 99px" class="text-white mr-2 py-1 px-3" href="https://tatomitho-app.app.link">Get App</a>
                </div> -->
            </div> 
        </div>
    </div>
    @include('site_includes.header')
    @yield('content')
    @include('site_includes.footer')
</body>
</html>