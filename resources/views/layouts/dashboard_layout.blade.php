<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="Askbootstrap" />
        <meta name="author" content="Askbootstrap" />
        <title>TaatoMitho</title>
        <!-- Favicon Icon -->
        <link rel="icon" type="image/png" href="{{asset('/img/favicon.png')}}">
        <!-- Feather Icon-->
        <link href="{{asset('admin/vendor/icons/feather.css')}}" rel="stylesheet" type="text/css">
        <!-- Fontawesome Icon-->
        <link href="{{asset('admin/vendor/fontawesome/css/fontawesome.min.css')}}" rel="stylesheet" type="text/css">
        <!-- Bootstrap Css -->
        <link href="{{asset('admin/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
         <!-- Dropzone -->
        <link href="{{asset('vendor/dropzone/dist/dropzone.css')}}" rel="stylesheet">
         <!-- Date Picker -->
        <link href="http://www.ansonika.com/foogra/admin_section/css/date_picker.css" rel="stylesheet">
        <!-- Custom Css -->
        <link href="{{asset('admin/css/styles.css')}}" rel="stylesheet" />
        <!-- Datatables css -->
        <link href="{{asset('admin/vendor/dataTables/dataTables/css/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
        <!-- Summernote Editor -->
      <link href="{{asset('admin/vendor/summernote/summernote-bs4.min.css')}}" rel="stylesheet">
      <!-- toggle -->
      <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    </head>
    <body class="sb-nav-fixed">
        <div class="clearfix"></div>
        <div class="wrapper">
          @include('admin_includes.header')
          @include('admin_includes.dashboard_sidebar')
          <div class="container-fluid">
              <div id="layoutSidenav_content">
                @yield('content')
                @include('admin_includes.footer')
              <div>
          </div>
       </div>
        <!-- Jquery -->
      <script src="{{asset('admin/vendor/jquery/jquery.min.js')}}"></script>
      <!-- Fontawesome -->
      <script src="{{asset('admin/vendor/fontawesome/js/all.min.js')}}"></script>
      <!-- Bootstrap -->
      <script src="{{asset('admin/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
      <!-- Custom Js -->
      <script src="{{asset('admin/js/scripts.js')}}"></script>
      <!-- Ajax Chart Js -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js')}}" crossorigin="anonymous"></script>
      <!-- Chart Js -->
      <script src="{{asset('admin/assets/demo/chart-area-demo.js')}}"></script>
      <script src="{{asset('admin/assets/demo/chart-bar-demo.js')}}"></script>
      <script src="{{asset('admin/assets/demo/chart-pie-demo.js')}}"></script>
      <!-- Datatable Js -->
      <script src="{{asset('admin/vendor/dataTables/dataTables/js/jquery.dataTables.min.js')}}"></script>
      <script src="{{asset('admin/vendor/dataTables/dataTables/js/dataTables.bootstrap.min.js')}}"></script>
      <script src="{{asset('admin/assets/demo/datatables-demo.js')}}"></script>
      <!-- Dropzone-->
      <script src="{{asset('admin/vendor/dropzone/dist/min/dropzone.min.js')}}"></script>
      <script src="http://www.ansonika.com/foogra/admin_section/vendor/bootstrap-datepicker.js"></script>
      <script>
         $('input.date-pick').datepicker();
      </script>
      
      <!-- Summernote Editor -->
      <script src="{{asset('admin/vendor/summernote/summernote-bs4.min.js')}}"></script>
      
      <script>
         $('.editor').summernote({
             fontSizes: ['10', '14'],
             toolbar: [
                 // [groupName, [list of button]]
                 ['style', ['bold', 'italic', 'underline', 'clear']],
                 ['font', ['strikethrough']],
                 ['fontsize', ['fontsize']],
                 ['para', ['ul', 'ol', 'paragraph']]
             ],
             placeholder: 'Write here ....',
             tabsize: 2,
             height: 200
         });
      </script>
    </body>
</html>