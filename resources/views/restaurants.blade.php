@extends('layouts.site_layout')
@section('content')
   <div class="restaurant-breadcrumbs-banner">
      <div class="container text-white">
         <a href="/" class="mx-2 text-white">Home</a> / 
         <a href="/restaurants" class="mx-2 text-white">Restaurants</a>
      </div>
   </div>
   <section class="breadcrumb-osahan pt-5 pb-5 bg-dark position-relative text-center">
      <h1 class="text-white">Restaurants Near You</h1>
      <h6 class="text-white-50">Best deals at your favourite restaurants</h6>

      <div class="row">
         <div class="col-md-4"></div>
         <div class="input-group col-md-4">
            <input type="text" id="search-dishes" placeholder="Search Restaurant..." class="form-control">
            <div class="input-group-append">
               <button type="button" id="search-close" class="btn btn-link">
               <i class="icofont-close-circled"></i>
               </button>
            </div>
         </div>
         <div class="col-md-4"></div>
      </div>
   </section>
   <section class="section pt-5 pb-5 products-listing">
      <div class="container">
         <div class="row d-none-m">
            <div class="col-md-12">
               <div class="dropdown float-right">
                  <a class="btn btn-outline-info dropdown-toggle btn-sm border-white-btn" href="" role="button" data-toggle="dropdown" aria-expanded="false">
                     @if(app('request')->input('all'))
                        {{ app('request')->input('all') }}
                     @else
                        Open
                     @endif
                  </a>
                  <div class="dropdown-menu dropdown-menu-right shadow-sm border-0 ">
                     @if(app('request')->input('all'))
                        <a class="dropdown-item" href="{{url('restaurants')}}?all=Open">Open</a>
                     @endif
                     <a class="dropdown-item" href="{{url('restaurants')}}?all=All restaurants">All restaurants</a>
                  </div>
               </div>
               <!-- <h5 class="font-weight-bold mt-0 mb-3">OFFERS <small class="h6 mb-0 ml-2">299 restaurants
                  </small>
               </h5> -->
            </div>
         </div>
         <div class="row">
            <div class="col-md-3">
               <div class="filters shadow-sm rounded bg-white mb-4">
                  <div class="filters-header border-bottom pl-4 pr-4 pt-3 pb-3">
                     <h5 class="m-0">Filter By</h5>
                  </div>
                  <div class="filters-body">
                     <div id="accordion">
                        <div class="filters-card border-bottom p-4">
                           <div class="filters-card-header" id="headingOne">
                              <h6 class="mb-0">
                                 <a href="#" class="btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                 Location <i class="icofont-arrow-down float-right"></i>
                                 </a>
                              </h6>
                           </div>
                           <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                              <div class="filters-card-body card-shop-filters">
                                 @foreach($locations as $key => $location)
                                    <div class="custom-control custom-checkbox">
                                       <input type="checkbox" class="custom-control-input radio locationValue{{$key+1}}" id="cb{{$key+1}}" value="{{$location->name}}" onclick="locationSearch('{{$location->name}}', '{{$key+1}}')" name="cuisine[1][]">
                                       <label class="custom-control-label" for="cb{{$key+1}}">{{$location->name}}
                                          <!-- <small class="text-black-50">230</small> -->
                                       </label>
                                    </div>
                                 @endforeach
                                 <!-- <div class="mt-2"><a href="#" class="link">See all</a></div> -->
                              </div>
                           </div>
                        </div>
                        <div class="filters-card border-bottom p-4">
                           <div class="filters-card-header" id="headingTwo">
                              <h6 class="mb-0">
                                 <a href="#" class="btn-link" data-toggle="collapse" data-target="#collapsetwo" aria-expanded="true" aria-controls="collapsetwo">
                                 All cuisines
                                 <i class="icofont-arrow-down float-right"></i>
                                 </a>
                              </h6>
                           </div>
                           <div id="collapsetwo" class="collapse show" aria-labelledby="headingTwo" data-parent="#accordion">
                              <div class="filters-card-body card-shop-filters">
                                 <form class="filters-search mb-3">
                                    <div class="form-group">
                                       <i class="icofont-search"></i>                                 
                                       <input type="text" class="form-control" placeholder="Start typing to search...">
                                    </div>
                                 </form>
                                 @foreach($cuisines as $key => $cuisine)
                                    <div class="custom-control custom-checkbox">
                                       <input type="checkbox" class="custom-control-input radio cuisineVal{{$key+1}}" id="cb1{{$key+1}}" value="{{$cuisine->name}}" onclick="cuisineSearch('{{$cuisine->name}}', '{{$key+1}}')" name="cuisine[1][]">
                                       <label class="custom-control-label" for="cb1{{$key+1}}"> {{$cuisine->name}}
                                          <!-- <small class="text-black-50">50</small> -->
                                       </label>
                                    </div>
                                 @endforeach
                                 <!-- <div class="mt-2"><a href="#" class="link">See all</a></div> -->
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- <div class="filters pt-2">
                  <div class="filters-body rounded shadow-sm bg-white">
                     <div class="filters-card p-4">
                        <div>
                           <div class="filters-card-body card-shop-filters pt-0">
                              <div class="custom-control custom-radio">
                                 <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input" checked="">
                                 <label class="custom-control-label" for="customRadio1">Gold Partner</label>
                              </div>
                              <div class="custom-control custom-radio mt-1 mb-1">
                                 <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
                                 <label class="custom-control-label" for="customRadio2">Order Food Online</label>
                              </div>
                      <div class="custom-control custom-radio">
                                 <input type="radio" id="customRadio3" name="customRadio" class="custom-control-input" checked="">
                                 <label class="custom-control-label" for="customRadio3">Osahan Eat</label>
                              </div>
                              <hr>
                              <small class="text-success">Use code OSAHAN50 to get 50% OFF (up to $30) on first 5 orders. T&Cs apply.</small>
                           </div>
                        </div>
                     </div>
                  </div>
               </div> -->
            </div>
            <div class="col-md-9">
               <div class="row">
                  @if(count($hotels) > 0)
                     @foreach($hotels as $hotel)
                        @if($hotel->list_status == 1)
                        <div class="col-md-4 col-sm-6 mb-4 pb-2 content">
                           <div class="list-card bg-white h-100 rounded overflow-hidden position-relative shadow-sm title">
                              <div class="list-card-image">
                                 <div class="star position-absolute"><span class="badge badge-success"><i class="icofont-star"></i> 3.1 (30)</span></div>
                                 <div class="favourite-heart text-danger position-absolute"><a href="/restaurants/world-famous"><i class="icofont-heart"></i></a></div>
                                 <a href="{{url('restaurants', $hotel->slug)}}">
                                 <img src="{{ URL::to('storage/hotel_thumbnail_image', $hotel->hotel_thumbnail_image) }}" style="width:100%;" class="img-fluid item-img">
                                 </a>
                              </div>
                              <div class="p-3 position-relative">
                                 <div class="list-card-body">
                                    <h6 class="mb-1"><a href="{{url('restaurants', $hotel->slug)}}" class="text-black">{{$hotel->name}}</a></h6>
                                    <p class="text-gray mb-0 my-1"><i class="fa fa-map-pin"></i> <b>{{$hotel->address}}</b></p>
                                    @php
                                       $cuisins = json_decode($hotel->cuisines, true);
                                       $myArray = array();
                                    @endphp
                                    <p class="text-gray mb-3">
                                       @foreach($cuisins as $cuisi)
                                          @php $myArray[] = $cuisi@endphp
                                       @endforeach
                                       {{implode(' • ', $myArray)}}
                                    </p>
                                    @if($hotel->hotel_registerdetails != null)
                                       @if($hotel->hotel_registerdetails->offers_text != null)
                                          <p class="m-0 p-0 px-2 mt-2 mb-2 alert alert-warning py-2"><small class="font-weight-lighter"><i class="fa fa-lg fa-ticket"></i> {{$hotel->hotel_registerdetails->offers_text}}</small></p>
                                       @endif
                                    @endif
                                 </div>
                              </div>
                           </div>
                        </div>
                        @endif
                     @endforeach
                  @else
                     <div class="p-5 m-4 text-center">
                        <h3>Didn’t find anything for the given search</h3>
                        <h5>Try searching for another restaurant or cuisine</h5>
                     </div>
                  @endif
                  <!-- <div class="col-md-12 text-center load-more">
                     <button class="btn btn-primary" type="button" disabled>
                     <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                     Loading...
                     </button>  
                  </div> -->
               </div>
               <div class="pagination">
                  {{ $hotels->links() }}
               </div>
            </div>
         </div>
      </div>
      </div>
   </section>
@endsection