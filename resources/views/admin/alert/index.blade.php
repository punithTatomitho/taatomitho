@extends('layouts.admin_layout')
@section('content')
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Alerts</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item"><a href="{{url('admin/console')}}">Console</a></li>
                <li class="breadcrumb-item active">Alerts</li>
            </ol>
            @if (Session::has('success'))
                    <div class="alert alert-info">{{ Session::get('success') }}</div>
            @endif
            @if (Session::has('error'))
                <div class="alert alert-danger">{{ Session::get('error') }}</div>
            @endif
            <div class="card mb-4">
                <div class="card-header">
                    <i class="fas fa-table mr-1"></i>
                    Alert Lists
                </div>
                <div class="card-body">
                <a href="{{ url('admin/manage-alerts/create')}}" class="btn btn-primary btn-sm float-right" >Add Alert</a>
                <br><br>
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Image</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i= 1; ?>
                                @foreach($alerts as $alert)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td> {{$alert->title}}</td>
                                    <td> {!!$alert->description!!}</td>
                                    <td><img class="img-fluid mr-3 float-left" alt="{{$alert->alert_image}}" src="{{ URL::to('storage/alert_image/',$alert->alert_image) }}"></td>
                                    <td>
                                    @if($alert->alert_status == 1)
                                    <a href="{{url('admin/manage-alert_status/'.$alert->id.'/status')}}" onclick="return confirm('Status will be In-Active!')">
                                        <span class="btn btn-success">Active</span>
                                    </a>
                                    @else 
                                    <a href="{{url('admin/manage-alert_status/'.$alert->id.'/status')}}" onclick="return confirm('Status will be Active!')">
                                        <span class="btn btn-danger">
                                        In-Active</span>
                                    </a>
                                    @endif
                                    </td>
                                    <td> 
                                    <a href="{{ url('admin/manage-alerts/'.$alert->id.'/edit')}}"><span class="btn btn-dark"><i class="fa fa-edit"></i></i></span></a>
                                    <br><br>
                                        <form action="{{url('admin/manage-alerts/'.$alert->id) }}" class="delete" method="POST">
                                                {{ method_field('DELETE') }} {{csrf_field()}}
                                                <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure? Data will be deleted permanently!')"><i class="fa fa-trash"></i></button>
                                            </form></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection