@extends('layouts.admin_layout')
@section('content')
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Alerts</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item"><a href="{{url('admin/manage-alerts')}}">Alerts</a></li>
                <li class="breadcrumb-item active">Edit Alerts</li>
            </ol>
            <div class="row">
                <div class="col-xl-12">
                <div class="card mb-4">
                    <div class="card-header">
                        Edit Alerts
                    </div>
                    <div class="card-body">
                        <form action="{{ url('admin/manage-alerts/'.$alert->id)}}" method="POST" enctype="multipart/form-data">
                        @csrf
						{{ method_field('PUT') }}
                            <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Title</label>
                                            <input type="text" class="form-control" name="title" placeholder="title" required value="{{$alert->title}}">
                                        </div>
                                        <div class="form-group">
                                            <label>Description</label>
                                            <textarea class="editor" id="exampleFormControlTextarea1" rows="3" name="description" 
								               >{{$alert->description}}</textarea>
                                               @if ($errors->has('description'))
                                                <span class="text-danger">{{ $errors->first('description') }}</span>
                                            @endif
                                        </div>
                                        <div class="form-group mt-10">
                                            <label class="control-label">Alert Image:<small><a target="_blank" href="https://www.figma.com/">Crop image</a> - <a target="_blank" href="https://squoosh.app/">Compress image</a></small></label><br>
                                            <input type="file" class="form-control" name="alert_image" placeholder="Alert Image"  value="{{old('hotel_banner_image')}}">
                                            @if ($errors->has('Alert Image'))
                                                <span class="text-danger">{{ $errors->first('Alert Image') }}</span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-success">
                                                <i class="feather-send"></i> SAVE
                                            </button>
                                            <a  class="btn btn-danger" href="{{url('admin/manage-alerts')}}"> Cancel</a>
                                        <div>
                                    </div>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection