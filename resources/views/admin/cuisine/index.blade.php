@extends('layouts.admin_layout')
@section('content')
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Cuisines</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item"><a href="{{url('admin/console')}}">Console</a></li>
                <li class="breadcrumb-item active">Cuisines</li>
            </ol>
            @if (Session::has('success'))
                    <div class="alert alert-info">{{ Session::get('success') }}</div>
            @endif
            @if (Session::has('error'))
                <div class="alert alert-danger">{{ Session::get('error') }}</div>
            @endif
            <div class="card mb-4">
                <div class="card-header">
                    <i class="fas fa-table mr-1"></i>
                    Cuisines Lists
                </div>
                <div class="card-body">
                <a href="{{ url('admin/manage-cuisine/create')}}" class="btn btn-primary btn-sm float-right" >Add Cuisine</a>
                <br><br>
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Name</th>
                                    <th>Sort_Order</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i= 1; ?>@foreach($cuisines as $cuisine)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td> {{$cuisine->name}}</td>
                                    <td> {{$cuisine->status}}</td>
                                    <td>
                                        @if($cuisine->status == 1)
                                        <a href="{{url('admin/manage-cuisine_status/'.$cuisine->id)}}" onclick="return confirm('Status will be In-Active!')">
                                            <span class="btn btn-primary">Active</span>
                                        </a>
                                        @else 
                                        <a href="{{url('admin/manage-cuisine_status/'.$cuisine->id)}}" onclick="return confirm('Status will be Active!')">
                                            <span class="btn btn-danger">
                                            In-Active</span>
                                        </a>
                                        @endif
                                        <a href="{{ url('admin/manage-cuisine/'.$cuisine->id)}}"><span class="btn btn-secondary"><i class="fa fa-eye"></i> Details</span></a>
                                        <a href="{{ url('admin/manage-cuisine/'.$cuisine->id.'/edit')}}"><span class="btn btn-dark"><i class="fa fa-edit"></i></span></a>
                                        <!-- <br><br>
                                            <form action="{{url('admin/manage-cuisine/'.$cuisine->id) }}" class="delete" method="POST">
                                                {{ method_field('DELETE') }} {{csrf_field()}}
                                                <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure? Data will be deleted permanently!')"><i class="fa fa-trash"></i></button>
                                            </form> -->
                                        </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection