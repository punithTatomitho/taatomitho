@extends('layouts.admin_layout')
@section('content')

    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Add Cuisines</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item"><a href="{{url('admin/manage-cuisine')}}">Cuisines</a></li>
                <li class="breadcrumb-item active">Add Cuisine</li>
            </ol>
            <div class="row">
                <div class="col-xl-12">
                <div class="card mb-4">
                    <div class="card-header">
                        Add New Cuisine
                    </div>
                    <div class="card-body">
                        <form action="{{ url('admin/manage-cuisine')}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Name</label>
                                            <input type="text" class="form-control" name="name" placeholder="Name">
                                        </div>
                                        <div class="form-group">
                                            <label>Sort Order</label>
                                            <input type="number" class="form-control" name="sort_order" placeholder="Sor order">
                                        </div>
                                        <div class="form-group">
                                            <label>meta tag</label>
                                            <input type="text" class="form-control" name="meta_tag" placeholder="Meta tag">
                                        </div>
                                        <div class="form-group mt-10">
                                            <label>Cuisine Image</label>
                                            <input type="file" class="form-control" name="image" placeholder="Image">
                                        </div>
                                        <div class="form-group">
                                            <div class="demo-heading">Status</div>
                                            <div class="icheck-material-primary icheck-inline">
                                                <input type="radio" id="inline-radio-primary" name="status" value="1" />
                                                <label for="inline-radio-primary">Active</label>
                                            </div>
                                            <div class="icheck-material-info icheck-inline">
                                                <input type="radio" id="inline-radio-info" name="status" value="0" />
                                                <label for="inline-radio-info">In-Active</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Type</label>
                                            <div>
                                                <select class="custom-select" name="cuisinetype_id">
                                                <option value="">Select type</option>
                                                    @foreach($cuisine_types as $c_type)
                                                    <option value="{{ $c_type->id}}">{{$c_type->type}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Description</label>
                                            <textarea class="editor" id="exampleFormControlTextarea1" rows="3" name="description" 
								               ></textarea>
                                            <!-- <input class="editor" name="description" type='text'> -->
                                            <!-- <div class="editor"></div> -->
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success">
                                            <i class="feather-send"></i> SAVE
                                        </button>
                                        <a  class="btn btn-danger" href="{{url('admin/manage-cuisine')}}"> Cancel</a>
                                    </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection