@extends('layouts.dashboard_layout')
@section('content')
<div class="container">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Order Details</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6 float-right">
                            <b>Restaurant Details</b><br>
                            <b>Name:</b> {{$order->hotel->name}}<br>
                            <b>Phone:</b> {{$order->hotel->phone}}<br>
                            <b>Email:</b> {{$order->hotel->email}}<br>
                            <b>Address:</b> {{$order->hotel->address}}
                            <!-- @if(isset($order->hotel))
                                {{$order->hotel->slug}}<br>
                                {{$order->hotel->phone}}<br>
                                {{$order->hotel->email}}<br>
                                {{$order->hotel->address}}
                            @endif -->
                            <br><br>
                            <b>Customer Details</b><br>
                            Name: {{$order->name}}<br>
                            Email: {{$order->email}}<br>
                            Phone: {{$order->phone_number}}</b><br>
                            Customer Note: <b>{{$order->delivery_note}}</b><br><br>

                            <b>Order details</b><br>
                            Delivery at: {{$order->delivery_time}}<br>
                            @if(isset($order->delivery_date)){!! htmlspecialchars_decode(date('j<\s\up>S</\s\up>
                            F Y', strtotime($order->delivery_date))) !!}@endif
                            <br><br>
                            <b> Delivery Details </b><br>
                            Address : {{$order->delivery_address}}
                            <br><br>
                            <b> Rider Details </b><br>
                            Rider Name : {{$order->rider_name}}
                            <br><br>
                            <b>Csr comments</b>
                            <br><br>
                            @if(Session::has('success'))
                                <div class="alert alert-info">{{ Session::get('success') }}</div>
                            @endif
                            @if(Session::has('error'))
                                <div class="alert alert-danger">{{ Session::get('error') }}</div>
                            @endif
                            <form action="{{ url('admin/orders_csr_comments/'.$order->order_id)}}" method="POST" enctype="multipart/form-data">
                                @csrf
                                {{ method_field('PUT') }}
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <textarea class="editor" id="exampleFormControlTextarea1" rows="3" name="csr_comments" 
                                            >{{$order->csr_comments}}</textarea>
                                        
                                        </div>
                                        <div class="form-group">
                                        <button type="submit" class="btn btn-success">
                                            <i class="feather-send"></i>Send
                                        </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-6">
                            @php $itemDetails = json_decode($order->item_details) @endphp
                            @foreach($itemDetails as $details)
                            <div class="bg-white rounded border p-1">
                                @if(isset($details->category))
                                    <b class="text-danger">Category :</b><b>{{ucfirst(trans($details->category))}}</b>
                                @endif
                                <h5>
                                    <b class="text-danger">{{ucfirst(trans($details->name))}} (NPR
                                        {{$details->price}})</b> - {{$details->quantity}}
                                </h5>
                                <h5><b class="text-danger">NPR</b> - {{($details->price)*($details->quantity)}}</h5>
                                <!-- <h5><span class="badge badge-primary">
                                        @if(isset($order->hotel->hotel_category)){{$order->hotel->hotel_category->name }}@endif
                                    </span></h5> -->
                            </div>
                            <hr>
                            @endforeach
                            <b>Subtotal</b> : {{$order->final_amount}}<br>
                            @if(isset($order->hotel))
                            <b>VAT</b> : {{$order->hotel->vat}}<br>
                            <b>Service Charge</b> : {{$order->hotel->service_charge}}<br>
                            <b>Delivery Charge</b> : {{$order->hotel->delivery_charge}}<br>
                            @endif
                            <b>VAT ID</b> :
                            <div class="mt-3">
                                <b>Payment Details</b>
                                <br>
                                <b>{{$order->payment_mode}}</b><br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>
@endsection