@extends('layouts.dashboard_layout')
@section('content')
<style>
    p {
        color:black;
    }
</style>
<main>
    @if(Auth::user()->hasRole('admin') or Auth::user()->hasRole('superadmin'))
    <div class="container-fluid">
        <h1 class="mt-4">Orders</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Dashboard</a></li>
            <li class="breadcrumb-item active">Orders</li>
        </ol>
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-2 col-md-6">
                    <div class="card bg-primary text-white mb-4">
                        <div class="card-body">Total Orders
                            <br>
                            {{$total_orders}}
                        </div>
                    </div>
                </div>
                <div class="col-xl-2 col-md-6">
                    <div class="card bg-warning text-white mb-4">
                        <div class="card-body">Total Inprogress
                            <br>
                            {{$total_inprogress}}
                        </div>
                    </div>
                </div>
                <div class="col-xl-2 col-md-6">
                    <div class="card bg-success text-white mb-4">
                        <div class="card-body">Total Completed
                            <br>
                            {{$total_completed}}
                        </div>
                    </div>
                </div>
                <div class="col-xl-2 col-md-6">
                    <div class="card bg-danger text-white mb-4">
                        <div class="card-body">Total Rejected
                            <br>
                            {{$total_rejected}}
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="card bg-dark text-white mb-4">
                        <div class="card-body">TOTAL AMOUNT
                            <br>
                            <h5> NPR {{$total_amount}}</h5>
                            Cash : NPR {{$total_cash_amount}} ({{$total_cash}})
                            <br>
                            Khalti : NPR {{$total_khalti_amount}} ({{$total_khalti}})
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
        @if (Session::has('success'))
        <div class="alert alert-info">{{ Session::get('success') }}</div>
        @endif
        @if (Session::has('error'))
        <div class="alert alert-danger">{{ Session::get('error') }}</div>
        @endif
        @foreach($orders as $order)
        <div class="container">
            <!-- Modal for Rider -->
            <div class="modal fade" id="Rider-{{$order->order_id}}" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Rider Name</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                            <!-- select -->
                            <form action="{{url('admin/select-order_rider')}}" method="POST"
                                enctype="multipart/form-data" onchange="return showstatus();">
                                @csrf
                                <div class="form-group">
                                    <input type="text" value="{{$order->order_id}}" name="order_id" hidden="" />
                                </div>
                                <div class="form-group">
                                    <select class="custom-select" name="rider_id" required>
                                        @foreach($riders as $rider)
                                        <option value="{{$rider->id}}"
                                            {{ $order->rider_name == $rider->name ? 'selected' : ''}}>{{$rider->name}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">
                                        <i class="feather-send"></i> SAVE
                                    </button>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    @endforeach
    <div class="card mb-4">
        <div class="card-header">
        <i class="fas fa-table mr-1"></i>
            Orders Lists
        @if(Auth::user()->roles[0]['name'] != 'rider')
            <!-- <a href="{{ url('admin/manage-orders_citywise/'.'index'.'/'.$city_name.'/'.$start_date.'/'.$end_date)}}" class="btn btn-success btn-sm float-right"> All Orders</a> -->
            <a href="{{ url('admin/manage-orders_citywise/'.'rejected'.'/'.$city_name.'/'.$start_date.'/'.$end_date)}}" class="btn btn-dark btn-sm float-right mr-2" style="background-color:#F80719; color:black;">Rejected <span class="badge badge-light">{{$total_rejected}}</span></a>
            <a href="{{ url('admin/manage-orders_citywise/'.'ready_to_delivery'.'/'.$city_name.'/'.$start_date.'/'.$end_date)}}" class="btn btn-dark btn-sm float-right mr-2" style="background-color:#F8F407; color:black;">Ready-to-Delivery <span class="badge badge-light">{{$total_read_to_delivery}}</span></a>
            <a href="{{ url('admin/manage-orders_citywise/'.'completed'.'/'.$city_name.'/'.$start_date.'/'.$end_date)}}"
                class="btn btn-dark btn-sm float-right mr-2" style="background-color:#C1CCC0;color:black; ">Completed  <span class="badge badge-light">{{$total_completed}}</span></a>
            <a href="{{ url('admin/manage-orders_citywise/'.'inprogress'.'/'.$city_name.'/'.$start_date.'/'.$end_date)}}" class="btn btn-dark btn-sm float-right mr-2 " style="background-color:#3CA398; color:black;">In
                progress <span class="badge badge-light">{{$total_inprogress}}</span></a>
            <a href="{{ url('admin/manage-orders_citywise/'.'accepted'.'/'.$city_name.'/'.$start_date.'/'.$end_date)}}"
                class="btn btn-dark btn-sm float-right mr-2" style="background-color:#9D9DEE; color:black;">Accepted  <span class="badge badge-light">{{$total_accepted}}</span></a>
            <a href="{{ url('admin/manage-orders_citywise/'.'recieved'.'/'.$city_name.'/'.$start_date.'/'.$end_date)}}"
                class="btn btn btn-sm float-right mr-2"style="background-color:#74D242;color:black;">Recieved <span class="badge badge-light">{{$total_recieved}}</span></a>
        @endif
        </div>
        <div class="card-body">
        @if(Auth::user()->roles[0]['name'] != 'rider')
            <div class="border p-3">
                <form action="{{url('admin/manage-orders/'.'index')}}" method="get">
                @csrf
                    <select name="city" class="p-1">
                        <option>Select City</option>
                        <option value="all" {{$city == "all" ? 'selected' : '' }}>All</option>
                        @foreach($cities as $hotel_city)
                        <option value="{{$hotel_city->slug}}" {{$city == $hotel_city->slug ? 'selected' : '' }}>{{$hotel_city->name}}</option>
                        @endforeach
                    </select>
                    &#160;&#160;
                    <input type="date" name="start_date" value="{{ date('Y-m-d', strtotime($start_date)) }}">
                    &#160;&#160;
                    <lable><b>To</b></lable>
                    &#160;&#160;
                    <input type="date" name="end_date" value="{{ date('Y-m-d', strtotime($end_date)) }}">
                    &#160;&#160;
                    <button type="submit" class="btn btn-success border">Apply</button>
                    <a href="{{url('admin/manage-orders/'.'index')}}" class="btn btn-default border">Reset</a>
                </form>
            </div>
            @endif
            <br><br>
            <div class="table-responsive">
                <table class="table table-bordered" id="orderTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>City</th>
                            <!-- <th>Reference Number </th> -->
                            <th width="35%">Order details </th>
                            <th>Amount</th>
                            <th width="15%">Status</th>
                            @if(Auth::user()->roles[0]['name'] != 'rider')
                            <th>Rider</th>
                            @endif
                            <th>Csr</th>
                            <th>Csr Comments</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i= 1; ?>
                        @foreach($orders as $order)
                        @if($order->delivery_status == "recieved")
                        <tr style="background-color:#74D242">
                        @elseif($order->delivery_status == NUll)
                        <tr>
                        @elseif($order->delivery_status == "accepted")
                        <tr style="background-color:#9D9DEE">
                        @elseif($order->delivery_status == "inprogress")
                        <tr style="background-color:#3CA398">
                        @elseif($order->delivery_status == "rejected")
                        <tr style="background-color:#F80719">
                        @elseif($order->delivery_status == "ready_to_delivery")
                        <tr style="background-color:#F8F407">
                        @elseif($order->delivery_status == "completed")
                        <tr style="background-color:#C1CCC0">
                        @endif
                        <!-- <tr {{ $order->delivery_status == "recieved" | $order->delivery_status == NULL ? 'style=background-color:yellow;' : ''}}> -->
                            <td class="verticalTableHeader"><b>{{$order->name}}</b></td>
                            <td>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-6 float-right">
                                        <b>Customer Details</b><br>
                                        Name: {{$order->customer_name}}<br>
                                        Email: {{$order->customer_email}}<br>
                                        Phone: {{$order->phone_number}}</b><br><br>

                                        <b>Order details</b><br>
                                        Restaurant : <b>{{$order->hotel_name}}</b><br>
                                            Delivery at :
                                        {{$order->delivery_time}}
                                        <p>@if(isset($order->delivery_date)){!! htmlspecialchars_decode(date('j
                                            <\s\up>S</\s\up> F Y', strtotime($order->delivery_date))) !!}@endif</p>
                                        </div>
                                        <div class="col-md-6">
                                            <p><b>Recieved at :</b><br>{{$order->updated_at}}</p>
                                            <p><b>Reference Number :</b><br>#NPTTM{{$order->order_id}}</p>
                                            <p><a class="text-success" href="{{ url('admin/order_view/'.$order->order_id)}}" target="_blank"><span
                                                        class="btn btn-dark bordered"style="background-color:#F9F9FE; color:black;"><i class="fa fa-eye"></i> View
                                                        Details</span></a>
                                            <p>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td>
                                NPR {{$order->final_amount}} ({{$order->payment_mode}})
                            </td>
                            <td>
                                @php $order_id = $order->order_id; @endphp
                                <select class="custom-select" name="deliver_status" id="deliveryStatus_{{$order->order_id}}" onchange="statusChange({{$order_id}});">
                                <option value="" {{ $order->delivery_status == NULL ? 'selected' : ''}} hidden></option>
                                    <option value="recieved"
                                        {{ $order->delivery_status == "recieved" ? 'selected' : ''}}>Recieved</option>
                                    <option value="accepted"
                                        {{ $order->delivery_status == "accepted" ? 'selected' : ''}}>Accepted
                                    </option>
                                    <option value="ready_to_delivery"
                                        {{ $order->delivery_status == "ready_to_delivery" ? 'selected' : ''}}>
                                        Ready to delivey</option>
                                    <option value="inprogress"
                                        {{ $order->delivery_status == "inprogress" ? 'selected' : ''}}>
                                        Inprogress</option>
                                    <option value="completed"
                                        {{ $order->delivery_status == "completed" ? 'selected' : ''}}>Completed
                                    </option>
                                    <option value="rejected"
                                        {{ $order->delivery_status == "rejected" ? 'selected' : ''}}>Rejected
                                    </option>
                                </select>
                            </td>
                            @if(Auth::user()->roles[0]['name'] != 'rider')
                            <td>
                                <a class="text-success" data-toggle="modal" data-target="#Rider-{{$order->order_id}}"><i
                                        class="fa fa-user"></i> <i class="fa fa-location-arrow"></i>
                                    {{$order->rider_name}}</a>
                            </td>
                            @endif
                            <td>{{$order->csr_name}}</td>
                            <td>{!!$order->csr_comments!!}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </div>
</main>
<script>
// for delivery status
    function statusChange(id) {
        if(window.confirm("Status will be change!") == true){
            var deliver_status = $("#deliveryStatus_"+id).val();
            var order_id = id;
            if (deliver_status) {
                $.ajax({
                    url: '/admin/delivery-status',
                    type: "GET",
                    dataType: "json",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {
                        'deliver_status': deliver_status,
                        'order_id': order_id
                    },
                    success: function(data) {
                        console.log('success');
                        location.reload();
                    }
                });
            }
        }else{
            location.reload();
        }
    }
</script>
<script type="text/javascript">
        setTimeout(function(){
            location.reload();
        },30000);

        $(document).ready(function() {
            $('#orderTable').dataTable( {
            "aLengthMenu": [[50, 75, -1], [50, 75, "All"]],
            "pageLength": 50,
            "aaSorting": [],
            } );
        } );
</script>
@endsection