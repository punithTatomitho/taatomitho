@extends('layouts.admin_layout')
@section('content')
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Edit City</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item"><a href="{{url('admin/manage-city')}}">Cities</a></li>
                <li class="breadcrumb-item active">Edit City</li>
            </ol>
            <div class="row">
                <div class="col-xl-12">
                <div class="card mb-4">
                    <div class="card-header">
                       Edit City
                    </div>
                    <div class="card-body">
                        <form action="{{ url('admin/manage-city/'.$city->id)}}" method="POST" enctype="multipart/form-data">
                        @csrf
						{{ method_field('PUT') }}
                            <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Name</label>
                                            <input type="text" class="form-control" name="name" placeholder="Name" value="{{$city->name}}" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Sort Order</label>
                                            <input type="number" class="form-control" name="sort_order" placeholder="Sor order" value="{{$city->sort_order}}">
                                        </div>
                                        <div class="form-group">
                                            <label>meta tag</label>
                                            <input type="text" class="form-control" name="meta_tag" placeholder="Meta tag" value="{{$city->meta_tag}}">
                                        </div>
                                        <div class="form-group mt-10">
                                            <label>City Image</label>
                                            <input type="file" class="form-control" name="image" placeholder="Image">
                                        </div>
                                        <div class="form-group">
                                            <div class="demo-heading">Status</div>
                                            <div class="icheck-material-primary icheck-inline">
                                                <input type="radio" id="inline-radio-primary" name="status" value="1" {{ $city->status == 1 ? 'checked' : ''}}/>
                                                <label for="inline-radio-primary">Active</label>
                                            </div>
                                            <div class="icheck-material-info icheck-inline">
                                                <input type="radio" id="inline-radio-info" name="status" value="0" {{ $city->status == 0 ? 'checked' : ''}}/>
                                                <label for="inline-radio-info">In-Active</label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                        <button type="submit" class="btn btn-success">
                                            <i class="feather-send"></i> SAVE
                                        </button>
                                        <a  class="btn btn-danger" href="{{url('admin/manage-city')}}"> Cancel</a>
                                    </div>
                                    </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection