@extends('layouts.admin_layout')
@section('content')
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Cities</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item"><a href="{{url('admin/console')}}">Console</a></li>
                <li class="breadcrumb-item active">Cities</li>
            </ol>
            @if (Session::has('success'))
                    <div class="alert alert-info">{{ Session::get('success') }}</div>
            @endif
            @if (Session::has('error'))
                <div class="alert alert-danger">{{ Session::get('error') }}</div>
            @endif
            <div class="card mb-4">
                <div class="card-header">
                    <i class="fas fa-table mr-1"></i>
                    Cities Lists
                </div>
                <div class="card-body">
                <div class="alert alert-info">Note: Cities are auto populated when you add restaurants (from the location details). You can only edit/delete cities here.</div>
                <br><br>
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Name</th>
                                    <th>Sort_Order</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i= 1; ?>@foreach($cities as $city)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td> {{$city->name}}</td>
                                    <td> {{$city->sort_order}}</td>
                                    <td>
                                        @if($city->status == 1)
                                        <a href="{{url('admin/manage-city_status/'.$city->id.'/status')}}" onclick="return confirm('Status will be In-Active!')">
                                            <span class="btn btn-success">Active</span>
                                        </a>
                                        @else 
                                        <a href="{{url('admin/manage-city_status/'.$city->id.'/status')}}" onclick="return confirm('Status will be Active!')">
                                            <span class="btn btn-secondary">
                                            In-Active</span>
                                        </a>
                                        @endif
                                        @if($city->papular_status == 1)
                                        <a href="{{url('admin/manage-city_status/'.$city->id.'/papular_status')}}" onclick="return confirm('City will be Not-Papular!')">
                                            <span class="btn btn-success">Papular</span>
                                        </a>
                                        @else 
                                        <a href="{{url('admin/manage-city_status/'.$city->id.'/papular_status')}}"  onclick="return confirm('City will be Papular!')">
                                            <span class="btn btn-secondary">
                                            Not Papular</span>
                                        </a>
                                        @endif
                                        <a href="{{ url('admin/manage-city/'.$city->id)}}"><span class="btn btn-secondary"><i class="fa fa-eye"></i> Details</span></a>
                                        <a href="{{ url('admin/manage-city/'.$city->id.'/edit')}}"><span class="btn btn-secondary"><i class="fa fa-edit"></i>edit</i></span></a>
                                        <!-- <br><br>
                                            <form action="{{url('admin/manage-city/'.$city->id) }}" class="delete" method="POST">
                                                {{ method_field('DELETE') }} {{csrf_field()}}
                                                <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure? Data will be deleted permanently!')"><i class="fa fa-trash"></i></button>
                                            </form> -->
                                        </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection