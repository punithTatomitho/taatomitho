@extends('layouts.site_layout')
@section('content')
<div class="container py-5">
        <!-- Row 1  -->
        @if(Auth::user()->hasRole('admin') or Auth::user()->hasRole('superadmin'))
        <div class="row">
            <div class="col-lg-4 mb-4">
                <div class="card h-100 border-0 shadow-sm">
                    <div class="card-body">
                        <div class="media">
                            <!-- <img src="..." class="mr-3" alt="..."> -->
                            <i class="fa fa-home mr-3 align-self-center fa-lg"></i>
                            <div class="media-body">
                                <h4 class="mb-0"> <a href="{{url('admin/kiosk')}}" class="text-decoration-none">Kiosk</a></h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 mb-4">
                <div class="card h-100 border-0 shadow-sm">
                    <div class="card-body">
                        <div class="media">
                            <!-- <img src="..." class="mr-3" alt="..."> -->
                            <i class="fa fa-cogs mr-3 fa-lg align-self-center" aria-hidden="true"></i>
                            <div class="media-body">
                                <h4 class="mb-0">
                                    <a href="{{url('admin/console')}}" class="text-decoration-none">Console</a></h4>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-lg-4 mb-4">
                <div class="card h-100 border-0 shadow-sm">
                    <div class="card-body">
                        <div class="media">
                            <!-- <img src="..." class="mr-3" alt="..."> -->
                            <i class="fa fa-tachometer mr-3 fa-lg align-self-center" aria-hidden="true"></i>
                            <div class="media-body">
                                <h4 class="mb-0"> <a href="{{url('admin/dashboard')}}" class="text-decoration-none">Dashboard</a> </h4>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- Row 2  -->
        <div class="row">
            <div class="col-lg-4 mb-4">
                <div class="card h-100 border-0 shadow-sm">
                    <div class="card-body">
                        <div class="media">
                            <!-- <img src="..." class="mr-3" alt="..."> -->
                            <i class="fa fa-mobile mr-3 fa-lg align-self-center" aria-hidden="true"></i>
                            <div class="media-body">
                                <h4 class="mb-0"> <a href="" class="text-decoration-none"> Your Orders </a></h4>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="col-lg-4 mb-4">
                <div class="card h-100 border-0 shadow-sm">
                    <div class="card-body">
                        <div class="media">
                            <!-- <img src="..." class="mr-3" alt="..."> -->
                            <i class="fa fa-list-alt mr-3 fa-lg align-self-center" aria-hidden="true"></i>
                            <div class="media-body">
                                <h4 class="mb-0"> <a href="" class="text-decoration-none"> Your Reservations </a> </h4>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="col-lg-4 mb-4">
                <div class="card h-100 border-0 shadow-sm">
                    <div class="card-body">
                        <div class="media">
                            <!-- <img src="..." class="mr-3" alt="..."> -->
                            <i class="fa fa-star-half-o mr-3 fa-lg align-self-center" aria-hidden="true"></i>
                            <div class="media-body">
                                <h4 class="mb-0"> <a href="" class="text-decoration-none"> Your Reviews </a></h4>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <!-- Row 3 -->
        <div class="row">
            <div class="col-lg-4 mb-4">
                <div class="card h-100 border-0 shadow-sm">
                    <div class="card-body">
                        <div class="media">
                            <!-- <img src="..." class="mr-3" alt="..."> -->
                            <i class="fa fa-user-plus mr-3  fa-lg" aria-hidden="true"></i>
                            <div class="media-body">
                                <h4 class="mb-0">
                                    <a href="" class="text-decoration-none"> Your Settings </a>
      	                        </h4>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
        @endif
        @if(Auth::user()->hasRole('csr') or Auth::user()->hasRole('rider'))
        <div class="row">
        <div class="col-lg-4 mb-4">
        </div>
            <div class="col-lg-4 mb-4">
            <div class="card h-100 border-0 shadow-sm">
                    <div class="card-body">
                        <div class="media">
                            <!-- <img src="..." class="mr-3" alt="..."> -->
                            <i class="fa fa-home mr-3 align-self-center fa-lg"></i>
                            <div class="media-body">
                                <h4 class="mb-0"> <a href="{{url('admin/dashboard')}}" class="text-decoration-none">Dashboard</a> </h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>
@endsection
