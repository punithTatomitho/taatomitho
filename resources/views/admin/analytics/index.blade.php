@extends('layouts.admin_layout')
@section('content')
	<main>
		<div class="container-fluid">
			<h1 class="mt-4">Analytic</h1>
			<ol class="breadcrumb mb-4">
				<li class="breadcrumb-item active">Analytic</li>
			</ol>
			<div class="card mb-4">
				<div class="card-header">
        			Gross Revenue
                	<span class="right">
                		<button class="btn btn-default mb-1">
                			<i class="fa fa-refresh"></i>
                		</button>
                	</span>
    			</div>
    			<div class="card-body">
    				<div class="row">
	    				<div class="col-md-4">
		    				<h6 class="metric-title mb-0">
		                        <i class="metric-icon"><svg width="14" height="20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14 20"><path fill="#4AA2E2" d="M7 3v2c-2.761424 0-5 2.238576-5 5 0 1.380712.559644 2.630712 1.464466 3.535534l-1.414213 1.414213C.783502 13.682997 0 11.932997 0 10c0-3.865993 3.134007-7 7-7zm4.949747 2.050253C13.216498 6.317003 14 8.067003 14 10c0 3.865993-3.134007 7-7 7v-2c2.761424 0 5-2.238576 5-5 0-1.380712-.559644-2.630712-1.464466-3.535534l1.414213-1.414213zM7 20l-4-4 4-4v8zM7 8V0l4 4-4 4z"></path></svg></i> Daily Revenue
		                    </h6><hr>
		                    <h4>NPR {{$today_revenue}}</h4>
		                    <small>Total amount</small>
		                    <p>NPR {{$total_amount}}</p>
		                    <p class="metric-stat-sm mb-0">
							{{$today_order_successful}} Successful orders
                            </p>
                            <p class="metric-stat-sm mb-0">
							{{$today_order_rejected}} Rejected orders
                            </p>
                            <p class="metric-stat-sm mb-0">
							{{$today_order_inprogress}} In-progress orders
                            </p>
		                </div>
		                <div class="col-md-4">
		                    <h6 class="metric-title mb-0">
		                        <i class="metric-icon"><svg width="20" height="12" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 12"><path fill="#4AA2E2" fill-rule="evenodd" d="M16.414214 4.707107L19.707107 8V0h-8L15 3.292893 9.707107 8.585786l-4-4L0 10.292893l1.414214 1.414214 4.292893-4.292893 4 4 6.707107-6.707107z"></path></svg></i> Monthly Revenue ({{$current_month}})
		                    </h6><hr>
		                    <h4>NPR {{$current_month_revenue}}</h4>
		                    <small>Total amount</small>
		                    <p>NPR {{$total_amount}}</p>
		                    <p class="metric-stat-sm mb-0">
                                {{$current_month_successful}} Successful orders
                            </p>
                            <p class="metric-stat-sm mb-0">
                                {{$current_month_rejected}} Rejected orders
                            </p>
                            <p class="metric-stat-sm mb-0">
                                {{$current_month_inprogress}} In-progress orders
                            </p>
		                </div>
		                <div class="col-md-4">
		                    <h6 class="metric-title mb-0">
		                        <i class="metric-icon"><svg width="20" height="12" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 12"><path fill="#4AA2E2" fill-rule="evenodd" d="M16.414214 4.707107L19.707107 8V0h-8L15 3.292893 9.707107 8.585786l-4-4L0 10.292893l1.414214 1.414214 4.292893-4.292893 4 4 6.707107-6.707107z"></path></svg></i> Yearly Revenue ({{$current_year}})
		                    </h6><hr>
		                    <h4>NPR {{$year_revenue}}</h4>
		                    <small>Total amount</small>
		                    <p>NPR {{$total_amount}}</p>
		                    <p class="metric-stat-sm mb-0">
                                {{$year_successful}} Successful orders
                            </p>
                            <p class="metric-stat-sm mb-0">
                                {{$year_rejected}} Rejected orders
                            </p>
                            <p class="metric-stat-sm mb-0">
                                {{$year_inprogress}} In-progress orders
                            </p>
		                </div>
		            </div>
    			</div>
            </div>
			<div class="card mb-4">
				<div class="card-header">
					<i class="fas fa-table mr-1"></i>
					Restaurants List
					<form action="{{url('admin/analytics')}}" style="float: right;margin-right: 73%;">
						<select name="hotel_id" class="form-control" onchange="this.form.submit()">
							<option value="">Select</option>
							@foreach($hotels as $hotel)
								<option value="{{$hotel->hotelId}}" {{ $selectedHotel == $hotel->hotelId ? "selected" : "" }}>{{$hotel->hotelName}}</option>
							@endforeach
						</select>
					</form>
				</div>
				<div class="card-body">
					@if($selectedHotel == '')
						<h5>Select restaurant to see the bills and generate bill</h5>
					@else
						<a href="{{url('admin/generate-hotel-bill', $selectedHotel)}}" class="btn btn-primary btn-sm float-right" >Generate Bill</a>
						@if(count($previousBills) > 0)
							<div class="table-responsive">
		                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
		                            <thead>
		                                <tr>
		                                    <th>No</th>
		                                    <th>Name</th>
		                                    <th>From date and To date</th>
		                                    <th>Bill generated on</th>
		                                    <th>Action</th>
		                                </tr>
		                            </thead>
		                            <tbody>
		                                <?php $i= 1; ?>
		                                @foreach($previousBills as $bill)
		                                <tr>
		                                    <td>{{$i++}}</td>
		                                    <td>{{$bill->hotel_name}}</td>
		                                    <td>{{$bill->bill_from_date}} To {{$bill->bill_to_date}}</td>
		                                    <td>{{$bill->created_at}}</td>
		                                    <td><a href="{{url('admin/view-hotel-bill', $bill->hotel_bill_id)}}">View bill</a></td>
		                                </tr>
		                                @endforeach
		                            </tbody>
		                        </table>
		                    </div>
		                @else
                        	<p class="text-danger">No bills generated till date</p>
                        @endif
					@endif
				</div>
			</div>
		</div>
	</main>
@endsection