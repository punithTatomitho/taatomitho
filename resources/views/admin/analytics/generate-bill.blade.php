@extends('layouts.admin_layout')
@section('content')
	<main>
		<div class="container-fluid">
			<div class="card mb-4">
				<div class="card-header">
					<div class="center" style="text-align: center;margin: auto;width: 60%;padding: 10px;">
						<img alt="logo" src="{{asset('img/logo-color.png')}}" style="width: 20%;">
						<h3>Innerworld Enterprises Pvt Ltd</h3>
						<p>Mitrapark, Chabahil, Kathmandu</p>
					</div>
					<h3>{{$hotel_detail->name}}</h3>
					<p class="metric-stat-sm mb-0">{{$hotel_detail->address}}</p>
					<p class="metric-stat-sm mb-0">{{$hotel_detail->phone}}</p>
					<p class="metric-stat-sm mb-0">{{$hotel_detail->email}}</p>
					<span style="float: right;">
						<p class="metric-stat-sm mb-0">From: {{$fromDate}}</p>
						<p class="metric-stat-sm mb-0">To: {{$toDate}}</p>
					</span>
				</div>
				<div class="card-body">
					@if(count($orders) > 0)
						<div class="table-responsive">
	                        <table class="table table-bordered" width="100%" cellspacing="0">
	                            <thead style="background: indianred;">
	                                <tr>
	                                    <th>No</th>
	                                    <th>Date</th>
	                                    <th>Status</th>
	                                    <th>Order Amount</th>
	                                    <th>Service Charge</th>
	                                    <th>Amount</th>
	                                </tr>
	                            </thead>
	                            <tbody>
	                                <?php $i= 1; $total = 0; ?>
	                                @foreach($orders as $order)
	                                @php $total += $order->final_amount; @endphp
	                                <tr>
	                                    <td>{{$i++}}</td>
	                                    <td>{{$order->order_date}}</td>
	                                    <td>{{$order->delivery_status}}</td>
	                                    <td>NPR {{number_format($order->final_amount, 2)}}</td>
	                                    <td>0.00</td>
	                                    <td>NPR {{number_format($order->final_amount, 2)}}</td>
	                                </tr>
	                                @endforeach
	                                <tr>
	                                	<td class="no-border"></td>
	                                	<td class="no-border"></td>
	                                	<td class="no-border"></td>
	    								<td class="no-border"></td>
	    								<td><strong>Total</strong></td>
	    								<td>NPR {{number_format($total, 2)}}</td>
	                                </tr>
	                                <tr>
	                                	<td class="no-border"></td>
	                                	<td class="no-border"></td>
	                                	<td class="no-border"></td>
	    								<td class="no-border"></td>
	    								<td><strong>Commission ( {{$hotel_detail->hotel_registerdetails->commission}} %)</strong></td>
	    								@php $cAmount = ($total*$hotel_detail->hotel_registerdetails->commission)/100; @endphp
	    								<td>NPR {{number_format($cAmount, 2)}}</td>
	                                </tr>
	                                <tr>
	                                	<td class="no-border"></td>
	                                	<td class="no-border"></td>
	                                	<td class="no-border"></td>
	    								<td class="no-border"></td>
	    								<td><strong>Pay out</strong></td>
	    								<td>NPR {{number_format($total-$cAmount, 2)}}</td>
	                                </tr>
	                            </tbody>
	                        </table>
	                    </div>
	                @else
                    	<p class="text-danger">Don't have recent orders</p>
                    @endif
                    <br>
                    <div class="row">
                    	<div class="col-md-6">
	                    	<h5>Received by:</h5>
	                    	<p>(Name & Signature)</p>
	                    </div>
	                    <div class="col-md-6">
	                    	<span style="float: right;">
		                    	<h5>Paid by:</h5>
		                    	<p>Authorised Signatory</p>
		                    </span>
	                    </div>
                    </div>
				</div>
			</div>
		</div>
	</main>
@endsection