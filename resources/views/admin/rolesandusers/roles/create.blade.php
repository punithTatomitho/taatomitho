@extends('layouts.kiosk_layout')
@section('content')
<main>
    <div class="container-fluid">
        <h1 class="mt-4">Role</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="{{url('admin/manage-role')}}">Role</a></li>
            <li class="breadcrumb-item active">Add Role</li>
        </ol>
        <div class="row">
            <div class="col-xl-12">
                <div class="card mb-4">
                    <div class="card-header">
                        Add New Role
                    </div>
                    <div class="card-body">
                        <form action="{{ url('admin/manage-role')}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <label for="name">Role Name</label>
                                <div class=" form-group">
                                    <div class="">
                                        <input type="text" name="name" class="form-control"
                                            placeholder="Please Enter Role Name Here" required>
                                    </div>
                                </div>
                                <label for="display_name">Display Name</label>
                                <div class="form-group">
                                    <div class="">
                                        <input type="text" name="display_name" class="form-control"
                                            placeholder="Please Enter Display Name Here" required>
                                    </div>
                                </div>
                                <label for="display_name">Description</label>
                                <div class="form-group">
                                    <div class="">
                                        <input type="text" name="description" class="form-control"
                                            placeholder="Please Enter Description Here" required>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary btn-round px-5"> Submit</button>
                                &nbsp;&nbsp;
                                <a href="{{ url('admin/manage-role')}}" class="btn btn-danger btn-round px-5">Cancel</a>
                        </form>
                        </form>
                    </div>
                </div>
            </div>
        </div>
</main>
@endsection