@extends('layouts.kiosk_layout')
@section('content')
<main>
    <div class="container-fluid">
        <h1 class="mt-4">Role</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="{{url('admin/kiosk')}}">kiosk</a></li>
            <li class="breadcrumb-item active">Role</li>
        </ol>
        @if (Session::has('success'))
        <div class="alert alert-info">{{ Session::get('success') }}</div>
        @endif
        @if (Session::has('error'))
        <div class="alert alert-danger">{{ Session::get('error') }}</div>
        @endif
        <div class="card mb-4">
            <div class="card-header">
                <i class="fas fa-table mr-1"></i>
                Role Lists
            </div>
            <div class="card-body">
                <a href="{{ url('admin/manage-role/create')}}" class="btn btn-primary btn-sm float-right">Add
                    Role</a>
                <br><br>
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Role </th>
                                <th>Display Name</th>
                                <th>Description</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i=1;?> @foreach($roles as $role)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$role->name}}</td>
                                <td>{{$role->display_name}}</td>
                                <td>{{$role->description}}</td>
                                <td><a href="{{ url('admin/manage-role/'.$role->id.'/edit')}}"><span class="btn btn-dark"><i class="fa fa-edit"></i></span></a>
                                <br><br>
								<!-- <form action="{{url('admin/manage-role/'.$role->id)}}" class="delete" method="POST">
												{{ method_field('DELETE') }}{{csrf_field()}}
												<button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
									</form> -->
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection