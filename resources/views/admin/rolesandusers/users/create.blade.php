@extends('layouts.kiosk_layout')
@section('content')
<main>
    <div class="container-fluid">
        <h1 class="mt-4">User</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="{{url('admin/manage-users')}}">manage Users</a></li>
            <li class="breadcrumb-item active">Add User</li>
        </ol>
        <div class="row">
            <div class="col-xl-12">
                <div class="card mb-4">
                    <div class="card-header">
                        Add New User
                    </div>
                    <div class="card-body">
                        <form action="{{ url('admin/manage-users/')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <label for="name">Role Name</label>
                            <div class=" form-group">
                                <select class="form-control" name="role_id" required="">
                                    <option value="">-- Please select --</option>
                                    @foreach($roles as $role)
                                    @if($role->name != 'user')
                                    <option value="{{$role->id}}">{{$role->name}}</option>
                                    @endif
                                    @endforeach
                                </select>
                            </div>
							<div class="form-group mt-10">
								<!-- <label class="control-label">Profile Image: (640 x 400) <small><a target="_blank" href="https://www.figma.com/">Crop image</a> - <a target="_blank" href="https://squoosh.app/">Compress image</a></small></label><br> -->
								<label>Profile Image</label>
								<input type="file" class="form-control" name="profile_image" placeholder="profile_image"  value="{{old('profile_image')}}">
								@if ($errors->has('profile_image'))
									<span class="text-danger">{{ $errors->first('profile_image') }}</span>
								@endif
							</div>
                            <label for="display_name">Name</label>
                            <div class="form-group">
                                <div class="">
                                    <input type="text" name="name" class="form-control"
                                        placeholder="Please Enter Your Name Here" value="{{old('name')}}" required>
                                </div>
                            </div>
                            @error('name')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror

                            <label for="display_name">phone_number</label>
                            <div class="form-group">
                                <div class="">
                                    <input type="text" name="phone_number" class="form-control"
                                        placeholder="Please Enter Your phone_number Here" value="{{old('phone_number')}}">
                                </div>
                            </div>
                            @error('phone_number')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror

                            <label for="display_name">Email</label>
                            <div class="form-group">
                                <div class="">
                                    <input type="text" name="email" class="form-control"
                                        placeholder="Please Enter Your Email Here" value="{{old('email')}}" required>
                                </div>
                            </div>
                            @error('email')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror

                            <label for="display_name">Password</label>
                            <div class="form-group">
                                <div class="">
                                    <input type="password" name="password" class="form-control"
                                        placeholder="Please Enter Password Here" value="{{old('password')}}" required>
                                </div>
                            </div>

                            <label for="display_name">Confirm Password</label>
                            <div class="form-group">
                                <div class="">
                                    <input type="password" class="form-control" name="password_confirmation" required
                                        autocomplete="new-password" value="{{old('password_confirmation')}}"
                                        placeholder="Confirm Password">
                                </div>
                            </div>
                            @error('password')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                            <div class="form-group">
							<div class="demo-heading">Status</div>
							<div class="icheck-material-primary icheck-inline">
								<input type="radio" id="inline-radio-primary" name="status" value="1" checked=""/>
								<label for="inline-radio-primary">Active</label>
							</div>
							<div class="icheck-material-info icheck-inline">
								<input type="radio" id="inline-radio-info" name="status" value="0" />
								<label for="inline-radio-info">In-Active</label>
							</div>
						</div>
                            <button type="submit" class="btn btn-primary btn-round px-5"> Submit</button>
                            &nbsp;&nbsp;
                            <a href="{{ url('admin/manage-users')}}" class="btn btn-danger btn-round px-5">Cancel</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
</main>
@endsection