@extends('layouts.kiosk_layout')
@section('content')
<main>
    <div class="container-fluid">
        <h1 class="mt-4">User</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="{{url('admin/kiosk')}}">kiosk</a></li>
            <li class="breadcrumb-item active">User</li>
        </ol>
        @if (Session::has('success'))
        <div class="alert alert-info">{{ Session::get('success') }}</div>
        @endif
        @if (Session::has('error'))
        <div class="alert alert-danger">{{ Session::get('error') }}</div>
        @endif
        <div class="card mb-4">
            <div class="card-header">
                <i class="fas fa-table mr-1"></i>
                User Lists
            </div>
            <div class="card-body">
                <a href="{{ url('admin/manage-users/create')}}" class="btn btn-primary btn-sm float-right">Add
                    Users</a>
                <br><br>
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>No</th>
								<th>Profile Image</th>
                                <th>Name </th>
                                <th>Email</th>
                                <th>Contact Number</th>
                                <th>Role</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i=1;?> @foreach($users as $user)
                            @if($user->roles[0]['name'] != 'user')
                            <tr>
                                <td>{{$i++}}</td>
								
								<td style="width:20%">@if(isset($user->user_details->profile_image))<img class="img-fluid mr-3 float-left" style="width: 60%" alt="{{$user->user_details->profile_image}}" src="{{ URL::to('storage/profile_image',$user->user_details->profile_image) }}">@endif</td>
                                <td>{{$user->name}}</td>
                                <td>{{$user->email}}</td>
                                <td>@if(isset($user->user_details->phone_number)){{$user->user_details->phone_number}}@endif</td>
                                <td>{{$user->roles[0]['name']}}</td>
                                <td><a href="{{ url('admin/manage-users/'.$user->id.'/edit')}}"><span class="btn btn-dark"><i class="fa fa-edit"></i></span></a>
								<br><br>
                                @if($user->user_details->status == 1)
                                <a href="{{url('admin/manage-users_status/'.$user->id.'/status')}}" onclick="return confirm('Status will be In-Active And User will be deleted !')">
                                    <span class="btn btn-success">Active</span>
                                </a>
                                @else 
                                <a href="{{url('admin/manage-users_status/'.$user->id.'/status')}}" onclick="return confirm('Status will be Active!')">
                                    <span class="btn btn-danger">
                                    In-Active</span>
                                </a>
                                @endif
                                    <!-- <form action="{{url('admin/manage-users/'.$user->id)}}" class="delete"
                                        method="POST">
                                        {{ method_field('DELETE') }}{{csrf_field()}}
                                        <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                    </form> -->
                                </td>
                            </tr>
                            @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection