@extends('layouts.kiosk_layout')
@section('content')
<main>
    <div class="container-fluid">
        <h1 class="mt-4">User</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="{{url('admin/manage-users')}}">User</a></li>
            <li class="breadcrumb-item active">Edit User</li>
        </ol>
        <div class="row">
            <div class="col-xl-12">
                <div class="card mb-4">
                    <div class="card-header">
                        Edit User
                    </div>
                    <div class="card-body">
					<form action="{{ url('admin/manage-users/'.$user->id)}}" method="post" enctype="multipart/form-data">
							@csrf
                            {{method_field('PATCH')}}
							<label for="name">Role Name</label>
							<div class=" form-group">
								<select class="form-control" name="role_id" required="">
									<!-- <option value="">-- Please select --</option> -->
									@foreach($roles as $role)
					                    <option value="{{$role->id}}" {{$user->roles[0]['name'] == $role->name ? 'selected':''}}>{{$role->name}}</option>
					                @endforeach
								</select>
							</div>
							<label for="display_name">Name</label>
							<div class="form-group">
								<div class="">
									<input type="text" name="name" class="form-control"  placeholder="Please Enter Name Here" value="{{$user->name}}" required>
								</div>
							</div>
                            @error('name')
									<div class="alert alert-danger">{{ $message }}</div>
							@enderror
							<div class="form-group">
								<!-- <label class="control-label">Profile Image: (640 x 400) <small><a target="_blank" href="https://www.figma.com/">Crop image</a> - <a target="_blank" href="https://squoosh.app/">Compress image</a></small></label><br> -->
								<label>Profile Image</label>
								<input type="file" class="form-control" name="profile_image" placeholder="profile_image"  value="{{old('profile_image')}}">
								@if ($errors->has('profile_image'))
									<span class="text-danger">{{ $errors->first('profile_image') }}</span>
								@endif
							</div>
                            
							<label for="display_name">phone_number</label>
							<div class="form-group">
								<div class="">
									<input type="text" name="phone_number" class="form-control"  placeholder="Please Enter Your phone_number Here" value="{{$user->user_details->phone_number}}">
								</div>
							</div>
							@error('phone_number')
									<div class="alert alert-danger">{{ $message }}</div>
							@enderror

						<label for="display_name">Email</label>
						<div class="form-group">
							<div class="">
								<input type="text" name="email" class="form-control"  placeholder="Please Enter Your Email Here" value="{{$user->email}}"  required>
							</div>
						</div>
                        @error('email')
									<div class="alert alert-danger">{{ $message }}</div>
							@enderror
                        
                        <label for="display_name">New Password</label>
						<div class="form-group">
							<div class="">
								<input type="password" name="password" class="form-control"   placeholder="Please Enter Password Here">
							</div>
						</div>
                        <label for="display_name">Confirm Password</label>
						<div class="form-group">
							<div class="">
								<input type="password" class="form-control"   name="password_confirmation" autocomplete="new-password" placeholder="Confirm Password">
							</div>
						</div>
						@error('password')
								<div class="alert alert-danger">{{ $message }}</div>
						@enderror
						<div class="form-group">
							<div class="demo-heading">Status</div>
							<div class="icheck-material-primary icheck-inline">
								<input type="radio" id="inline-radio-primary" name="status" value="1" {{ $user->user_details->status == 1 ? 'checked' : ''}}/>
								<label for="inline-radio-primary">Active</label>
							</div>
							<div class="icheck-material-info icheck-inline">
								<input type="radio" id="inline-radio-info" name="status" value="0" {{ $user->user_details->status == 0 ? 'checked' : ''}}/>
								<label for="inline-radio-info">In-Active</label>
							</div>
						</div>
						<button type="submit" class="btn btn-primary btn-round px-5"> Submit</button>
						&nbsp;&nbsp;
						<a href="{{ url('admin/manage-users')}}" class="btn btn-danger btn-round px-5">Cancel</a>
					</form>
                    </div>
                </div>
            </div>
        </div>
</main>
@endsection