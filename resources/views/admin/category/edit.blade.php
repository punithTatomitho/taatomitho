@extends('layouts.admin_layout')
@section('content')
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Category</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item"><a href="{{url('admin/manage-catrgory')}}">Category</a></li>
                <li class="breadcrumb-item active">Edit Category</li>
            </ol>
            <div class="row">
                <div class="col-xl-12">
                <div class="card mb-4">
                    <div class="card-header">
                        Edit Category
                    </div>
                    <div class="card-body">
                        <form action="{{ url('admin/manage-category/'.$category->id)}}" method="POST" enctype="multipart/form-data">
                        @csrf
						{{ method_field('PUT') }}
                            <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label> Category Name</label>
                                            <input type="text" class="form-control" name="category_name" placeholder="category_name" required
                                            value="{{$category->category_name}}">
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-success">
                                                <i class="feather-send"></i> SAVE
                                            </button>
                                            <a  class="btn btn-danger" href="{{url('admin/manage-category')}}"> Cancel</a>
                                        <div>
                                    </div>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection