@extends('layouts.admin_hoteloptions_layout')
@section('content')
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Gallery</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item"><a href="{{url('admin/manage-hotel-gallery')}}">Gallery</a></li>
                <li class="breadcrumb-item active"> {{$hotel->name}} / Gallery</li>
            </ol>
            <div class="row">
                <div class="col-xl-12">
                <div class="card mb-4">
                    <div class="card-header">
                        Add New Gallery
                    </div>
                    <div class="card-body">
                        <form action="{{ url('admin/manage-hotel-gallery')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                            <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Sort_order</label>
                                            <input type="number" class="form-control" name="sort_order" placeholder="0" value="0">
                                            <input type="text" class="form-control" name="hotel_id" value="{{$hotel->id}}" hidden>
                                        </div>
                                        <div class="form-group mt-10">
                                                <label class="control-label">Image: (640 x 400) <small><a target="_blank" href="https://www.figma.com/">Crop image</a> - <a target="_blank" href="https://squoosh.app/">Compress image</a></small></label><br>
                                                <label>Image</label>
                                                <input type="file" class="form-control" name="image" placeholder="image"  value="{{old('image')}}">
                                                @if ($errors->has('image'))
                                                    <span class="text-danger">{{ $errors->first('image') }}</span>
                                                @endif
                                            </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-success">
                                                <i class="feather-send"></i> SAVE
                                            </button>
                                            <a  class="btn btn-danger" href="{{url('admin/manage-hotel-category_index/'.$hotel->id)}}"> Cancel</a>
                                        <div>
                                    </div>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection