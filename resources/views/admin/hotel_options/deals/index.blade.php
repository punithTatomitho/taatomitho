@extends('layouts.admin_hoteloptions_layout')
@section('content')
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Deals</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item"><a href="{{url('admin/console')}}">Console</a></li>
                <li class="breadcrumb-item active"> {{$hotel->name}} / Deals</li>
            </ol>
            @if (Session::has('success'))
                    <div class="alert alert-info">{{ Session::get('success') }}</div>
            @endif
            @if (Session::has('error'))
                <div class="alert alert-danger">{{ Session::get('error') }}</div>
            @endif
            <div class="card mb-4">
                <div class="card-header">
                    <i class="fas fa-table mr-1"></i>
                    Deals Lists
                </div>
                <div class="card-body">
                <a href="{{ url('admin/manage-hotel-deals_create/'.$hotel->id)}}" class="btn btn-primary btn-sm float-right" >Add Deals</a>
                <br><br>
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Name</th>
                                    <th>Sort Order</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i= 1; ?>@foreach($deals as $deal)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td> {{$deal->name}}</td>
                                    <td>{{$deal->sot_order}}</td>
                                    <td>
                                        @if($deal->status == 1)
                                        <a href="{{url('admin/manage-hotel-deals_status/'.$deal->id.'/status')}}" onclick="return confirm('Status will be In-Active!')">
                                            <span class="btn btn-primary">Active</span>
                                        </a>
                                        @else 
                                        <a href="{{url('admin/manage-hotel-deals_status/'.$deal->id.'/status')}}" onclick="return confirm('Status will be Active!')">
                                            <span class="btn btn-danger">
                                            In-Active</span>
                                        </a>
                                        @endif
                                        @if($deal->promotion_status == 'on')
                                        <a href="{{url('admin/manage-hotel-deals_status/'.$deal->id.'/promotion_status')}}" onclick="return confirm('Deal will be Not-Promote!')">
                                            <span class="btn btn-success">Promotion</span>
                                        </a>
                                        @elseif($deal->promotion_sttaus == 'off' || $deal->promotion_sttaus == null)
                                        <a href="{{url('admin/manage-hotel-deals_status/'.$deal->id.'/promotion_status')}}" onclick="return confirm('Deal will be Promote!')">
                                            <span class="btn btn-secondary">
                                            No Promotion</span>
                                        </a>
                                        @endif
                                        <a href="{{ url('admin/manage-hotel-deals_show/'.$deal->hotel_id.'/'.$deal->id)}}"><span class="btn btn-secondary"><i class="fa fa-eye"></i> Details</span></a>
                                        <!-- <br><br>
                                            <form action="{{url('admin/manage-hotel-deals/'.$deal->id) }}" class="delete" method="POST">
                                                {{ method_field('DELETE') }} {{csrf_field()}}
                                                <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure? Data will be deleted permanently!')"><i class="fa fa-trash"></i></button>
                                            </form> -->
                                        </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection