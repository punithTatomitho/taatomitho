@extends('layouts.admin_hoteloptions_layout')
@section('content')
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Deals</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item"><a href="{{url('admin/manage-hotel-deals')}}">Deals</a></li>
                <li class="breadcrumb-item active"> {{$hotel->name}} / Deals</li>
            </ol>
            <div class="row">
                <div class="col-xl-12">
                <div class="card mb-4">
                    <div class="card-header">
                        Add New Deals
                    </div>
                    <div class="card-body">
                        <form action="{{ url('admin/manage-hotel-deals')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                            <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Name</label>
                                            <input type="text" class="form-control" name="name" placeholder="name" required>
                                            <input type="text" class="form-control" name="hotel_id" value="{{$hotel->id}}" hidden>
                                        </div>
                                        <div class="form-group">
                                            <label>Sort_order</label>
                                            <input type="number" class="form-control" name="sort_order" placeholder="sort_order" value="0">
                                        </div>
                                        <div class="form-group">
                                            <label>description</label>
                                            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="description" 
								               >{{old('description')}}</textarea>
                                        </div>
                                        <div class="form-group mt-10">
                                            <label class="control-label">Item Image: (400 x 400) <small><a target="_blank" href="https://www.figma.com/">Crop image</a> - <a target="_blank" href="https://squoosh.app/">Compress image</a></small></label><br>
                                            <label>Image</label>
                                            <input type="file" class="form-control" name="image" placeholder="image"  value="{{old('image')}}">
                                            @if ($errors->has('image'))
                                                <span class="text-danger">{{ $errors->first('image') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Price</label>
                                            <input type="number" class="form-control" name="price" placeholder="item price" value="10">
                                        </div>
                                        <div class="form-group">
                                            <label>Tax</label>
                                            <input type="number" class="form-control" name="tax" placeholder="amount tax" value="0">
                                        </div>
                                        <div class="form-group">
                                        <div class="demo-heading">Promote item</div>
                                            <div class="icheck-material-primary icheck-inline">
                                                <input type="radio" id="inline-radio-primary" name="promotion_status" value="on" checked=""/>
                                                <label for="inline-radio-primary">ON</label>
                                            </div>
                                            <div class="icheck-material-info icheck-inline">
                                                <input type="radio" id="inline-radio-info" name="promotion_status" value="off" />
                                                <label for="inline-radio-info">OFF</label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                        <table class="table table-bordered" id="dynamicTable">  
                                            <tr>
                                                <th>Category</th>
                                                <th>Quanity</th>
                                                <th>Action</th>
                                            </tr>
                                            <tr>  
                                                <td><select class="custom-select" name="addmore[0][category_id]">
                                                    <option value="">Select category </option>
                                                        @foreach($categories as $category)
                                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                                        @endforeach
                                                    </select></td>  
                                                <td><input type="text" name="addmore[0][quantity]" placeholder="Enter your Qty" class="form-control" /></td>  
                                                <td><button type="button" name="add" id="add" class="btn btn-success">Add More</button></td>  
                                            </tr>  
                                        </table>
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-success">
                                                <i class="feather-send"></i> SAVE
                                            </button>
                                            <a  class="btn btn-danger" href="{{url('admin/manage-hotel-category_index/'.$hotel->id)}}"> Cancel</a>
                                        <div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript">
   
   var i = 0;
      
   $("#add").click(function(){
  
       ++i;
  
       $("#dynamicTable").append('<tr><td><select class="custom-select" name="addmore['+i+'][category_id]"><option value="">Select category </option>@foreach($categories as $category)<option value="{{$category->id}}">{{$category->name}}</option>@endforeach</select></td><td><input type="text" name="addmore['+i+'][quantity]" placeholder="Enter your Qty" class="form-control" /></td><td><button type="button" class="btn btn-danger remove-tr">Remove</button></td></tr>');
   });
  
   $(document).on('click', '.remove-tr', function(){  
        $(this).parents('tr').remove();
   });  
  
</script>
@endsection