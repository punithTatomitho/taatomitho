@extends('layouts.admin_hoteloptions_layout')
@section('content')
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Timings</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item"><a href="{{url('admin/consloe/')}}">Console</a></li>
                <li class="breadcrumb-item active"> {{$hotel->name}} / Timings</li>
            </ol>
            @if (Session::has('success'))
                    <div class="alert alert-info">{{ Session::get('success') }}</div>
            @endif
            @if (Session::has('error'))
                <div class="alert alert-danger">{{ Session::get('error') }}</div>
            @endif
            <div class="row">
                <div class="col-xl-12">
                <div class="card mb-4">
                    <div class="card-header">
                        Add Opening Timings
                    </div>
                    <div class="card-body">
                        <form action="{{ url('admin/manage-hotel-timings')}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <input type="text" class="form-control" name="hotel_id" value="{{$hotel->id}}" hidden>
                            </div>
                                <div class="form-group">
                                    <table class="table table-bordered" id="dynamicTable">  
                                        <tr>
                                            <th>Monday start time</th>
                                            <th>Monday end time</th>
                                            <th>Action</th>
                                        </tr>
                                        <tr>  
                                            <td>
                                                <input type="time" name="addmore[0][monday_start_at]" placeholder="Enter monday_start_at" class="form-control" required value="{{$timings->monday_start_at}}"/>
                                            </td>  
                                            <td> <input type="time" name="addmore[0][monday_end_at]" placeholder="Enter monday_end_at" class="form-control" required  value="{{$timings->monday_end_at}}"/></td>  
                                            <!-- <td><button type="button" name="add" id="add" class="btn btn-success">Add More</button></td>   -->
                                        </tr>  
                                    </table>
                                </div>
                                <div class="form-group">
                                <table class="table table-bordered" id="dynamicTable2">
                                    <tr>
                                    <th>Tuesday start time</th>
                                    <th>Tuesday end time</th>
                                    <th>Action</th>
                                    </tr> 
                                    <tr>
                                    <td>
                                        <input type="time" name="addmore[0][tuesday_start_at]" placeholder="Enter tuesday_start_at" class="form-control" required  value="{{$timings->tuesday_start_at}}"/>
                                        </td>
                                        <td><input type="time" name="addmore[0][tuesday_end_at]" placeholder="Enter tuesday_end_at" class="form-control" required value="{{$timings->tuesday_end_at}}"/></td>
                                        <!-- <td><button type="button" name="add" id="add2" class="btn btn-success">Add More</button></td>   -->
                                    </tr>
                                </table>
                                <div>
                                <div class="form-group">
                                <table class="table table-bordered" id="dynamicTable3">
                                    <tr>
                                    <th>Wednesday start time</th>
                                    <th>Wednesday end time</th>
                                    <th>Action</th>
                                    </tr> 
                                    <tr>
                                    <td>
                                        <input type="time" name="addmore[0][wednesday_start_at]" placeholder="Enter wednesday_start_at" class="form-control" required value="{{$timings->wednesday_start_at}}"/>
                                    </td>
                                        <td><input type="time" name="addmore[0][wednesday_end_at]" placeholder="Enter wednesday_end_at" class="form-control" required value="{{$timings->wednesday_end_at}}"/></td>
                                        <!-- <td><button type="button" name="add" id="add3" class="btn btn-success">Add More</button></td>   -->
                                    </tr>
                                </table>
                                <div>
                                <div class="form-group">
                                <table class="table table-bordered" id="dynamicTable4">
                                    <tr>
                                    <th>Thursday start time</th>
                                    <th>Thursday end time</th>
                                    <th>Action</th>
                                    </tr> 
                                    <tr>
                                        <td>
                                            <input type="time" name="addmore[0][thursday_start_at]" placeholder="Enter thursday_start_at" class="form-control" required value="{{$timings->thursday_start_at}}"/>
                                        </td>
                                        <td><input type="time" name="addmore[0][thursday_end_at]" placeholder="Enter thursday_end_at" class="form-control" required value="{{$timings->thursday_end_at}}"/></td>
                                        <!-- <td><button type="button" name="add" id="add4" class="btn btn-success">Add More</button></td>   -->
                                    </tr>
                                </table>
                                <div>
                                <div class="form-group">
                                <table class="table table-bordered" id="dynamicTable5">
                                    <tr>
                                    <th>Friday start time</th>
                                    <th>Friday end time</th>
                                    <th>Action</th>
                                    </tr> 
                                    <tr>
                                        <td>
                                            <input type="time" name="addmore[0][friday_start_at]" placeholder="Enter friday_start_at" class="form-control" required value="{{$timings->friday_start_at}}"/>
                                        </td>
                                        <td><input type="time" name="addmore[0][friday_end_at]" placeholder="Enter friday_end_at" class="form-control" required value="{{$timings->friday_end_at}}"/></td>
                                        <!-- <td><button type="button" name="add" id="add5" class="btn btn-success">Add More</button></td>   -->
                                    </tr>
                                </table>
                                <div>
                                <div class="form-group">
                                <table class="table table-bordered" id="dynamicTable6">
                                    <tr>
                                    <th>Saturday start time</th>
                                    <th>Saturday end time</th>
                                    <th>Action</th>
                                    </tr> 
                                    <tr>
                                        <td>
                                            <input type="time" name="addmore[0][saturday_start_at]" placeholder="Enter saturday_start_at" class="form-control" required value="{{$timings->saturday_start_at}}"/>
                                        </td>
                                        <td><input type="time" name="addmore[0][saturday_end_at]" placeholder="Enter saturday_end_at" class="form-control" required value="{{$timings->saturday_end_at}}"/></td>
                                        <!-- <td><button type="button" name="add" id="add6" class="btn btn-success">Add More</button></td>   -->
                                    </tr>
                                </table>
                                <div>
                                <div class="form-group">
                                <table class="table table-bordered" id="dynamicTable7">
                                    <tr>
                                    <th>Sunday start time</th>
                                    <th>Sunday end time</th>
                                    <th>Action</th>
                                    </tr> 
                                    <tr>
                                        <td>
                                            <input type="time" name="addmore[0][sunday_start_at]" placeholder="Enter sunday_start_at" class="form-control" required value="{{$timings->sunday_start_at}}"/>
                                        </td>
                                        <td><input type="time" name="addmore[0][sunday_end_at]" placeholder="Enter sunday_end_at" class="form-control" required value="{{$timings->sunday_end_at}}"/></td>
                                        <!-- <td><button type="button" name="add" id="add7" class="btn btn-success">Add More</button></td>   -->
                                    </tr>
                                </table>
                                <div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">
                                        <i class="feather-send"></i> Update
                                    </button>
                                <div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript">

   var i = 0;
      
   $("#add").click(function(){
  
       ++i;
  
       $("#dynamicTable").append('<tr><td><input type="time" name="addmore['+i+'][monday_shift_]" placeholder="Enter monday_shift_" class="form-control" /></td><td><input type="time" name="addmore['+i+'][monday_end_at]" placeholder="Enter monday_end_at" class="form-control" /></td><td><button type="button" class="btn btn-danger remove-tr">Remove</button></td></tr>');
   });
  
   $(document).on('click', '.remove-tr', function(){  
        $(this).parents('tr').remove();
   });
   
//    tuesday table
   $("#add2").click(function(){
  
  ++i;

  $("#dynamicTable2").append('<tr><td><input type="time" name="addmore['+i+'][tuesday_shift_]" placeholder="Enter tuesday_shift_" class="form-control" /></td><td><input type="time" name="addmore['+i+'][tuesday_end_at]" placeholder="Enter tuesday_end_at" class="form-control" /></td><td><button type="button" class="btn btn-danger remove-tr2">Remove</button></td></tr>');
});

$(document).on('click', '.remove-tr2', function(){  
   $(this).parents('tr').remove();
}); 
  
  //    wednesday table
  $("#add3").click(function(){
  
  ++i;

  $("#dynamicTable3").append('<tr><td><input type="time" name="addmore['+i+'][wednesday_shift_]" placeholder="Enter wednesday_shift_" class="form-control" /></td><td><input type="time" name="addmore['+i+'][wednesday_end_at]" placeholder="Enter wednesday_end_at" class="form-control" /></td><td><button type="button" class="btn btn-danger remove-tr3">Remove</button></td></tr>');
});

$(document).on('click', '.remove-tr3', function(){  
   $(this).parents('tr').remove();
}); 

//    Thursday table
$("#add4").click(function(){
  
  ++i;

  $("#dynamicTable4").append('<tr><td><input type="time" name="addmore['+i+'][thursday_shift_]" placeholder="Enter thursday_shift_" class="form-control" /></td><td><input type="time" name="addmore['+i+'][thursday_end_at]" placeholder="Enter thursday_end_at" class="form-control" /></td><td><button type="button" class="btn btn-danger remove-tr4">Remove</button></td></tr>');
});

$(document).on('click', '.remove-tr4', function(){  
   $(this).parents('tr').remove();
});

//    friday table
$("#add5").click(function(){
  
  ++i;

  $("#dynamicTable5").append('<tr><td><input type="time" name="addmore['+i+'][firday_shift_]" placeholder="Enter firday_shift_" class="form-control" /></td><td><input type="time" name="addmore['+i+'][friday_end_at]" placeholder="Enter friday_end_at" class="form-control" /></td><td><button type="button" class="btn btn-danger remove-tr5">Remove</button></td></tr>');
});

$(document).on('click', '.remove-tr5', function(){  
   $(this).parents('tr').remove();
});

// saturday table
$("#add6").click(function(){
  
  ++i;

  $("#dynamicTable6").append('<tr><td><input type="time" name="addmore['+i+'][saturday_shift_]" placeholder="Enter staturday_shift_" class="form-control" /></td><td><input type="time" name="addmore['+i+'][saturday_end_at]" placeholder="Enter staturday_end_at" class="form-control" /></td><td><button type="button" class="btn btn-danger remove-tr6">Remove</button></td></tr>');
});

$(document).on('click', '.remove-tr6', function(){  
   $(this).parents('tr').remove();
});

// sunday table
$("#add7").click(function(){
  
  ++i;

  $("#dynamicTable7").append('<tr><td><input type="time" name="addmore['+i+'][sunday_shift_]" placeholder="Enter sunday_shift_" class="form-control" /></td><td><input type="time" name="addmore['+i+'][sunday_end_at]" placeholder="Enter sunday_end_at" class="form-control" /></td><td><button type="button" class="btn btn-danger remove-tr6">Remove</button></td></tr>');
});

$(document).on('click', '.remove-tr7', function(){  
   $(this).parents('tr').remove();
});

</script>
@endsection