@extends('layouts.admin_hoteloptions_layout')
@section('content')
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Category Details</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item"><a href="{{url('admin/manage-hotel-category_index/'.$category->hotel_id)}}">Manage Category</a></li>
                <li class="breadcrumb-item active">{{$category->hotel->name}} / Category Details</li>
            </ol>
            <div class="card mb-4">
                <div class="card-header">
                    <i class="fas fa-table mr-1"></i>
                        Category Details of <b>{{$category->name}}</b>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <p style="color:red;"><b>Name</b></p>
                            <p>{{$category->name}}</p>
                            <p style="color:red;"><b>Description</b></p>
                            <div class="text-align-justify">{!!$category->description!!}</div>
                            <p style="color:red; text-align:justify;"><b>meta data</b></p>
                            <p>{{$category->meta_tag}}</p>
                        </div>
                        <div class="col-md-6">
                            <p style="color:red;"><b>Status</b></p>
                            <p>@if($category->status == 1) Active @else In-Active @endif</p>
                            <p style="color:red;"><b>Sort_order</b></p>
                            <p>{{$category->sort_order}}</p>
                            <p style="color:red;"><b>updated</b></p>
                            {!! htmlspecialchars_decode(date('j<\s\up>S</\s\up> F Y', strtotime($category->updated_at))) !!}
                            <p style="color:red;"><b>created_at</b></p>
                            {!! htmlspecialchars_decode(date('j<\s\up>S</\s\up> F Y', strtotime($category->created_at))) !!}
                        </div>
                    <div>
                </div>
            </div>
        </div>
    </main>
@endsection