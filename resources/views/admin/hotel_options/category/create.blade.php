@extends('layouts.admin_hoteloptions_layout')
@section('content')
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Category</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item"><a href="{{url('admin/manage-hotel-category_index/'.$hotel->id)}}">Category</a></li>
                <li class="breadcrumb-item active"> {{$hotel->name}} / Category</li>
            </ol>
            <div class="row">
                <div class="col-xl-12">
                <div class="card mb-4">
                    <div class="card-header">
                        Add New Category
                    </div>
                    <div class="card-body">
                        <form action="{{ url('admin/manage-hotel-category')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                            <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Category</label>
                                            <input type="text" class="form-control" name="name" placeholder="name" required>
                                            <input type="text" class="form-control" name="hotel_id" value="{{$hotel->id}}" hidden>
                                        </div>
                                        <div class="form-group">
                                            <label>Sort_order</label>
                                            <input type="number" class="form-control" name="sort_order" placeholder="0" value="0">
                                        </div>
                                        <div class="form-group">
                                            <label>description</label>
                                            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="description" 
								               >{{old('description')}}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Meta Tag</label>
                                            <input type="text" class="form-control" name="meta_tag">
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-success">
                                                <i class="feather-send"></i> SAVE
                                            </button>
                                            <a  class="btn btn-danger" href="{{url('admin/manage-hotel-category_index/'.$hotel->id)}}"> Cancel</a>
                                        <div>
                                    </div>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection