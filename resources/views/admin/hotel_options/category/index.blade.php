@extends('layouts.admin_hoteloptions_layout')
@section('content')
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Category</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item"><a href="{{url('admin/console')}}">Console</a></li>
                <li class="breadcrumb-item active"> {{$hotel->name}} / Category</li>
            </ol>
            @if (Session::has('success'))
                    <div class="alert alert-info">{{ Session::get('success') }}</div>
            @endif
            @if (Session::has('error'))
                <div class="alert alert-danger">{{ Session::get('error') }}</div>
            @endif
            <div class="card mb-4">
                <div class="card-header">
                    <i class="fas fa-table mr-1"></i>
                    Categories Lists
                </div>
                <div class="card-body">
                <a href="{{ url('admin/manage-hotel-category_create/'.$hotel->id)}}" class="btn btn-primary btn-sm float-right" >Add Category</a>
                <br><br>
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Name</th>
                                    <th>Sort_Order</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i= 1; ?>@foreach($categories as $category)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td> {{$category->name}}</td>
                                    <td> {{$category->sort_order}}</td>
                                    <td>
                                        @if($category->status == 1)
                                        <a href="{{url('admin/manage-hotel-category_status/'.$category->id.'/status')}}" onclick="return confirm('Status will be In-Active!')">
                                            <span class="btn btn-primary">Active</span>
                                        </a>
                                        @else 
                                        <a href="{{url('admin/manage-hotel-category_status/'.$category->id.'/status')}}" onclick="return confirm('Status will be Active!')">
                                            <span class="btn btn-danger">
                                            In-Active</span>
                                        </a>
                                        @endif
                                        @if($category->primary_status == 1)
                                        <a href="{{url('admin/manage-hotel-category_status/'.$category->id.'/primary_status')}}" onclick="return confirm('Category will be Secondary!')">
                                            <span class="btn btn-success">Primary</span>
                                        </a>
                                        @elseif($category->primary_status == 2 || $category->primary_status == null)
                                        <a href="{{url('admin/manage-hotel-category_status/'.$category->id.'/primary_status')}}" onclick="return confirm('Category will be Primary!')">
                                            <span class="btn btn-secondary">
                                            Secondary</span>
                                        </a>
                                        @endif
                                        <a href="{{ url('admin/manage-hotel-category_show/'.$category->hotel_id.'/'.$category->id)}}"><span class="btn btn-secondary"><i class="fa fa-eye"></i> Details</span></a>
                                        <a href="{{ url('admin/manage-hotel-category_edit/'.$category->hotel_id.'/'.$category->id)}}"><span class="btn btn-dark"><i class="fa fa-edit"></i></span></a>
                                        <!-- <br><br>
                                            <form action="{{url('admin/manage-hotel-category/'.$category->id) }}" class="delete" method="POST">
                                                {{ method_field('DELETE') }} {{csrf_field()}}
                                                <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure? Data will be deleted permanently!')"><i class="fa fa-trash"></i></button>
                                            </form> -->
                                        </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection