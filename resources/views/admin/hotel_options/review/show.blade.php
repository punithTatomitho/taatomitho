@extends('layouts.admin_hoteloptions_layout')
@section('content')
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Review Details</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item"><a href="{{url('admin/manage-hotel-review_index/'.$review->hotel_id)}}">Manage Review</a></li>
                <li class="breadcrumb-item active">{{$review->hotel->name}} / Review Details</li>
            </ol>
            <div class="card mb-4">
                <div class="card-header">
                    <i class="fas fa-table mr-1"></i>
                        Review Details of <b>{{$review->name}}</b>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <p style="color:red;"><b>Title</b></p>
                            <p>{{$review->title}}</p>
                            <p style="color:red;"><b>Rating</b></p>
                            <p>{{$review->rating}}</p>
                            <p style="color:red;"><b>Body</b></p>
                            <div class="text-align-justify">{{$review->body}}</div>                            
                        </div>
                        <div class="col-md-6">
                            <p style="color:red;"><b>approved_status</b></p>
                            <p>@if($review->approved_status == 1) Approved @else Not-Approved @endif</p>
                            <p style="color:red;"><b>updated</b></p>
                            {!! htmlspecialchars_decode(date('j<\s\up>S</\s\up> F Y', strtotime($review->updated_at))) !!}
                            <p style="color:red;"><b>created_at</b></p>
                            {!! htmlspecialchars_decode(date('j<\s\up>S</\s\up> F Y', strtotime($review->created_at))) !!}
                        </div>
                    <div>
                </div>
            </div>
        </div>
    </main>
@endsection