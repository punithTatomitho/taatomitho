@extends('layouts.admin_hoteloptions_layout')
@section('content')
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Review</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item"><a href="{{url('admin/console')}}">Console</a></li>
                <li class="breadcrumb-item active"> {{$hotel->name}} / Review</li>
            </ol>
            @if (Session::has('success'))
                    <div class="alert alert-info">{{ Session::get('success') }}</div>
            @endif
            @if (Session::has('error'))
                <div class="alert alert-danger">{{ Session::get('error') }}</div>
            @endif
            <div class="card mb-4">
                <div class="card-header">
                    <i class="fas fa-table mr-1"></i>
                    Review 
                </div>
                <div class="card-body">
                <br><br>
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Title</th>
                                    <th>Body</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i= 1; ?>@foreach($reviews as $review)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td> {{$review->title}}</td>
                                    <td> {{$review->body}}</td>
                                    <td>
                                        @if($review->approved_status == 1)
                                        <a href="{{url('admin/manage-hotel-review_status/'.$review->id.'/approved_status')}}">
                                            <span class="btn btn-primary">Approved</span>
                                        </a>
                                        @else 
                                        <a href="{{url('admin/manage-hotel-review_status/'.$review->id.'/approved_status')}}">
                                            <span class="btn btn-danger">
                                            Not-Approved</span>
                                        </a>
                                        @endif
                                        <a href="{{ url('admin/manage-hotel-review_show/'.$review->hotel_id.'/'.$review->id)}}"><span class="btn btn-secondary"><i class="fa fa-eye"></i> Details</span></a>
                                        <!-- <br><br>
                                            <form action="{{url('admin/manage-hotel-review/'.$review->id) }}" class="delete" method="POST">
                                                {{ method_field('DELETE') }} {{csrf_field()}}
                                                <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                            </form> -->
                                        </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection