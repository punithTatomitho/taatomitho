@extends('layouts.admin_hoteloptions_layout')
@section('content')
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Items</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item"><a href="{{url('admin/manage-hotel-item_index/'.$hotel->id)}}">Items</a></li>
                <li class="breadcrumb-item active"> {{$hotel->name}} / Item</li>
            </ol>
            <div class="row">
                <div class="col-xl-12">
                <div class="card mb-4">
                    <div class="card-header">
                        EDIT Item / {{$item->name}}
                    </div>
                    <div class="card-body">
                        <form action="{{ url('admin/manage-hotel-item/'.$item->id)}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        {{ method_field('PUT') }}
                                <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>name</label>
                                                <input type="text" class="form-control" name="name" placeholder="name" required value="{{$item->name}}">
                                                <input type="text" class="form-control" name="hotel_id" value="{{$hotel->id}}" hidden>
                                            </div>
                                            <div class="form-group">
                                                <label>description</label>
                                                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="description" 
                                                >{{$item->description}}</textarea>
                                            </div>
                                            <div class="form-group">
                                                <label>Category</label>
                                                <div>
                                                    <select class="custom-select" name="category_id">
                                                    <option value="">Select category </option>
                                                        @foreach($categories as $category)
                                                        @if($item->category->name == $category->name)
                                                        <option value="{{$category->id}}" selected>{{$category->name}}</option>
                                                        @else
                                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                                        @endif
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Spice Level</label>
                                                <div>
                                                    <select class="custom-select" name="spice_level">
                                                    <option value="0" {{ $item->spice_level == 0 ? 'selected' : ''}}>0</option>
                                                    <option value="1" {{ $item->spice_level == 1 ? 'selected' : ''}}>1</option>                                                       
                                                    <option value="2" {{ $item->spice_level == 2 ? 'selected' : ''}}>2</option>                                                       
                                                    <option value="3" {{ $item->spice_level == 3 ? 'selected' : ''}}>3</option>                                                       
                                                    <option value="4" {{ $item->spice_level == 4 ? 'selected' : ''}}>4</option>
                                                    <option value="5" {{ $item->spice_level == 5 ? 'selected' : ''}}>5</option>                                                       
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Sort Order</label>
                                                <input type="number" class="form-control" name="sort_order" placeholder="name" value="{{$item->sort_order}}" required>
                                            </div>
                                            <div class="form-group mt-10">
                                                <label class="control-label">Item Image: (440 x 280) <small><a target="_blank" href="https://www.figma.com/">Crop image</a> - <a target="_blank" href="https://squoosh.app/">Compress image</a></small></label><br>
                                                <label>Image</label>
                                                <input type="file" class="form-control" name="image" placeholder="image"  value="{{old('image')}}">
                                                @if ($errors->has('image'))
                                                    <span class="text-danger">{{ $errors->first('image') }}</span>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <label>Addons</label>
                                                <div>
                                                    <select class="custom-select" name="addons">
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class = "col-md-6">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="demo-heading">Promote Item</div>
                                                        <div class="icheck-material-primary icheck-inline">
                                                            <input type="radio" id="inline-radio-primary" name="promote_item" value="on" {{ $item->promote_item == "on" ? 'checked' : ''}} />
                                                            <label for="inline-radio-primary">ON</label>
                                                        </div>
                                                        <div class="icheck-material-info icheck-inline">
                                                            <input type="radio" id="inline-radio-info" name="promote_item" value="off" {{ $item->promote_item == "off" ? 'checked' : ''}}/>
                                                            <label for="inline-radio-info">OFF</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="demo-heading">Vegetarian</div>
                                                            <div class="icheck-material-primary icheck-inline">
                                                                <input type="radio" id="inline-radio-primary" name="vegetarian" value="yes" {{ $item->vegetarian == "yes" ? 'checked' : ''}}/>
                                                                <label for="inline-radio-primary">YES</label>
                                                            </div>
                                                            <div class="icheck-material-info icheck-inline">
                                                                <input type="radio" id="inline-radio-info" name="vegetarian" value="no" {{ $item->vegetarian == "no" ? 'checked' : ''}}/>
                                                                <label for="inline-radio-info">NO</label>
                                                            </div>
                                                    </div>
                                                    <div class="form-group">
                                                            <div class="demo-heading">Best Seller</div>
                                                            <div class="icheck-material-primary icheck-inline">
                                                                <input type="radio" id="inline-radio-primary" name="best_seller" value="yes" {{ $item->best_seller == "yes" ? 'checked' : ''}}/>
                                                                <label for="inline-radio-primary">YES</label>
                                                            </div>
                                                            <div class="icheck-material-info icheck-inline">
                                                                <input type="radio" id="inline-radio-info" name="best_seller" value="no"  {{ $item->best_seller == "no" ? 'checked' : ''}}/>
                                                                <label for="inline-radio-info">NO</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                        <div class="form-group">
                                                            <div class="demo-heading">Low Fat</div>
                                                            <div class="icheck-material-primary icheck-inline">
                                                                <input type="radio" id="inline-radio-primary" name="low_fat" value="yes"  {{ $item->low_fat == "yes" ? 'checked' : ''}}/>
                                                                <label for="inline-radio-primary">YES</label>
                                                            </div>
                                                            <div class="icheck-material-info icheck-inline">
                                                                <input type="radio" id="inline-radio-info" name="low_fat" value="no" {{ $item->low_fat == "yes" ? 'checked' : ''}}/>
                                                                <label for="inline-radio-info">NO</label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="demo-heading">New</div>
                                                            <div class="icheck-material-primary icheck-inline">
                                                                <input type="radio" id="inline-radio-primary" name="new" value="yes" {{ $item->new == "yes" ? 'checked' : ''}}/>
                                                                <label for="inline-radio-primary">YES</label>
                                                            </div>
                                                            <div class="icheck-material-info icheck-inline">
                                                                <input type="radio" id="inline-radio-info" name="new" value="no" {{ $item->new == "no" ? 'checked' : ''}}/>
                                                                <label for="inline-radio-info">NO</label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="demo-heading">Active</div>
                                                            <div class="icheck-material-primary icheck-inline">
                                                                <input type="radio" id="inline-radio-primary" name="status" value="1" checked="" {{ $item->status == 1 ? 'checked' : ''}}/>
                                                                <label for="inline-radio-primary">YES</label>
                                                            </div>
                                                            <div class="icheck-material-info icheck-inline">
                                                                <input type="radio" id="inline-radio-info" name="status" value="0" {{ $item->status == 0 ? 'checked' : ''}}/>
                                                                <label for="inline-radio-info">NO</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <div class="form-group">
                                                <label>Segments</label>
                                                    <div>
                                                        <select class="custom-select" name="segments[]" multiple>
                                                            @php $Segments = json_decode($item->segments); @endphp
                                                            <option value="online"  {{ in_array('online', $Segments) ? ' selected' : '' }}>Online</option>
                                                            <option value="inhouse" {{ in_array('inhouse', $Segments) ? ' selected' : '' }}>Inhouse</option>                                                       
                                                            <option value="dinein" {{ in_array('dinein', $Segments) ? ' selected' : '' }}>dinein</option>                                                                                                            
                                                        </select>
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                            <label>Items Pricing</label>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <label>Price</label>
                                                        <input type="number" class="form-control" name="item_price" placeholder="item price" required value="{{$item->item_price}}">
                                                        <div class="mt-3">
                                                            <label>Calories</label>
                                                            <input type="number" class="form-control" name="calories" placeholder="0" value="{{$item->calories}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label>Old price</label>
                                                        <input type="number" class="form-control" name="item_oldprice" placeholder="old price" required value="{{$item->item_oldprice}}">
                                                    </div>
                                                    
                                            </div>
                                            <div class="mt-5">
                                                <label><b>Item Type & Inventory</b></label>
                                                <br>
                                                <label><b>Item has variants</b></label>
                                            </div>
                                            <div class="col-md-6">
                                                        <label>SKU Identifier</label>
                                                        <input type="text" class="form-control" name="sku_identifier" placeholder="sku Indentifier" value="{{$item->sku_identifier}}">
                                                    </div>
                                            </div>
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-success">
                                                        <i class="feather-send"></i> SAVE
                                                    </button>
                                                    <a  class="btn btn-danger" href="{{url('admin/manage-hotel-item_index/'.$hotel->id)}}"> Cancel</a>
                                                <div>
                                            </div>
                                    </div>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection