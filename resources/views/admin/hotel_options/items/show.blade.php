@extends('layouts.admin_hoteloptions_layout')
@section('content')
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Item Details</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item"><a href="{{url('admin/manage-hotel-item_index/'.$item->hotel_id)}}">Manage Item</a></li>
                <li class="breadcrumb-item active">{{$item->hotel->name}} / Item Details</li>
            </ol>
            <div class="card mb-4">
                <div class="card-header">
                    <i class="fas fa-table mr-1"></i>
                        Item Details of <b>{{$item->name}}</b>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <p style="color:red;"><b>Name</b></p>
                            <p>{{$item->name}}</p>
                            <p style="color:red;"><b>Description</b></p>
                            <div class="text-align-justify">{!!$item->description!!}</div>
                            <p style="color:red;"><b>Addons</b></p>
                            <p>@if(isset($item->addons)) {{$item-addons}} @else No addons @endif</p>
                            <p style="color:red;"><b>Category</b></p>
                            <p>{{$item->category->name}}</p>
                            <p style="color:red;"><b>Promotion</b></p>
                            <p>{{strtoupper($item->promote_item)}}</p>
                            <p style="color:red;"><b>Vegetarian</b></p>
                            <p>{{strtoupper($item->vegetarian)}}</p>
                            <p style="color:red;"><b>Best Seller</b></p>
                            <p>{{strtoupper($item->best_seller)}}</p>
                        </div>
                        <div class="col-md-6">
                            <p style="color:red;"><b>Status</b></p>
                            <p>@if($item->status == 1) Active @else In-Active @endif</p>
                            <p style="color:red;"><b>Sort_order</b></p>
                            <p>{{$item->sort_order}}</p>
                            
                            <p style="color:red;"><b>Price</b></p>
                            <p>{{$item->item_price}}</p>

                            <p style="color:red;"><b>Old Price</b></p>
                            <p>{{$item->item_oldprice}}</p>

                            <p style="color:red;"><b>updated</b></p>
                            {!! htmlspecialchars_decode(date('j<\s\up>S</\s\up> F Y', strtotime($item->updated_at))) !!}
                            <p style="color:red;"><b>created_at</b></p>
                            {!! htmlspecialchars_decode(date('j<\s\up>S</\s\up> F Y', strtotime($item->created_at))) !!}
                        </div>
                    <div>
                </div>
            </div>
        </div>
    </main>
@endsection