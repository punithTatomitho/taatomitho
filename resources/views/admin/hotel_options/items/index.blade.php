@extends('layouts.admin_hoteloptions_layout')
@section('content')
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Items</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item"><a href="{{url('admin/console')}}">Console</a></li>
                <li class="breadcrumb-item active"> {{$hotel->name}} / Items</li>
            </ol>
            @if (Session::has('success'))
                    <div class="alert alert-info">{{ Session::get('success') }}</div>
            @endif
            @if (Session::has('error'))
                <div class="alert alert-danger">{{ Session::get('error') }}</div>
            @endif
            <div class="container">
            <!-- Modal for Rider -->
            <div class="modal fade" id="upload_items" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Upload Csv file</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                            <!-- select -->
                            <form action="{{ url('admin/manage-hotel-item_import/'.$hotel->id) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <label for="code">Upload Items Using Csv file</label>
                                <br>
                                <div class="form-group">
                                    <input type="file" name="file" class="form-control" accept=".csv" required="">
                                    <input type="text" class="form-control" name="hotel_id" value="{{$hotel->id}}" hidden>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-success" onclick="return confirm('Are you sure? Previous data will be replaced by this data!')">Upload</button>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            <div class="card mb-4">
                <div class="card-header">
                    <i class="fas fa-table mr-1"></i>
                    Items Lists
                </div>
                <div class="card-body">
                <a href="{{ url('admin/manage-hotel-item_create/'.$hotel->id)}}" class="btn btn-primary btn-sm float-right" >Add Items</a>
                <a href="{{ url('admin/manage-hotel-item_export/'.$hotel->id)}}" class="btn btn-default btn-sm float-right mr-2 border">Download Csv file</a>
                <a  data-toggle="modal" data-target="#upload_items" class="btn btn-default btn-sm float-right mr-2 border">Upload Items</a>
                <br><br>
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Name</th>
                                    <th>Segement</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i= 1; ?>@foreach($items as $item)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td> {{$item->name}}</td>
                                    <td>{{str_replace( array('[',']','"') , '',$item->segments)}}</td>
                                    <td>
                                        @if($item->status == 1)
                                        <a href="{{url('admin/manage-hotel-item_status/'.$item->id.'/status')}}" onclick="return confirm('Status will be In-Active!')">
                                            <span class="btn btn-primary">Active</span>
                                        </a>
                                        @else 
                                        <a href="{{url('admin/manage-hotel-item_status/'.$item->id.'/status')}}" onclick="return confirm('Status will be Active!')">
                                            <span class="btn btn-danger">
                                            In-Active</span>
                                        </a>
                                        @endif
                                        @if($item->promote_item == 'on')
                                        <a href="{{url('admin/manage-hotel-item_status/'.$item->id.'/promote_item')}}" onclick="return confirm('Item will be Not-Promote!')">
                                            <span class="btn btn-success">Promotion</span>
                                        </a>
                                        @elseif($item->promote_item == 'off' || $item->promote_item == null)
                                        <a href="{{url('admin/manage-hotel-item_status/'.$item->id.'/promote_item')}}" onclick="return confirm('Item will be Promote!')">
                                            <span class="btn btn-secondary">
                                            No Promotion</span>
                                        </a>
                                        @endif
                                        <a href="{{ url('admin/manage-hotel-item_show/'.$item->hotel_id.'/'.$item->id)}}"><span class="btn btn-secondary"><i class="fa fa-eye"></i> Details</span></a>
                                        <a href="{{ url('admin/manage-hotel-item_edit/'.$item->hotel_id.'/'.$item->id)}}"><span class="btn btn-dark"><i class="fa fa-edit"></i></span></a>
                                        <!-- <br><br>
                                            <form action="{{url('admin/manage-hotel-item/'.$item->id) }}" class="delete" method="POST">
                                                {{ method_field('DELETE') }} {{csrf_field()}}
                                                <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure? Data will be deleted permanently!')"><i class="fa fa-trash"></i></button>
                                            </form> -->
                                        </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection