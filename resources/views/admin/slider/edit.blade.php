@extends('layouts.admin_layout')
@section('content')
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Slider</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item"><a href="{{url('admin/manage-slider')}}">Manage Slider</a></li>
                <li class="breadcrumb-item active">Edit Slider</li>
            </ol>
            <div class="row">
                <div class="col-xl-12">
                <div class="card mb-4">
                    <div class="card-header">
                        Edit Slider
                    </div>
                    <div class="card-body">
                        <form action="{{ url('admin/manage-slider/'.$slider->id)}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        {{ method_field('PUT') }}
                            <div class="row">
                                    <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Category</label>
                                        <div>
                                            <select class="custom-select" name="category_id" required>
                                            <option value="">Select category </option>
                                                @foreach($categories as $category)
                                                <option value="{{$category->id}}" {{$slider->category->category_name == $category->category_name ? 'selected' : ''}}>{{$category->category_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="icheck-material-primary icheck-inline">
                                        <input type="checkbox"  name="promotion_status" value="yes" {{ $slider->promotion_status == "yes" ? 'checked' : '' }}/>
                                        <label>Promoted</label>
                                    </div>
                                        <div class="form-group">
                                            <label>Title</label>
                                            <input type="text" class="form-control" name="title" placeholder="tile" required value="{{$slider->title}}">
                                        </div>
                                        <div class="form-group">
                                                <label>description</label>
                                                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="description" 
                                                required>{{$slider->description}}</textarea>
                                            </div>
                                        <div class="form-group">
                                            <label>Sort order</label>
                                            <input type="number" class="form-control" name="sort_order" placeholder="" value="{{$slider->sort_order}}"/>
                                        </div>
                                        <div class="form-group mt-10">
                                            <label class="control-label">Icon Image: <small><a target="_blank" href="https://www.figma.com/">Crop image</a> - <a target="_blank" href="https://squoosh.app/">Compress image</a></small></label><br>
                                            <!-- <label>Image</label> -->
                                            <input type="file" class="form-control" name="icon_image" >
                                        </div>
                                        <div class="form-group">
                                            <label>Back ground Color</label>
                                            <input type="color"  name="bg_color" value="{{$slider->bg_color}}"/>
                                        </div>
                                        <div class="form-group">
                                            <label>Text Color</label>
                                            <input type="color"  name="text_color" value="{{$slider->text_color}}"/>
                                        </div>
                                        <div class="form-group mt-10">
                                            <label class="control-label">Back Ground Image: <small><a target="_blank" href="https://www.figma.com/">Crop image</a> - <a target="_blank" href="https://squoosh.app/">Compress image</a></small></label><br>
                                            <!-- <label>Image</label> -->
                                            <input type="file" class="form-control" name="background_image" >
                                        </div>
                                        <div class="form-group">
                                            <label>Button Text</label>
                                            <input type="text" class="form-control" name="button_text" value="{{$slider->button_text}}"/>
                                        </div>

                                        <div class="form-group">
                                            <label>Button Color</label>
                                            <input type="color" name="button_color" value="{{$slider->button_color}}"/>
                                        </div>
                                        <div class="form-group">
                                        <label>Redirect To</label>
                                            <div>
                                                <select class="custom-select" name="redirect_to">
                                                <option value="">Select options </option>
                                                    @foreach($categories as $category)
                                                    <option value="{{lcfirst($category->category_name)}}" {{$slider->redirect_to == env('APP_URL')."/restaurants/".lcfirst($category->category_name) ? 'selected':''}}>{{$category->category_name}}</option>
                                                    @endforeach
                                                    @foreach($hotels as $hotel)
                                                    <option value="{{$hotel->slug}}" {{$slider->redirect_to == env('APP_URL')."/restaurants/".$hotel->slug ? 'selected':''}}>{{$hotel->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Zone</label>
                                            <input type="text" class="form-control" name="zones" placeholder="zones" value="{{$slider->zones}}">
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-success">
                                                <i class="feather-send"></i> SAVE
                                            </button>
                                            <a  class="btn btn-danger" href="{{url('admin/manage-slider')}}"> Cancel</a>
                                        <div>
                                    </div>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection