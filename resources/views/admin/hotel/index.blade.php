@extends('layouts.admin_layout')
@section('content')
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Hotels</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item"><a href="{{url('admin/console')}}">Console</a></li>
                <li class="breadcrumb-item active">Hotels</li>
            </ol>
            @if (Session::has('success'))
                    <div class="alert alert-info">{{ Session::get('success') }}</div>
            @endif
            @if (Session::has('error'))
                <div class="alert alert-danger">{{ Session::get('error') }}</div>
            @endif
            <div class="container">
            <!-- Modal for Rider -->
            <div class="modal fade" id="upload_hotels" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Upload Csv file</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                            <!-- select -->
                            <form action="{{ url('admin/hotels_import') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <label for="code">Upload Hotels Using Csv file</label>
                                <br>
                                <div class="form-group">
                                    <input type="file" name="file" class="form-control" accept=".csv" required="">
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-success" onclick="return confirm('Are you sure? Previous data will be replaced by this data!')">Upload</button>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            <div class="card mb-4">
                <div class="card-header">
                    <i class="fas fa-table mr-1"></i>
                    Hotels Lists
                </div>
                <div class="card-body">
                <a href="{{ url('admin/manage-hotel/create')}}" class="btn btn-primary btn-sm float-right" >Add Hotel</a>
                <a  data-toggle="modal" data-target="#upload_hotels" class="btn btn-default btn-sm float-right mr-2 border">Upload Hotels</a>
                <br><br>
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Name</th>
                                    <th>Sort Order</th>
                                    <th>Updated</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i= 1; ?>@foreach($hotels as $hotel)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td> {{$hotel->name}}</td>
                                    <td> {{$hotel->sort_order}}</td>
                                    <td> {{$hotel->updated_at}}</td>
                                    <td>
                                        @if($hotel->offers_status == 1)
                                        <a href="{{url('admin/manage-hotel_status/'.$hotel->id.'/offers_status')}}">
                                            <span class="btn btn-success" onclick="return confirm('Offers will be disabled!')">Offers</span>
                                        </a>
                                        @else 
                                        <a href="{{url('admin/manage-hotel_status/'.$hotel->id.'/offers_status')}}" onclick="return confirm('Offers will be enabled!')">
                                            <span class="btn btn-secondary">
                                            No-Offers</span>
                                        </a>
                                        @endif
                                        @if($hotel->hotel_status == 1)
                                        <a href="{{url('admin/manage-hotel_status/'.$hotel->id.'/hotel_status')}}" onclick="return confirm('Hotel will close!')">
                                            <span class="btn btn-success">Open</span>
                                        </a>
                                        @else 
                                        <a href="{{url('admin/manage-hotel_status/'.$hotel->id.'/hotel_status')}}" onclick="return confirm('Hotel will open!')">
                                            <span class="btn btn-secondary">
                                            Closed</span>
                                        </a>
                                        @endif
                                        @if($hotel->promotion_status == 1)
                                        <a href="{{url('admin/manage-hotel_status/'.$hotel->id.'/promotion_status')}}" onclick="return confirm('Promotion status will be off!')">
                                            <span class="btn btn-success">Promotion ON</span>
                                        </a>
                                        @else 
                                        <a href="{{url('admin/manage-hotel_status/'.$hotel->id.'/promotion_status')}}" onclick="return confirm('Promotion status will be On!')">
                                            <span class="btn btn-secondary">
                                            Promotion OFF</span>
                                        </a>
                                        @endif
                                        @if($hotel->list_status == 1)
                                        <a href="{{url('admin/manage-hotel_status/'.$hotel->id.'/list_status')}}" onclick="return confirm('Hotel will not be listed!')">
                                            <span class="btn btn-success">Listed</span>
                                        </a>
                                        @else 
                                        <a href="{{url('admin/manage-hotel_status/'.$hotel->id.'/list_status')}}" onclick="return confirm('Hotel will be listed!')">
                                            <span class="btn btn-secondary">
                                            Not Listed</span>
                                        </a>
                                        @endif
                                        <a href="{{ url('restaurants', $hotel->slug)}}" target="_blank"><span class="btn btn-secondary"><i class="fa fa-eye"></i>Preview</span></a>
                                        <a href="{{ url('admin/manage-hotel-category_index/'.$hotel->id)}}"><span class="btn btn-secondary"><i class="fa fa-bars"></i>Options</span></a>
                                        <a href="{{ url('admin/manage-hotel/'.$hotel->id.'/edit')}}"><span class="btn btn-dark"><i class="fa fa-edit"></i></span></a>
                                        <br><br>
                                            <!-- <form action="{{url('admin/manage-hotel/'.$hotel->id) }}" class="delete" method="POST">
                                                {{ method_field('DELETE') }} {{csrf_field()}}
                                                <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure? Data will be deleted permanently!')"><i class="fa fa-trash"></i></button>
                                            </form> -->
                                        </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection