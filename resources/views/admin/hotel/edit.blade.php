@extends('layouts.admin_layout')
@section('content')
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Edit Hotel</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item"><a href="{{url('admin/manage-hotel')}}">Hotels</a></li>
                <li class="breadcrumb-item active">Edit Hotels</li>
            </ol>
            <div class="row">
                <div class="col-xl-12">
                <div class="card mb-4">
                    <div class="card-header">
                        Edit Hotels
                    </div>
                    <div class="card-body">
                    <form action="{{ url('admin/manage-hotel/'.$hotel->id)}}" method="POST" enctype="multipart/form-data">
                            @csrf
                        	{{ method_field('PUT') }}
                            <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Name</label>
                                            <input type="text" class="form-control" name="name" placeholder="Name" value="{{$hotel->name}}">
                                            @if ($errors->has('name'))
                                                <span class="text-danger">{{ $errors->first('name') }}</span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label>Sort_order</label>
                                            <input type="number" class="form-control" name="sort_order" placeholder="sort_order" value="{{$hotel->sort_order}}">
                                            @if ($errors->has('sort_order'))
                                                <span class="text-danger">{{ $errors->first('sort_order') }}</span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label>Tag</label>
                                            <input type="text" class="form-control" name="tag" placeholder="tag" value="{{$hotel->tag}}">
                                            @if ($errors->has('tag'))
                                                <span class="text-danger">{{ $errors->first('tag') }}</span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label>Description</label>
                                            <textarea class="editor" id="exampleFormControlTextarea1" rows="3" name="description" 
								               >{{ $hotel->description}}</textarea>
                                               @if ($errors->has('description'))
                                                <span class="text-danger">{{ $errors->first('description') }}</span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label>meta tag</label>
                                            <input type="text" class="form-control" name="meta_tag" placeholder="Meta tag"  value="{{$hotel->meta_tag}}">
                                            @if ($errors->has('meta_tag'))
                                                <span class="text-danger">{{ $errors->first('meta_tag') }}</span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label>phone</label>
                                            <input type="text" class="form-control" name="phone" placeholder="phone"  value="{{$hotel->phone}}">
                                            @if ($errors->has('phone'))
                                                <span class="text-danger">{{ $errors->first('phone') }}</span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input type="text" class="form-control" name="email" placeholder="Email"  value="{{$hotel->email}}">
                                            @if ($errors->has('email'))
                                                <span class="text-danger">{{ $errors->first('email') }}</span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label>Contact Name</label>
                                            <input type="text" class="form-control" name="contact_name" placeholder="Contact Name"  value="{{$hotel->contact_name}}">
                                            @if ($errors->has('contact_name'))
                                                <span class="text-danger">{{ $errors->first('contact_name') }}</span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label>Contact Phone</label>
                                            <input type="text" class="form-control" name="contact_phone" placeholder="Contact Phone"  value="{{$hotel->contact_phone}}">
                                            @if ($errors->has('contact_phone'))
                                                <span class="text-danger">{{ $errors->first('contact_phone') }}</span>
                                            @endif
                                        </div>
                                        <div class="form-group mt-10">
                                            <!-- <label>Hotel Banner Image</label> -->
                                            <label class="control-label">Hotel Banner Image: (1360 × 425) <small><a target="_blank" href="https://www.figma.com/">Crop image</a> - <a target="_blank" href="https://squoosh.app/">Compress image</a></small></label><br>
                                            <input type="file" class="form-control" name="hotel_banner_image" placeholder="hotel_banner_image"  value="{{old('hotel_banner_image')}}">
                                            @if ($errors->has('hotel_banner_image'))
                                                <span class="text-danger">{{ $errors->first('hotel_banner_image') }}</span>
                                            @endif
                                        </div>
                                        <div class="form-group mt-10">
                                            <!-- <label>Hotel Thumbnail Image</label> -->
                                            <label class="control-label">Hotel Thumbnail Image: (1024 × 1024) <small><a target="_blank" href="https://www.figma.com/">Crop image</a> - <a target="_blank" href="https://squoosh.app/">Compress image</a></small></label><br>
                                            <input type="file" class="form-control" name="hotel_thumbnail_image" placeholder="hotel_thumbnail_image" value="{{old('hotel_thumbnail_image')}}">
                                            @if ($errors->has('hotel_thumbnail_image'))
                                                <span class="text-danger">{{ $errors->first('hotel_thumbnail_image') }}</span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label>Allergy Indication</label>
                                            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="allergy_indication" 
								               >{{$hotel->allergy_indication}}</textarea>
                                               @if ($errors->has('allergy_indication'))
                                                <span class="text-danger">{{ $errors->first('allergy_indication') }}</span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label>FaceBook Id</label>
                                            <input type="text" class="form-control" name="facebook_id" placeholder="facebook_id" value="{{$hotel->facebook_id}}">
                                            @if ($errors->has('facebook_id'))
                                                <span class="text-danger">{{ $errors->first('facebook_id') }}</span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label>Twitter Id</label>
                                            <input type="text" class="form-control" name="twitter_id" placeholder="twitter_id" value="{{$hotel->twitter_id}}">
                                            @if ($errors->has('twitter_id'))
                                                <span class="text-danger">{{ $errors->first('twitter_id') }}</span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                        <div class="demo-heading">Order details</div>
                                        @if($hotel->order_details != "null")
                                            <div class="icheck-material-primary icheck-inline">
                                                <input type="checkbox" id="inline-radio-primary" name="order_details[]" value="Accept Online Orders" {{ in_array('Accept Online Orders', json_decode($hotel->order_details)) ? ' checked' : '' }}/>
                                                <label>Accept Online Orders</label>
                                            </div>
                                            <div class="icheck-material-primary icheck-inline">
                                                <input type="checkbox" id="inline-radio-primary" name="order_details[]" value="Serves Night Orders" {{ in_array('Serves Night Orders', json_decode($hotel->order_details)) ? ' checked' : '' }} />
                                                <label>Serves Night Orders</label>
                                            </div>
                                            <div class="icheck-material-primary icheck-inline">
                                                <input type="checkbox" id="inline-radio-primary" name="order_details[]" value="Accept Table Reservations" {{ in_array('Accept Table Reservations', json_decode($hotel->order_details)) ? ' checked' : '' }}/>
                                                <label>Accept Table Reservations</label>
                                            </div>
                                            <div class="icheck-material-primary icheck-inline">
                                                <input type="checkbox" id="inline-radio-primary" name="order_details[]" value="Accept Deliveries" onchange="toggleAccetpDeliveryInput()" id="acceptdelivery" {{ in_array('Accept Deliveries', json_decode($hotel->order_details)) ? ' checked' : '' }}/>
                                                <label>Accept Deliveries</label>
                                                <input type="text" class="form-control" name="accept_deliveries" placeholder="accept_deliveries" id="acceptdeliveryinput" value="{{$hotel->accept_deliveries}}">
                                                @if ($errors->has('accept_deliveries'))
                                                    <span class="text-danger">{{ $errors->first('accept_deliveries') }}</span>
                                                @endif
                                            </div>
                                            <div class="icheck-material-primary icheck-inline">
                                                <input type="checkbox" id="inline-radio-primary" name="order_details[]" value="Accept Collections" onchange="toggleAcceptCollections()" id="acceptcollections" {{ in_array('Accept Collections', json_decode($hotel->order_details)) ? ' checked' : '' }}/>
                                                <label>Accept Collections</label>

                                                <input type="text" class="form-control" name="accept_collections" placeholder="accept_collections" id="acceptcollectionsinput" value="{{$hotel->accept_collections}}">
                                                @if ($errors->has('accept_collections'))
                                                    <span class="text-danger">{{ $errors->first('accept_collections') }}</span>
                                                @endif
                                            </div>
                                            @else
                                            <div class="icheck-material-primary icheck-inline">
                                                <input type="checkbox" id="inline-radio-primary" name="order_details[]" value="Accept Online Orders" />
                                                <label>Accept Online Orders</label>
                                            </div>
                                            <div class="icheck-material-primary icheck-inline">
                                                <input type="checkbox" id="inline-radio-primary" name="order_details[]" value="Serves Night Orders" />
                                                <label>Serves Night Orders</label>
                                            </div>
                                            <div class="icheck-material-primary icheck-inline">
                                                <input type="checkbox" id="inline-radio-primary" name="order_details[]" value="Accept Table Reservations" />
                                                <label>Accept Table Reservations</label>
                                            </div>
                                            <div class="icheck-material-primary icheck-inline">
                                                <input type="checkbox" id="inline-radio-primary" name="order_details[]" value="Accept Deliveries" onchange="toggleAccetpDeliveryInput()" id="acceptdelivery" />
                                                <label>Accept Deliveries</label>
                                                <input type="text" class="form-control" name="accept_deliveries" placeholder="accept_deliveries" id="acceptdeliveryinput" value="{{$hotel->accept_deliveries}}">
                                                @if ($errors->has('accept_deliveries'))
                                                    <span class="text-danger">{{ $errors->first('accept_deliveries') }}</span>
                                                @endif
                                            </div>
                                            <div class="icheck-material-primary icheck-inline">
                                                <input type="checkbox" id="inline-radio-primary" name="order_details[]" value="Accept Collections" onchange="toggleAcceptCollections()" id="acceptcollections" />
                                                <label>Accept Collections</label>

                                                <input type="text" class="form-control" name="accept_collections" placeholder="accept_collections" id="acceptcollectionsinput" value="{{$hotel->accept_collections}}">
                                                @if ($errors->has('accept_collections'))
                                                    <span class="text-danger">{{ $errors->first('accept_collections') }}</span>
                                                @endif
                                            </div>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label>Billing Address</label>
                                            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="billing_address" 
								               >{{$hotel->billing_address}}</textarea>
                                               @if ($errors->has('billing_address'))
                                                <span class="text-danger">{{ $errors->first('billing_address') }}</span>
                                            @endif
                                        </div>
                                        <!-- <div class="form-group">
                                            <div class="demo-heading">Status</div>
                                            <div class="icheck-material-primary icheck-inline">
                                                <input type="radio" id="inline-radio-primary" name="status" value="1" />
                                                <label for="inline-radio-primary">Active</label>
                                            </div>
                                            <div class="icheck-material-info icheck-inline">
                                                <input type="radio" id="inline-radio-info" name="status" value="0" />
                                                <label for="inline-radio-info">In-Active</label>
                                            </div>
                                        </div> -->
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Cuisines</label>
                                            <div>
                                                <select class="custom-select" name="cuisines[]" multiple>
                                                    @foreach($cuisines as $cuisine)
                                                    @if($hotel->cuisines != "null")
                                                    <option value="{{ $cuisine->name}}" {{ in_array($cuisine->name, json_decode($hotel->cuisines)) ? ' selected' : '' }}>{{$cuisine->name}}</option>
                                                    @else
                                                    <option value="{{ $cuisine->name}}" >{{$cuisine->name}}</option>
                                                    @endif
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('cuisines'))
                                                <span class="text-danger">{{ $errors->first('cuisines') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Range</label>
                                            <div>
                                                <select class="custom-select" name="range">
                                                <option value="">select range</option>
                                                <option value="Free" {{ $hotel->range == "Free" ? 'selected' : '' }}>Free</option>
                                                <option value="Inexpensive" {{ $hotel->range == "Inexpensive" ? 'selected' : '' }}>Inexpensive</option>
                                                <option value="Moderate"  {{ $hotel->range == "Moderate" ? 'selected' : '' }}>Moderate</option>   
                                                <option value="Expensive" {{ $hotel->range == "Expensive" ? 'selected' : '' }}>Expensive</option>
                                                <option value="Very Expensive" {{ $hotel->range == "Very Expensive" ? 'selected' : '' }}>Very Expensive</option>
                                                </select>
                                                @if ($errors->has('range'))
                                                <span class="text-danger">{{ $errors->first('range') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Latitude </label>
                                            <input type="text" class="form-control" name="latitude" placeholder="latitude" value="{{$hotel->latitude}}">
                                            @if ($errors->has('latitude'))
                                                <span class="text-danger">{{ $errors->first('latitude') }}</span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label>Longitude</label>
                                            <input type="text" class="form-control" name="longitude" placeholder="longitude" value="{{$hotel->longitude}}">
                                            @if ($errors->has('longitude'))
                                                <span class="text-danger">{{ $errors->first('longitude') }}</span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label>Google Place Id</label>
                                            <input type="text" class="form-control" name="google_place_id" placeholder="google_place_id" value="{{$hotel->google_place_id}}">
                                            @if ($errors->has('google_place_id'))
                                                <span class="text-danger">{{ $errors->first('google_place_id') }}</span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label>Address</label>
                                            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="address" 
								               >{{$hotel->address}}</textarea>
                                               @if ($errors->has('address'))
                                                <span class="text-danger">{{ $errors->first('address') }}</span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label>Street</label>
                                            <input type="text" class="form-control" name="street" placeholder="street" value="{{$hotel->street}}">
                                            @if ($errors->has('street'))
                                                <span class="text-danger">{{ $errors->first('street') }}</span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label>District</label>
                                            <input type="text" class="form-control" name="district" placeholder="district" value="{{$hotel->district}}">
                                            @if ($errors->has('district'))
                                                <span class="text-danger">{{ $errors->first('district') }}</span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label>Ward</label>
                                            <input type="text" class="form-control" name="ward" placeholder="ward" value="{{$hotel->ward}}">
                                            @if ($errors->has('ward'))
                                                <span class="text-danger">{{ $errors->first('ward') }}</span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label>Zip Code</label>
                                            <input type="text" class="form-control" name="zip_code" placeholder="zip_code" value="{{$hotel->zip_code}}">
                                            @if ($errors->has('zip_code'))
                                                <span class="text-danger">{{ $errors->first('zip_code') }}</span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label>City</label>
                                            <input type="text" class="form-control" name="city" placeholder="city" value="{{$hotel->city}}">
                                            @if ($errors->has('city'))
                                                <span class="text-danger">{{ $errors->first('city') }}</span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label>Delivery Areas</label>
                                            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="delivery_areas" 
								               >{{$hotel->delivery_areas}}</textarea>
                                               @if ($errors->has('delivery_areas'))
                                                <span class="text-danger">{{ $errors->first('delivery_areas') }}</span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-12">
                                                        <div class="col-md-6">
                                                            <label>Min Order Amount</label>
                                                            <input type="text" class="form-control" name="min_order_amount" placeholder="min_order_amount" value="{{$hotel->min_order_amount}}">
                                                            @if ($errors->has('min_order_amount'))
                                                                <span class="text-danger">{{ $errors->first('min_order_amount') }}</span>
                                                            @endif
                                                            <label>Delivery radius- Miles</label>
                                                            <input type="text" class="form-control" name="delivery_radius_miles" placeholder="delivery_radius_miles" value="{{$hotel->delivery_radius_miles}}">
                                                            <div class="input-group-append"><span class="input-group-text"><i class="fa fa-road"></i></span></div>
                                                            @if ($errors->has('delivery_radius_miles'))
                                                            @endif
                                                            <label>Delivery Charge</label>
                                                            <input type="text" class="form-control" name="delivery_charge" placeholder="delivery_charge"  value="{{$hotel->delivery_charge}}">
                                                            @if ($errors->has('delivery_charge'))
                                                                <span class="text-danger">{{ $errors->first('delivery_charge') }}</span>
                                                            @endif

                                                            <label>VAT</label>
                                                            <input type="number" class="form-control" name="vat" placeholder="vat"  value="{{$hotel->vat}}">
                                                            <div class="input-group-append"><span class="input-group-text">%</span></div>
                                                            @if ($errors->has('vat'))
                                                                <span class="text-danger">{{ $errors->first('vat') }}</span>
                                                            @endif
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label>Free Delivery Amount</label>
                                                            <input type="text" class="form-control" name="free_delivery_amount" placeholder="free_delivery_amount"  value="{{$hotel->free_delivery_amount}}">
                                                            @if ($errors->has('free_delivery_amount'))
                                                                <span class="text-danger">{{ $errors->first('free_delivery_amount') }}</span>
                                                            @endif
                                                            <label>Service Charge</label>
                                                            <input type="number" class="form-control" name="service_charge" placeholder="service_charge" value="{{$hotel->service_charge}}">
                                                            <div class="input-group-append"><span class="input-group-text">%</span></div>
                                                            @if ($errors->has('service_charge'))
                                                                <span class="text-danger">{{ $errors->first('vaservice_charget') }}</span>
                                                            @endif
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="demo-heading">Payment details</div>
                                            @if($hotel->payment_details != "null")
                                            <div class="icheck-material-primary icheck-inline">
                                                <input type="checkbox" id="inline-radio-primary" name="payment_details[]" value="Accept Online Payments" {{ in_array('Accept Online Payments', json_decode($hotel->payment_details)) ? ' checked' : '' }}/>
                                                <label>Accept Online Payments</label>
                                            </div>
                                            <div class="icheck-material-primary icheck-inline">
                                                <input type="checkbox" id="inline-radio-primary" name="payment_details[]" value="Accept Cash on Delivery"  {{ in_array('Accept Cash on Delivery', json_decode($hotel->payment_details)) ? ' checked' : '' }}/>
                                                <label>Accept Cash on Delivery</label>
                                            </div>
                                            @else
                                            <div class="icheck-material-primary icheck-inline">
                                                <input type="checkbox" id="inline-radio-primary" name="payment_details[]" value="Accept Online Payments" />
                                                <label>Accept Online Payments</label>
                                            </div>
                                            <div class="icheck-material-primary icheck-inline">
                                                <input type="checkbox" id="inline-radio-primary" name="payment_details[]" value="Accept Cash on Delivery" />
                                                <label>Accept Cash on Delivery</label>
                                            </div>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <div class="demo-heading">Other details</div>
                                            @if($hotel->other_details != "null")
                                            <div class="icheck-material-primary icheck-inline">
                                                <input type="checkbox" id="inline-radio-primary" name="other_details[]" value="Indoor Seating Available" {{ in_array('Indoor Seating Available', json_decode($hotel->other_details)) ? ' checked' : '' }}/>
                                                <label>Indoor Seating Available</label>
                                            </div>
                                            <div class="icheck-material-primary icheck-inline">
                                                <input type="checkbox" id="inline-radio-primary" name="other_details[]" value="Outdoor Seating Available" {{ in_array('Outdoor Seating Available', json_decode($hotel->other_details)) ? ' checked' : '' }}/>
                                                <label>Outdoor Seating Available</label>
                                            </div>
                                            <div class="icheck-material-primary icheck-inline">
                                                <input type="checkbox" id="inline-radio-primary" name="other_details[]" value="Serves Alcohol" {{ in_array('Serves Alcohol', json_decode($hotel->other_details)) ? ' checked' : '' }}/>
                                                <label>Serves Alcohol</label>
                                            </div>
                                            <div class="icheck-material-primary icheck-inline">
                                                <input type="checkbox" id="inline-radio-primary" name="other_details[]" value="Serves Breakfast" {{ in_array('Serves Breakfast', json_decode($hotel->other_details)) ? ' checked' : '' }}/>
                                                <label>Serves Breakfast</label>
                                            </div>
                                            <div class="icheck-material-primary icheck-inline">
                                                <input type="checkbox" id="inline-radio-primary" name="other_details[]" value="Pure Vegetarian" {{ in_array('Pure Vegetarian', json_decode($hotel->other_details)) ? ' checked' : '' }}/>
                                                <label>Pure Vegetarian</label>
                                            </div>
                                            @else
                                            <div class="icheck-material-primary icheck-inline">
                                                <input type="checkbox" id="inline-radio-primary" name="other_details[]" value="Indoor Seating Available" />
                                                <label>Indoor Seating Available</label>
                                            </div>
                                            <div class="icheck-material-primary icheck-inline">
                                                <input type="checkbox" id="inline-radio-primary" name="other_details[]" value="Outdoor Seating Available" />
                                                <label>Outdoor Seating Available</label>
                                            </div>
                                            <div class="icheck-material-primary icheck-inline">
                                                <input type="checkbox" id="inline-radio-primary" name="other_details[]" value="Serves Alcohol" />
                                                <label>Serves Alcohol</label>
                                            </div>
                                            <div class="icheck-material-primary icheck-inline">
                                                <input type="checkbox" id="inline-radio-primary" name="other_details[]" value="Serves Breakfast" />
                                                <label>Serves Breakfast</label>
                                            </div>
                                            <div class="icheck-material-primary icheck-inline">
                                                <input type="checkbox" id="inline-radio-primary" name="other_details[]" value="Pure Vegetarian" />
                                                <label>Pure Vegetarian</label>
                                            </div>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <div class="demo-heading">Accommodation details</div>
                                            @if($hotel->accommodation_details == "null")
                                            <div class="icheck-material-primary icheck-inline">
                                                <input type="checkbox" id="inline-radio-primary" name="accommodation_details[]" value="Accommodation Available" />
                                                <label>Accommodation Available</label>
                                            </div>
                                            @else
                                            <div class="icheck-material-primary icheck-inline">
                                                <input type="checkbox" id="inline-radio-primary" name="accommodation_details[]" value="Accommodation Available" {{ in_array('Accommodation Available', json_decode($hotel->accommodation_details )) ? ' checked' : '' }}/>
                                                <label>Accommodation Available</label>
                                            </div>
                                            
                                            @endif
                                        </div>
                                        <div class="demo-heading">Registration details</div>
                                        <div class="form-group">
                                            <label>Commission %</label>
                                            <input type="number" class="form-control" name="commission" placeholder="commission" value="{{$hotel->hotel_registerdetails->commission}}">
                                            @if ($errors->has('commission'))
                                                <span class="text-danger">{{ $errors->first('commission') }}</span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label>Payout Frequency</label>
                                            <input type="number" class="form-control" name="payout_frequency" placeholder="payout_frequency"  value="{{$hotel->hotel_registerdetails->payout_frequency}}">
                                            @if ($errors->has('payout_frequency'))
                                                <span class="text-danger">{{ $errors->first('payout_frequency') }}</span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label>Registration charge</label>
                                            <input type="number" class="form-control" name="registeration_charge"  value="{{$hotel->hotel_registerdetails->registeration_charge}}">
                                            @if ($errors->has('registeration_charge'))
                                                <span class="text-danger">{{ $errors->first('registeration_charge') }}</span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label>Accept orders upto</label>
                                            <input type="number" class="form-control" name="accept_orders_upto" value="{{$hotel->hotel_registerdetails->accept_orders_upto}}">
                                            @if ($errors->has('accept_orders_upto'))
                                                <span class="text-danger">{{ $errors->first('accept_orders_upto') }}</span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label>Start date</label>
                                            <input type="date" class="form-control" name="start_date"  value="{{$hotel->hotel_registerdetails->start_date}}">
                                            @if ($errors->has('start_date'))
                                                <span class="text-danger">{{ $errors->first('start_date') }}</span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label>End date</label>
                                            <input type="date" class="form-control" name="end_date" value="{{$hotel->hotel_registerdetails->end_date}}">
                                            @if ($errors->has('end_date'))
                                                <span class="text-danger">{{ $errors->first('end_date') }}</span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label>Comments</label>
                                            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="comments" 
								               >{{$hotel->hotel_registerdetails->comments}}</textarea>
                                               @if ($errors->has('comments'))
                                                <span class="text-danger">{{ $errors->first('comments') }}</span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label>Offers text</label>
                                            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="offers_text" 
								               >{{$hotel->hotel_registerdetails->offers_text}}</textarea>
                                               @if ($errors->has('offers_text'))
                                                <span class="text-danger">{{ $errors->first('offers_text') }}</span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <div class="demo-heading">Grocery details</div>
                                            @if( $hotel->hotel_registerdetails->groccery_details != "null")
                                            <div class="icheck-material-primary icheck-inline">
                                                <input type="checkbox" id="inline-radio-primary" name="groccery_details[]" value="Serves Groceries" {{ in_array('Serves Groceries', json_decode($hotel->hotel_registerdetails->groccery_details)) ? ' checked' : '' }}/>
                                                <label>Serves Groceries</label>
                                            </div>
                                            @else
                                            <div class="icheck-material-primary icheck-inline">
                                                <input type="checkbox" id="inline-radio-primary" name="groccery_details[]" value="Serves Groceries" />
                                                <label>Serves Groceries</label>
                                            </div>
                                            @endif
                                            <div class="demo-heading">Internal store details</div>
                                            @if($hotel->hotel_registerdetails->internal_store_details != "null")
                                            <div class="icheck-material-primary icheck-inline">
                                                <input type="checkbox" id="inline-radio-primary" name="internal_store_details[]" value="Owners store"  {{ in_array('Owners store', json_decode($hotel->hotel_registerdetails->internal_store_details)) ? ' checked' : '' }}/>
                                                <label>Owners store</label>
                                            </div>
                                            @else
                                            <div class="icheck-material-primary icheck-inline">
                                                <input type="checkbox" id="inline-radio-primary" name="internal_store_details[]" value="Owners store" />
                                                <label>Owners store</label>
                                            </div>
                                            @endif
                                        <div>
                                        <div class="form-group mt-10">
                                            <button type="submit" class="btn btn-success">
                                                <i class="feather-send"></i> SAVE
                                            </button>
                                            <a  class="btn btn-danger" href="{{url('admin/manage-hotel')}}"> Cancel</a>
                                    </div>
                                    </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
<script>
    var hotel_accdelivery = {!! json_encode($hotel->accept_deliveries) !!};
    var hotel_collection = {!! json_encode($hotel->accept_collections) !!};

$(document).ready(function(){
    if($("#acceptdelivery").is(":checked")){
        $("#acceptdeliveryinput").show();
    }else{
        $("#acceptdeliveryinput").hide();
    }

    if($("#acceptcollections").is(":checked")){
        $("#acceptcollectionsinput").show();
    }else{
        $("#acceptcollectionsinput").hide();
    }

    if(hotel_accdelivery == null){
        $("#acceptdeliveryinput").hide();
    }else{
        $("#acceptdeliveryinput").show();
        
    }
    if(hotel_collection == null){
        $("#acceptcollectionsinput").hide();
    }else{
        $("#acceptcollectionsinput").show();

    }
});
function toggleAccetpDeliveryInput(){
		$("#acceptdeliveryinput").toggle();
	}
    function toggleAcceptCollections(){
		$("#acceptcollectionsinput").toggle();
	}
</script>
@endsection