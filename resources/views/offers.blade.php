@extends('layouts.site_layout')
@section('content')
      <section class="section pt-5 pb-5">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <h5 class="font-weight-bold mt-0 mb-3">Available Affers</h5>
               </div>
               	@if(count($offers) > 0)
	               	@foreach($offers as $offer)
		               	<div class="col-md-4">
		                  	<div class="card offer-card border-0 shadow-sm">
			                    <img src="{{ URL::to('storage/slider_bg_image', $offer->icon_image) }}" style="width:100%; height: 50%;">
			                    <span class="card-text offer-text-title">{{$offer->title}}</span>
			                    <p class="offer-text-subtitle">{{$offer->description}}</p>
			                    <a href="{{$offer->redirect_to}}" class="btn btn-primary order-btn">Order now</a>
		                  	</div>
		               	</div>
	               	@endforeach
	               @else
	               	<h6>No offers available today.</h6>
	               @endif
            </div>
         </div>
      </section>
@endsection