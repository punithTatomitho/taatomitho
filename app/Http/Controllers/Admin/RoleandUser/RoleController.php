<?php

namespace App\Http\Controllers\Admin\RoleandUser;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Role;
use App\Models\User;
use Session;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $roles = Role::all();
        return view('admin.rolesandusers.roles.index',compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.rolesandusers.roles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'display_name' => 'required',
            'description' => 'required',
        ]);
        $input = $request->all();
        $role = Role::create($input);

        if($role){
                 Session::flash('success', 'Successfully Role Created!');
                 return redirect('admin/manage-role');
            }else{
                Session::flash('error', 'Something went wrong!');
                 return redirect('admin/manage-role');
            }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::find($id);
        return view('admin.rolesandusers.roles.edit',compact('role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'display_name' => 'required',
            'description' => 'required',
        ]);
        $input = $request->all();
        $data = Role::find($id);
        $data->update($input);
        if($data){
                 Session::flash('success', 'Successfully Role Updated!');
                 return redirect('admin/manage-role');
            }else{
                Session::flash('error', 'Something went wrong!');
                 return redirect('admin/manage-role');
            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task =  Role::find($id)->delete();
        if($task){
            Session::flash('success', 'Successfully deleted!');
            return redirect()->back();
        }
    }
}
