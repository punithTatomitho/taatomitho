<?php

namespace App\Http\Controllers\Admin\RoleandUser;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Role;
use App\Models\User;
use App\Models\RoleUser;
use App\Models\User_profile;
use Session;
use DB;
use Hash;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $users = User::with('roles')->where('name', '!=','user')->get();

        return view('admin.rolesandusers.users.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();

        return view('admin.rolesandusers.users.create',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
        $create['name']= $request->get('name');
        $create['email']= $request->get('email');
        $create['password']=Hash::make($request->get('password'));  
        $check = User::where('email','=',$request->get('email'))->first();
        if($check){
                Session::flash('error', 'Email name already exists!');
                return redirect('admin/manage-users');
             }else{
                $user = User::create($create);
                $user->attachRole($request->get('role_id'));
                if($request->file('profile_image')){
                    $filename1 = $request->file('profile_image');
                    $filename1->getClientOriginalExtension();
                    $filename_p1 = $filename1->store('public/profile_image');
                    $filename_path1 = str_replace('public/profile_image/', '', $filename_p1);
                    $profile_data['profile_image'] =  $filename_path1;
                 }

                $user_profile = new User_profile;
                $profile_data['user_id'] = $user->id;
                $profile_data['user_name'] = $request->get('name');
                $profile_data['phone_number']= $request->get('phone_number');
                $profile_data['status']= $request->get('status');
                $user_profile->create($profile_data);
                if($user_profile){
                    Session::flash('success', 'Successfully created!');
                    return redirect('admin/manage-users');
                }else{
                    Session::flash('error', 'Something went wrong!');
                    return redirect('admin/manage-users');
                    }

                }
            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::all();
        return view('admin.rolesandusers.users.edit',compact('user','roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

            $updata['name']= $request->get('name');
            $updata['email']= $request->get('email');
            $updata['password']=Hash::make($request->get('password'));  
            $user = User::findOrFail($id);
            $user->update($updata);
            if($user){
                    $role_user = RoleUser::where('user_id','=',$user->id);
                    $role['role_id'] = $request->get('role_id');
                    $role_user->update($role);
                    $user_profile = User_profile::where('user_id','=',$id);
                    $profile_data['user_name'] = $request->get('name');
                    $profile_data['phone_number'] = $request->get('phone_number');
                    $profile_data['user_address'] = $request->get('user_address');
                    $profile_data['status']= $request->get('status');
                    $profile_data['zip_code'] = $request->get('zip_code');
                    if($request->file('profile_image')){
                        $filename = $request->file('profile_image');
                        $filename->getClientOriginalExtension();
                        $filename_p = $filename->store('public/profile_image');
                        $filename_path = str_replace('public/profile_image/', '', $filename_p);
                        $profile_data['profile_image'] =  $filename_path;
                        $user_profile->update($profile_data);
                     }
                    $user_profile->update($profile_data);
            }
            if($user){
                Session::flash('success', 'Successfully Updated!');
                return redirect('admin/manage-users');
            }else{
                Session::flash('error', 'Something went wrong!');
                return redirect('admin/manage-users');
                }

                
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $profile = User_profile::where('user_id','=',$id);
        $profile->delete();
        if($profile){
            $task =  User::find($id)->delete();
            if($task){
                Session::flash('success', 'Successfully deleted!');
                return redirect()->back();
            }
        }
        
    }

    public function status_update(Request $request,$id){
        $role = Role::where('name','=','user')->first();
        $user = User::findOrFail($id);
        $user_profile = User_profile::where('user_id','=',$id)->first();
        if($request->status == "status"){
            if($user_profile->status == 1){
                $role_user = RoleUser::where('user_id','=',$id);
                $role_data1['role_id'] = $role->id;
                $role_user->update($role_data1);
                $status['status'] = 0;
                $user_profile->update($status);
                return redirect()->back();
            }else if($user->status == 0 OR $user->status == null){
                $status['status'] = 1;
                $user_profile->update($status);
                return redirect()->back();
            }

        }
    }
}
