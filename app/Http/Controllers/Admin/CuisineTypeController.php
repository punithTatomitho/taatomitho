<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CuisineType;
use Session;

class CuisineTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cuisine_types = CuisineType::All();
        return view('admin.cuisine_type.index',compact('cuisine_types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.cuisine_type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input['type'] = ucfirst($request->get('type'));
        $data = CuisineType::create($input);

        if($data){
                 Session::flash('success', 'Successfully Created!');
                 return redirect('admin/manage-cuisine-type');
            }else{
                Session::flash('error', 'Something went wrong!');
                return redirect('admin/manage-cuisine-type');
            }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cuisine_type = CuisineType::findOrFail($id);
        return view('admin.cuisine_type.edit',compact('cuisine_type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input['type'] = ucfirst($request->get('type'));
        $cuisine_type = CuisineType::findOrFail($id);
        $update = $cuisine_type->update($input);
        if($update){
                 Session::flash('success', 'Successfully Updated!');
                 return redirect('admin/manage-cuisine-type');
            }else{
                Session::flash('error', 'Something went wrong!');
                return redirect('admin/manage-cuisine-type');
            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = CuisineType::findOrFail($id)->delete();
        if($delete){
            Session::flash('success', 'Successfully deleted!');
            return redirect()->back();
        }else{
           Session::flash('error', 'Something went wrong!');
           return redirect()->back();
        }
    }

    public function status_update(Request $request,$id){
        $cuisine_type = CuisineType::findOrFail($id);
         if($request->status == "status"){
             if($cuisine_type->status == 1){
                 $status['status'] = 0;
                $cuisine_type->update($status);
                 return redirect()->back();
             }else if($cuisine_type->status == 0 OR $cuisine_type->status == null){
                 $status['status'] = 1;
                 $cuisine_type->update($status);
                 return redirect()->back();
             }
   
         }
     }
}
