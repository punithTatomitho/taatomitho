<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Hotel;
use App\Models\Order;
use App\Models\HotelBill;
use DB;
use Carbon\Carbon;

class AnalyticController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // $dailyRevenue = Order::where('order_date', date('Y-m-d'))->count();
        if($request->get('hotel_id')) {
            $data['hotels'] = Order::select(DB::raw('orders.hotel_id as hotelId, orders.hotel_name as hotelName'))
                                    ->distinct('hotel_id')
                                    ->groupBy('hotel_id')
                                    ->get();
            $data['selectedHotel'] = $request->get('hotel_id');
            $data['previousBills'] = HotelBill::where('hotel_id', $request->get('hotel_id'))->get();
        } else {
            $data['hotels'] = Order::select(DB::raw('orders.hotel_id as hotelId, orders.hotel_name as hotelName'))
                                    ->distinct('hotel_id')
                                    ->groupBy('hotel_id')
                                    ->get();
            $data['selectedHotel'] = '';
        }
            $currentMonth = date('m');
            $current_year = Carbon::now()->format('Y');
            $data['current_year'] = $current_year;
            $data['current_month'] = date('F');
        if($request->get('hotel_id') == Null){
            // today
            $data['today_revenue'] = Order::where('delivery_status','=','completed')->whereDate('created_at' ,Carbon::today())->sum('final_amount');
            $data['today_order_successful'] = Order::where('delivery_status','=','completed')->whereDate('created_at' ,Carbon::today())->count();
            $data['today_order_rejected'] = Order::where('delivery_status','=','rejected')->whereDate('created_at' ,Carbon::today())->count();
            $data['today_order_inprogress'] = Order::where('delivery_status','=','inprogress')->whereDate('created_at' ,Carbon::today())->count();
            // month
            $data['current_month_revenue'] = Order::where('delivery_status','=','completed')->whereRaw('MONTH(created_at)=?',[$currentMonth])->sum('final_amount');
            $data['current_month_successful'] = Order::where('delivery_status','=','completed')->whereRaw('MONTH(created_at)=?',[$currentMonth])->count();
            $data['current_month_rejected'] = Order::where('delivery_status','=','rejected')->whereRaw('MONTH(created_at)=?',[$currentMonth])->count();
            $data['current_month_inprogress'] = Order::where('delivery_status','=','inprogress')->whereRaw('MONTH(created_at)=?',[$currentMonth])->count();
            // year
            $data['year_revenue'] = Order::where('delivery_status','=','completed')->whereYear('created_at', '=', $current_year)->sum('final_amount');
            $data['year_successful'] = Order::where('delivery_status','=','completed')->whereYear('created_at', '=', $current_year)->count();
            $data['year_rejected'] = Order::where('delivery_status','=','rejected')->whereYear('created_at', '=', $current_year)->count();
            $data['year_inprogress'] = Order::where('delivery_status','=','inprogress')->whereYear('created_at', '=', $current_year)->count();
            // total revenue
            $data['total_amount'] = Order::where('delivery_status','=','completed')->sum('final_amount');
        }else{
            // today
            $data['today_revenue'] = Order::where('hotel_id','=',$request->get('hotel_id'))->where('delivery_status','=','completed')->whereDate('created_at' ,Carbon::today())->sum('final_amount');
            $data['today_order_successful'] = Order::where('hotel_id','=',$request->get('hotel_id'))->where('delivery_status','=','completed')->whereDate('created_at' ,Carbon::today())->count();
            $data['today_order_rejected'] = Order::where('hotel_id','=',$request->get('hotel_id'))->where('delivery_status','=','rejected')->whereDate('created_at' ,Carbon::today())->count();
            $data['today_order_inprogress'] = Order::where('hotel_id','=',$request->get('hotel_id'))->where('delivery_status','=','inprogress')->whereDate('created_at' ,Carbon::today())->count();
            // month
            $data['current_month_revenue'] = Order::where('hotel_id','=',$request->get('hotel_id'))->where('delivery_status','=','completed')->whereRaw('MONTH(created_at)=?',[$currentMonth])->sum('final_amount');
            $data['current_month_successful'] = Order::where('hotel_id','=',$request->get('hotel_id'))->where('delivery_status','=','completed')->whereRaw('MONTH(created_at)=?',[$currentMonth])->count();
            $data['current_month_rejected'] = Order::where('hotel_id','=',$request->get('hotel_id'))->where('delivery_status','=','rejected')->whereRaw('MONTH(created_at)=?',[$currentMonth])->count();
            $data['current_month_inprogress'] = Order::where('hotel_id','=',$request->get('hotel_id'))->where('delivery_status','=','inprogress')->whereRaw('MONTH(created_at)=?',[$currentMonth])->count();
            // year
            $data['year_revenue'] = Order::where('hotel_id','=',$request->get('hotel_id'))->where('delivery_status','=','completed')->whereYear('created_at', '=', $current_year)->sum('final_amount');
            $data['year_successful'] = Order::where('hotel_id','=',$request->get('hotel_id'))->where('delivery_status','=','completed')->whereYear('created_at', '=', $current_year)->count();
            $data['year_rejected'] = Order::where('hotel_id','=',$request->get('hotel_id'))->where('delivery_status','=','rejected')->whereYear('created_at', '=', $current_year)->count();
            $data['year_inprogress'] = Order::where('hotel_id','=',$request->get('hotel_id'))->where('delivery_status','=','inprogress')->whereYear('created_at', '=', $current_year)->count();
            // total revenue
            $data['total_amount'] = Order::where('hotel_id','=',$request->get('hotel_id'))->where('delivery_status','=','completed')->sum('final_amount');
        }
        return view('admin.analytics.index', $data);
    }

    public function generateHotelBill($hotel_id)
    {
        $latestBill = HotelBill::where('hotel_id', $hotel_id)->orderBy('hotel_bill_id', 'DESC')->first();
        $today = Carbon::now();
        if ($latestBill != null) {
            $fromDate = date($latestBill->bill_to_date);
        } else {
            $fromDate = date('2021-01-01');
        }

        $orders = Order::where('hotel_id', $hotel_id)
                        ->where('delivery_status', 'completed')
                        ->whereBetween('order_date', [$fromDate, $today])
                        ->get();
        $hotel_detail = Hotel::with('hotel_registerdetails')->where('id', $hotel_id)->first();
        $data['fromDate'] = $fromDate;
        $data['toDate'] = $today;

        $checkBillExits = HotelBill::where('hotel_id', $hotel_id)
                            ->where('bill_to_date', date('Y-m-d'))
                            ->first();
        
        $bill = new HotelBill();
        $bill->hotel_id = $hotel_id;
        $bill->hotel_slug = $hotel_detail->slug;
        $bill->hotel_name = $hotel_detail->name;
        $bill->bill_from_date = $fromDate;
        $bill->bill_to_date = $today;
        $totalAmount = 0;
        foreach($orders as $order) {
            $totalAmount += $order->final_amount;
        }
        $bill->total_amount = $totalAmount;
        $bill->commission = $hotel_detail->hotel_registerdetails->commission;

        $commissionAmount = ($totalAmount*$hotel_detail->hotel_registerdetails->commission)/100;

        $bill->commission_amount = $commissionAmount;
        $bill->pay_out = $totalAmount-$commissionAmount;
        if($checkBillExits != null) {
            $data['generatedBillId'] = $checkBillExits->hotel_bill_id;
        } else {
            $bill->save();
            $data['generatedBillId'] = $bill->hotel_bill_id;
        }

        $data['orders'] = $orders;
        $data['hotel_detail'] = $hotel_detail;

        return view('admin.analytics.generate-bill', $data);
    }

    public function viewHotelBill($id)
    {
        $bill = HotelBill::where('hotel_bill_id', $id)->first();
        $orders = Order::where('hotel_id', $bill->hotel_id)
                        ->whereBetween('order_date', [$bill->bill_from_date, $bill->bill_to_date])
                        ->get();
        $hotel_detail = Hotel::with('hotel_registerdetails')->where('id', $bill->hotel_id)->first();
        $data['fromDate'] = $bill->bill_from_date;
        $data['toDate'] = $bill->bill_to_date;
        
        $data['generatedBillId'] = $bill->hotel_bill_id;

        $data['orders'] = $orders;
        $data['hotel_detail'] = $hotel_detail;

        return view('admin.analytics.generate-bill', $data);
    }
}
