<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Alert;
use Session;
class AlertController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $alerts = Alert::All();
        return view('admin.alert.index',compact('alerts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.alert.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        if($request->file('alert_image')){
            $filename = $request->file('alert_image');
            $filename->getClientOriginalExtension();
            $filename_p = $filename->store('public/alert_image');
            $filename_path = str_replace('public/alert_image/', '', $filename_p);
            $input['alert_image'] =  $filename_path;
         }
         $create = Alert::create($input);

         if($create){
            Session::flash('success', 'Successfully Created!');
            return redirect('admin/manage-alerts');
       }else{
           Session::flash('error', 'Something went wrong!');
           return redirect('admin/manage-alerts');
       }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $alert = Alert::findOrFail($id);
        return view('admin.alert.edit',compact('alert'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $alert = Alert::findOrFail($id);
        if($request->file('alert_image')){
            $filename = $request->file('alert_image');
            $filename->getClientOriginalExtension();
            $filename_p = $filename->store('public/alert_image');
            $filename_path = str_replace('public/alert_image/', '', $filename_p);
            $input['alert_image'] =  $filename_path;
         }
         $update = $alert->update($input);

         if($update){
            Session::flash('success', 'Successfully Updated!');
            return redirect('admin/manage-alerts');
       }else{
           Session::flash('error', 'Something went wrong!');
           return redirect('admin/manage-alerts');
       }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Alert::findOrFail($id)->delete();
        if($delete){
            Session::flash('success', 'Successfully deleted!');
            return redirect()->back();
        }else{
           Session::flash('error', 'Something went wrong!');
           return redirect()->back();
        }
    }

    public function status_update(Request $request,$id){
        $alert = Alert::findOrFail($id);
        if($request->status == "status"){
            if($alert->alert_status == 1){
                $status['alert_status'] = 0;
                $alert->update($status);
                return redirect()->back();
            }else if($alert->alert_status == 0 OR $alert->alert_status == null){
                $status['alert_status'] = 1;
                $alert->update($status);
                return redirect()->back();
            }

        }
    }
}
