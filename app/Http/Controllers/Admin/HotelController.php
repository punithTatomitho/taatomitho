<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Hotel;
use App\Models\Cuisine;
use App\Models\City;
use App\Models\HotelRegisterDetail;
use Session;
use Illuminate\Support\Arr;
use Response;
use DB;

class HotelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hotels = Hotel::All();
        return view('admin.hotel.index',compact('hotels'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cuisines = Cuisine::where('status','=',1)->get();
        return view('admin.hotel.create',compact('cuisines'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $request->validate([
            'name' => ['required'],
            'email' => ['required'],
            'phone' => ['required'],
            'start_date' => ['required'],
            'end_date' => ['required'],
            'cuisines' => ['required'],
            'city' => ['required'],
            'registeration_charge' => ['required'],
            'accept_orders_upto' => ['required'],
            // 'hotel_banner_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048|dimensions:width=1360,height=425',
            // 'hotel_thumbnail_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048|dimensions:width=1024,height=1024',
        ]);

        $input = Arr::except($request->all(), ['_token','hotel_thumbnail_image','hotel_thumbnail_image']);
        if(in_array('Accept Deliveries',$request->get('order_details'))){
            $request->validate([
                'accept_deliveries' => ['required'],
            ]);
            $input['accept_deliveries'] = $request->get('accept_deliveries');
        }else{
            $input['accept_deliveries'] = Null;
        }
        if(in_array('Accept Collections',$request->get('order_details'))){
            $request->validate([
                'accept_collections' => ['required'],
            ]);
            $input['accept_collections'] = $request->get('accept_collections');
        }else{
            $input['accept_collections'] = null;
        }
        $input['slug'] = $request->get('name');
        $city_data['name'] = $request->get('city');
        $city_data['slug'] = $request->get('city');
        $city_data['status'] = 1;
        $city_data['papular_status'] = 1;
        $city = City::create($city_data);
        if($city){
            $input['city_id'] = $city->id;
            $input['promotion_status'] = 1;
            $input['hotel_status'] = 1;
            $input['list_status'] = 1;
            $input['offers_status'] = 1;
            $input['other_details'] = json_encode($request->get('other_details'));
            $input['cuisines'] = json_encode($request->get('cuisines'));
            $input['order_details'] = json_encode($request->get('order_details'));
            $input['payment_details'] = json_encode($request->get('payment_details'));
            $input['accommodation_details'] = json_encode($request->get('accommodation_details'));
            if($request->file('hotel_banner_image')){
                $filename = $request->file('hotel_banner_image');
                $filename->getClientOriginalExtension();
                $filename_p = $filename->store('public/hotel_banner_image');
                $filename_path = str_replace('public/hotel_banner_image/', '', $filename_p);
                $input['hotel_banner_image'] =  $filename_path;
            }
            if($request->file('hotel_thumbnail_image')){
                $filename = $request->file('hotel_thumbnail_image');
                $filename->getClientOriginalExtension();
                $filename_tp = $filename->store('public/hotel_thumbnail_image');
                $filename_tpath = str_replace('public/hotel_thumbnail_image/', '', $filename_tp);
                $input['hotel_thumbnail_image'] =  $filename_tpath;
            }
            $hotel = Hotel::create($input);
        }
        if($hotel){
                $registerdata['hotel_id'] = $hotel->id;
                $registerdata['commission'] = $request->get('commission');
                $registerdata['payout_frequency'] = $request->get('payout_frequency');
                $registerdata['registeration_charge'] = $request->get('registeration_charge');
                $registerdata['accept_orders_upto'] = $request->get('accept_orders_upto');
                $registerdata['start_date'] = $request->get('start_date');
                $registerdata['end_date'] = $request->get('end_date');
                $registerdata['comments'] = $request->get('comments');
                $registerdata['offers_text'] = $request->get('offers_text');
                $registerdata['groccery_details'] = json_encode($request->get('groccery_details'));
                $registerdata['internal_store_details'] = json_encode($request->get('internal_store_details'));
                $register_data = HotelRegisterDetail::create($registerdata);
                if($register_data){
                    Session::flash('success', 'Successfully Created!');
                    return redirect('admin/manage-hotel');
                }else{
                    Session::flash('error', 'Something went wrong!');
                    return redirect('admin/manage-hotel');
                }
            }else{
                Session::flash('error', 'Something went wrong!');
                return redirect('admin/manage-hotel');
            }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cuisines = Cuisine::where('status','=',1)->get();
        $hotel = Hotel::findOrFail($id);
        return view('admin.hotel.edit',compact('cuisines','hotel'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $hotel_data = Hotel::findOrFail($id);
        $request->validate([
            'name' => ['required'],
            'email' => ['required'],
            'phone' => ['required'],
            'start_date' => ['required'],
            'end_date' => ['required'],
            'cuisines' => ['required'],
            'city' => ['required'],
            'registeration_charge' => ['required'],
            'accept_orders_upto' => ['required'],
            // 'hotel_banner_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048|dimensions:width=1360,height=425',
            // 'hotel_thumbnail_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048|dimensions:width=1024,height=1024',
        ]);

        $input = $request->all();
        if(in_array('Accept Deliveries',$request->get('order_details'))){
            $request->validate([
                'accept_deliveries' => ['required'],
            ]);
            $input['accept_deliveries'] = $request->get('accept_deliveries');
        }else{
            $input['accept_deliveries'] = Null;
        }
        if(in_array('Accept Collections',$request->get('order_details'))){
            $request->validate([
                'accept_collections' => ['required'],
            ]);
            $input['accept_collections'] = $request->get('accept_collections');
        }else{
            $input['accept_collections'] = null;
        }
        // $input['slug'] = $request->get('name');
        $city_data['name'] = $request->get('city');
        // $city_data['slug'] = $request->get('city');
        $city_id = $hotel_data->city_id;
        $city_find = City::findOrFail($city_id);
        $city = $city_find->update($city_data);
        if($city){
            $input['city_id'] = $city_find->id;
            $input['other_details'] = json_encode($request->get('other_details'));
            $input['cuisines'] = json_encode($request->get('cuisines'));
            $input['order_details'] = json_encode($request->get('order_details'));
            $input['payment_details'] = json_encode($request->get('payment_details'));
            $input['accommodation_details'] = json_encode($request->get('accommodation_details'));
            $data = Arr::except($request->all(), ['_token','hotel_thumbnail_image','hotel_thumbnail_image']);
                if($request->file('hotel_banner_image') != null){
                    $filename = $request->file('hotel_banner_image');
                    $filename->getClientOriginalExtension();
                    $filename_p = $filename->store('public/hotel_banner_image');
                    $filename_path = str_replace('public/hotel_banner_image/', '', $filename_p);
                    $input['hotel_banner_image'] =  $filename_path;
                    $hotel_data = Hotel::findOrFail($id);
                    $hotel = $hotel_data->update($input);
                }

                if($request->file('hotel_thumbnail_image') != null){
                    $filename = $request->file('hotel_thumbnail_image');
                    $filename->getClientOriginalExtension();
                    $filename_tp = $filename->store('public/hotel_thumbnail_image');
                    $filename_tpath = str_replace('public/hotel_thumbnail_image/', '', $filename_tp);
                    $input['hotel_thumbnail_image'] =  $filename_tpath;
                    $hotel_data = Hotel::findOrFail($id);
                    $hotel = $hotel_data->update($input);
            }
            
            $hotel_data = Hotel::findOrFail($id);
            $hotel = $hotel_data->update($input);
        }
        if($hotel){
                $registerdata['hotel_id'] = $hotel_data->id;
                $registerdata['commission'] = $request->get('commission');
                $registerdata['payout_frequency'] = $request->get('payout_frequency');
                $registerdata['registeration_charge'] = $request->get('registeration_charge');
                $registerdata['accept_orders_upto'] = $request->get('accept_orders_upto');
                $registerdata['start_date'] = $request->get('start_date');
                $registerdata['end_date'] = $request->get('end_date');
                $registerdata['comments'] = $request->get('comments');
                $registerdata['offers_text'] = $request->get('offers_text');
                $registerdata['groccery_details'] = json_encode($request->get('groccery_details'));
                $registerdata['internal_store_details'] = json_encode($request->get('internal_store_details'));
                $register_find = HotelRegisterDetail::where('hotel_id','=',$id);
                $register_data = $register_find->update($registerdata);
                if($register_data){
                    Session::flash('success', 'Successfully Updated!');
                    return redirect('admin/manage-hotel');
                }else{
                    Session::flash('error', 'Something went wrong!');
                    return redirect('admin/manage-hotel');
                }
            }else{
                Session::flash('error', 'Something went wrong!');
                return redirect('admin/manage-hotel');
            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Hotel::findOrFail($id)->delete();
        if($delete){
            Session::flash('success', 'Successfully deleted!');
            return redirect()->back();
        }else{
           Session::flash('error', 'Something went wrong!');
           return redirect()->back();
        }
    }

    public function status_update(Request $request,$id)
    {
        $hotel_status = Hotel::findOrFail($id);
        if($request->status == "offers_status"){
            if($hotel_status->offers_status == 1){
                $status['offers_status'] = 0;
                $hotel_status->update($status);
                return redirect()->back();
            }else if($hotel_status->offers_status == 0 OR $hotel_status->offers_status == null){
                $status['offers_status'] = 1;
                $hotel_status->update($status);
                return redirect()->back();
            }
        }else if($request->status == "promotion_status"){
            if($hotel_status->promotion_status == 1){
                $status['promotion_status'] = 0;
                $hotel_status->update($status);
                return redirect()->back();
            }else if($hotel_status->promotion_status == 0 OR $hotel_status->promotion_status == null){
                $status['promotion_status'] = 1;
                $hotel_status->update($status);
                return redirect()->back();
            }
        }else if($request->status == "hotel_status"){
            if($hotel_status->hotel_status == 1){
                $status['hotel_status'] = 0;
                $hotel_status->update($status);
                return redirect()->back();
            }else if($hotel_status->hotel_status == 0 OR $hotel_status->hotel_status == null){
                $status['hotel_status'] = 1;
                $hotel_status->update($status);
                return redirect()->back();
            }
        }else if($request->status == "list_status"){
            if($hotel_status->list_status == 1){
                $status['list_status'] = 0;
                $hotel_status->update($status);
                return redirect()->back();
            }else if($hotel_status->list_status == 0 OR $hotel_status->list_status == null){
                $status['list_status'] = 1;
                $hotel_status->update($status);
                return redirect()->back();
            }
        }
    }

    public function importHotels(Request $request){
        $file = request()->file('file');
        $file = fopen($file,"r");
        $allCsv = [];
        $insertdata = [];
            while(! feof($file))
            {
                $data = fgetcsv($file);
                if($data != null){
                    $tempdata = array(
                        'name'=>$data[0],
                        'tag'=>$data[1],
                        'description'=>$data[2],
                        'meta_tag' => $data[3],
                        'phone' => $data[4],
                        'email' => $data[5],
                        'contact_name' =>$data[6],
                        'contact_phone' => $data[7],
                        'hotel_banner_image' => $data[8],
                        'hotel_thumbnail_image' => $data[9],
                        'allergy_indication' =>$data[10],
                        'facebook_id' =>$data[11],
                        'twitter_id' => $data[12],
                        'range' => $data[13],
                        'latitude' => $data[14],
                        'longitude' => $data[15],
                        'google_place_id' => $data[16],
                        'address' => $data[17],
                        'street'=>$data[18],
                        'district' =>$data[19],
                        'ward' => $data[20],
                        'zip_code' => $data[21],
                        'city' => $data[22],
                        'delivery_areas' => $data[23],
                        'min_order_amount' => $data[24],
                        'delivery_radius_miles' => $data[25],
                        'delivery_charge' => $data[26],
                        'free_delivery_amount' => $data[27],
                        'vat' => $data[28],
                        'service_charge' => $data[29],
                        'payment_details' => explode(',', $data[30]),
                        'other_details' => explode(',', $data[31]),
                        'order_details' => explode(',', $data[32]),
                        'accommodation_details' => explode(',', $data[33]),
                        'accept_deliveries' => $data[34],
                        'accept_collections' => $data[35],
                        'billing_address' => $data[36],
                        'promotion_status' => $data[37],
                        'hotel_status' => $data[38],
                        'list_status' => $data[39],
                        'offers_status' => $data[40],
                        'cuisines' => explode(',', $data[41]),
                        'commission' => $data[42],
                        'payout_frequency' => $data[43],
                        'registeration_charge' => $data[44],
                        'accept_orders_upto' => $data[45],
                        'start_date' => $data[46],
                        'end_date' => $data[47],
                        'comments' => $data[48],
                        'offers_text' => $data[49],
                        'groccery_details' => explode(',', $data[50]),
                        'internal_store_details' => explode(',', $data[51]),
                    );
                        array_push($allCsv, $tempdata);
                }
                else{
                Session::flash('error', 'Please check the csv file format!');
                    return redirect()->back();
                }
            }
            fclose($file);
            if(in_array('city',$allCsv[0]) and in_array('name',$allCsv[0])){
                    $delete_hotel = DB::table('hotels')->delete();
                    if($delete_hotel){
                        $delete_hotelregister = DB::table('hotel_register_details')->delete();
                    }
                array_splice($allCsv,0,1);
                foreach ($allCsv as $csv) {
                        $check_city = City::where('name','=',$csv['city'])->first();
                        if($check_city == null){
                            $city_create = new City;
                            $cityData['name'] = $csv['city'];
                            $cityData['slug'] = $csv['city'];
                            $cityData['sort_order'] = 0;
                            $cityData['status'] = 1;
                            $cityData['popular_status'] = 1;
                            $city = $city_create->create($cityData);
                        }else{
                            $city = City::where('name','=',$csv['city'])->first();
                        }
                    
                        if($city){
                            $hotels = new Hotel;
                                $hotelData['name'] = $csv['name'];
                                $hotelData['slug'] = $csv['name'];
                                $hotelData['description'] = $csv['description'];
                                $hotelData['tag'] = $csv['tag'];
                                $hotelData['meta_tag'] = $csv['meta_tag'];
                                $hotelData['phone'] = $csv['phone'];
                                $hotelData['email'] = $csv['email'];
                                $hotelData['contact_name'] = $csv['contact_name'];
                                $hotelData['contact_phone'] = $csv['contact_phone'];
                                $hotelData['hotel_banner_image'] = $csv['hotel_banner_image'];
                                $hotelData['hotel_thumbnail_image'] = $csv['hotel_thumbnail_image'];
                                $hotelData['allergy_indication'] = $csv['allergy_indication'];
                                $hotelData['facebook_id'] = $csv['facebook_id'];
                                $hotelData['twitter_id'] = $csv['twitter_id'];
                                $hotelData['range'] = $csv['range'];
                                $hotelData['latitude'] = $csv['latitude'];
                                $hotelData['longitude'] = $csv['longitude'];
                                $hotelData['google_place_id'] = $csv['google_place_id'];
                                $hotelData['address'] = $csv['address'];
                                $hotelData['street'] = $csv['street'];
                                $hotelData['district'] = $csv['district'];
                                $hotelData['ward'] = $csv['ward'];
                                $hotelData['zip_code'] = $csv['zip_code'];
                                $hotelData['city'] = $csv['city'];
                                $hotelData['delivery_areas'] = $csv['delivery_areas'];
                                $hotelData['min_order_amount'] = $csv['min_order_amount'];
                                $hotelData['delivery_radius_miles'] = $csv['delivery_radius_miles'];
                                $hotelData['delivery_charge'] = $csv['delivery_charge'];
                                $hotelData['free_delivery_amount'] = $csv['free_delivery_amount'];
                                $hotelData['vat'] = $csv['vat'];
                                $hotelData['service_charge'] = $csv['service_charge'];
                                $hotelData['other_details'] = json_encode($csv['other_details']);
                                $hotelData['cuisines'] = json_encode($csv['cuisines']);
                                $hotelData['order_details'] = json_encode($csv['order_details']);
                                $hotelData['payment_details'] = json_encode($csv['payment_details']);
                                $hotelData['accommodation_details'] = json_encode($csv['accommodation_details']);
                                $hotelData['accept_deliveries'] = $csv['accept_deliveries'];
                                $hotelData['accept_collections'] = $csv['accept_collections'];
                                $hotelData['billing_address'] = $csv['billing_address'];
                                $hotelData['promotion_status'] = $csv['promotion_status'];
                                $hotelData['hotel_status'] = $csv['hotel_status'];
                                $hotelData['list_status'] = $csv['list_status'];
                                $hotelData['offers_status'] = $csv['offers_status'];
                                $hotelData['city_id'] = $city->id;
                                $hotel = $hotels->create($hotelData);
                                if($hotel){
                                    $hotel_registerDetails = new HotelRegisterDetail;
                                    $registerdata['hotel_id'] =  $hotel->id;
                                    $registerdata['commission'] = $csv['commission'];
                                    $registerdata['payout_frequency'] = $csv['payout_frequency'];
                                    $registerdata['registeration_charge'] = $csv['registeration_charge'];
                                    $registerdata['accept_orders_upto'] = $csv['accept_orders_upto'];
                                    $registerdata['start_date'] = $csv['start_date'];
                                    $registerdata['end_date'] = $csv['end_date'];
                                    $registerdata['comments'] = $csv['comments'];
                                    $registerdata['offers_text'] = $csv['offers_text'];
                                    $registerdata['groccery_details'] = json_encode($csv['groccery_details']);
                                    $registerdata['internal_store_details'] = json_encode($csv['internal_store_details']);
                                    $regiterDetails = $hotel_registerDetails->create($registerdata);
                                }
                                array_push($insertdata,$csv);
                        }
                    }

                if(count($insertdata) > 0){
                    Session::flash('success', 'Successfully Items Added!');
                    return redirect()->back();
                }else{
                    Session::flash('error', 'Item Details are not Found ? Please check the csv file format!');
                    return redirect()->back();
                }
            }else{

                Session::flash('error', 'Please check the csv file format!');
                return redirect()->back();
            }

    }   
}
