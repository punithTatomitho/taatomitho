<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Cuisine;
use App\Models\CuisineType;
use Session;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class CuisineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cuisines = Cuisine::All();
        return view('admin.cuisine.index',compact('cuisines'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cuisine_types = CuisineType::where('status','=',1)->get();
        return view('admin.cuisine.create',compact('cuisine_types'));    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = Arr::except($request->all(), ['_token','image']);
        if($request->file('image')){
            $filename = $request->file('image');
            $filename->getClientOriginalExtension();
            $filename_p = $filename->store('public/cuisine_image');
            $filename_path = str_replace('public/cuisine_image/', '', $filename_p);
            $data['image'] =  $filename_path;
         }
         $data['slug'] = $request->get('name');
         $create = Cuisine::create($data);
         if($create){
             Session::flash('success', 'Successfully created!');
             return redirect('admin/manage-cuisine');
         }else{
            Session::flash('error', 'Something went wrong!');
            return redirect('admin/manage-cuisine');
         }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cuisine = Cuisine::findOrFail($id);
        return view('admin.cuisine.show',compact('cuisine'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cuisine = Cuisine::findOrFail($id);
        $cuisine_types = CuisineType::where('status','=',1)->get();
        return view('admin.cuisine.edit',compact('cuisine','cuisine_types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Arr::except($request->all(), ['_token','image']);
        if($request->file('image')){
            $filename = $request->file('image');
            $filename->getClientOriginalExtension();
            $filename_p = $filename->store('public/cuisine_image');
            $filename_path = str_replace('public/cuisine_image/', '', $filename_p);
            $data['image'] =  $filename_path;
         }
         $data['slug'] = $request->get('name');
         $cuisine = Cuisine::findOrFail($id);
         $update = $cuisine->update($data);
         if($update){
             Session::flash('success', 'Successfully updated!');
             return redirect('admin/manage-cuisine');
         }else{
            Session::flash('error', 'Something went wrong!');
            return redirect('admin/manage-cuisine');
         }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Cuisine::findOrFail($id)->delete();
        if($delete){
            Session::flash('success', 'Successfully deleted!');
            return redirect()->back();
        }else{
           Session::flash('error', 'Something went wrong!');
           return redirect()->back();
        }
    }

    public function status_update($id)
    {
        $status = Cuisine::findOrFail($id);
        if($status->status == 1){
            $c_status['status'] = 0;
            $status->update($c_status);
            return redirect()->back();
        }else if($status->status == 0){
            $c_status['status'] = 1;
            $status->update($c_status);
            return redirect()->back();
        }
    }
}
