<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Slider;
use App\Models\Category;
use App\Models\Hotel;
use Session;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sliders = Slider::All();
        return view('admin.slider.index',compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::where('status','=',1)->get();
        $hotels = Hotel::where('offers_status', 1)->get();
        return view('admin.slider.create',compact('categories','hotels'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        // $image = Arr::except($request->all(), ['_token','icon_image','background_image']);
    
         if($request->file('background_image')){
            $filename1 = $request->file('background_image');
            $filename1->getClientOriginalExtension();
            $filename_p1 = $filename1->store('public/slider_bg_image');
            $filename_path2 = str_replace('public/slider_bg_image/', '', $filename_p1);
            $data['background_image'] =  $filename_path2;
         }

         if($request->file('icon_image')){
            $filename2 = $request->file('icon_image');
            $filename2->getClientOriginalExtension();
            $filename_p2 = $filename2->store('public/slider_bg_image');
            $filename_path2 = str_replace('public/slider_bg_image/', '', $filename_p2);
            $data['icon_image'] =  $filename_path2;
         }
         if($request->get('promotion_status') == null){
            $data['promotion_status'] = 'no';
         }else{
            $data['promotion_status'] = $request->get('promotion_status');
         }

         if($request->get('redirect_to') == 'Restaurants') {
            $data['redirect_to'] = env('APP_URL').'/restaurants';
         }
         if($request->get('redirect_to') == 'Groceries') {
            $data['redirect_to'] = env('APP_URL').'/groceries';
         }

         if($request->get('redirect_to') != 'Restaurants' && $request->get('redirect_to') != 'Groceries') {
            $data['redirect_to'] = env('APP_URL').'/restaurants/'.$request->get('redirect_to');
         }
         $data['slider_status'] = 1;
         $create = Slider::create($data);
         if($create){
             Session::flash('success', 'Successfully created!');
             return redirect('admin/manage-slider');
         }else{
            Session::flash('error', 'Something went wrong!');
            return redirect('admin/manage-slider');
         }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Category::where('status','=',1)->get();
        $slider = Slider::findOrFail($id);
        $hotels = Hotel::where('offers_status', 1)->get();
        return view('admin.slider.edit',compact('categories','hotels','slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        // $image = Arr::except($request->all(), ['_token','icon_image','background_image']);
    
         if($request->file('background_image')){
            $filename1 = $request->file('background_image');
            $filename1->getClientOriginalExtension();
            $filename_p1 = $filename1->store('public/slider_bg_image');
            $filename_path2 = str_replace('public/slider_bg_image/', '', $filename_p1);
            $data['background_image'] =  $filename_path2;
         }

         if($request->file('icon_image')){
            $filename2 = $request->file('icon_image');
            $filename2->getClientOriginalExtension();
            $filename_p2 = $filename2->store('public/slider_bg_image');
            $filename_path2 = str_replace('public/slider_bg_image/', '', $filename_p2);
            $data['icon_image'] =  $filename_path2;
         }
         if($request->get('promotion_status') == null){
            $data['promotion_status'] = 'no';
         }else{
            $data['promotion_status'] = $request->get('promotion_status');
         }
         if($request->get('redirect_to') == 'Restaurants') {
            $data['redirect_to'] = env('APP_URL').'/restaurants';
         }
         if($request->get('redirect_to') == 'Groceries') {
            $data['redirect_to'] = env('APP_URL').'/groceries';
         }

         if($request->get('redirect_to') != 'Restaurants' && $request->get('redirect_to') != 'Groceries') {
            $data['redirect_to'] = env('APP_URL').'/restaurants/'.$request->get('redirect_to');
         }
         
         $slider = Slider::findOrFail($id);
         $update = $slider->update($data);
         if($update){
             Session::flash('success', 'Successfully Updated!');
             return redirect('admin/manage-slider');
         }else{
            Session::flash('error', 'Something went wrong!');
            return redirect('admin/manage-slider');
         }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Slider::findOrFail($id)->delete();
        if($delete){
            Session::flash('success', 'Successfully deleted!');
            return redirect()->back();
        }else{
           Session::flash('error', 'Something went wrong!');
           return redirect()->back();
        }
    }

    public function status_update(Request $request,$id){
      $slider = Slider::findOrFail($id);
      if($request->status == "status"){
          if($slider->slider_status == 1){
              $status['slider_status'] = 0;
              $slider->update($status);
              return redirect()->back();
          }else if($slider->slider_status == 0 OR $slider->slider_status == null){
              $status['slider_status'] = 1;
              $slider->update($status);
              return redirect()->back();
          }

      }
  }
}
