<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;

class IndexController extends Controller
{
    public function dashboardIndex(){
        return redirect('admin/manage-orders/'.'index');
    }

    public function consloeIndex(){
        return redirect('admin/manage-hotel');
    }

    public function kioskIndex(){
        return redirect('admin/manage-role');
    }
        
}
