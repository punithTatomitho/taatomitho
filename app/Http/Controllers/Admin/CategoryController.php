<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use Session;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::All();
        return view('admin.category.index',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input['category_name'] = ucfirst($request->get('category_name'));
        $data = Category::create($input);

        if($data){
                 Session::flash('success', 'Successfully Created!');
                 return redirect('admin/manage-category');
            }else{
                Session::flash('error', 'Something went wrong!');
                return redirect('admin/manage-category');
            }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::findOrFail($id);
        return view('admin.category.edit',compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input['category_name'] = ucfirst($request->get('category_name'));
        $category = Category::findOrFail($id);
        $update = $category->update($input);
        if($update){
                 Session::flash('success', 'Successfully Updated!');
                 return redirect('admin/manage-category');
            }else{
                Session::flash('error', 'Something went wrong!');
                return redirect('admin/manage-category');
            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Category::findOrFail($id)->delete();
        if($delete){
            Session::flash('success', 'Successfully deleted!');
            return redirect()->back();
        }else{
           Session::flash('error', 'Something went wrong!');
           return redirect()->back();
        }
    }

    public function status_update(Request $request,$id){
       $category = Category::findOrFail($id);
        if($request->status == "status"){
            if($category->status == 1){
                $status['status'] = 0;
               $category->update($status);
                return redirect()->back();
            }else if($category->status == 0 OR $category->status == null){
                $status['status'] = 1;
                $category->update($status);
                return redirect()->back();
            }
  
        }
    }
}
