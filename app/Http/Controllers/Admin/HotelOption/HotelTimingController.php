<?php

namespace App\Http\Controllers\Admin\HotelOption;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Hotel;
use App\Models\HotelTiming;
use App\Models\HotelShifTiming;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Session;

class HotelTimingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request,$id)
    {
        $hotel = Hotel::findOrFail($id);
        $timings = HotelTiming::where('hotel_id','=',$id)->first();
        if($timings == null){
            return view('admin.hotel_options.timings.create',compact('timings','hotel'));
        }else{
            return view('admin.hotel_options.timings.index',compact('timings','hotel'));
        }
        

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $hotel_id = $request->get('hotel_id');
            foreach ($request->addmore as $key => $value) {
                $data['hotel_id'] = $hotel_id;
                $data['monday_start_at'] = $value['monday_start_at'];
                $data['monday_end_at'] = $value['monday_end_at'];
                $data['tuesday_start_at'] = $value['tuesday_start_at'];
                $data['tuesday_end_at'] = $value['tuesday_end_at'];
                $data['wednesday_start_at'] = $value['wednesday_start_at'];
                $data['wednesday_end_at'] = $value['wednesday_end_at'];
                $data['thursday_start_at'] = $value['thursday_start_at'];
                $data['thursday_end_at'] = $value['thursday_end_at'];
                $data['friday_start_at'] = $value['friday_start_at'];
                $data['friday_end_at'] = $value['friday_end_at'];
                $data['saturday_start_at'] = $value['saturday_start_at'];
                $data['saturday_end_at'] = $value['saturday_end_at'];
                $data['sunday_start_at'] = $value['sunday_start_at'];
                $data['sunday_end_at'] = $value['sunday_end_at'];
                $check = HotelTiming::where('hotel_id','=',$hotel_id)->first();
                if($check != null){
                    $timings = HotelTiming::where('hotel_id','=',$hotel_id);
                    $update = $timings->update($data);
                    Session::flash('success', 'Successfully updated!');
                    return redirect('admin/manage-hotel-timings_index/'.$hotel_id);
                }else{
                    $create = HotelTiming::create($data);
                    Session::flash('success', 'Successfully Created!');
                    return redirect('admin/manage-hotel-timings_index/'.$hotel_id);
                }
                Session::flash('error', 'Something went wrong!');
                return redirect('admin/manage-hotel-timings_index/'.$hotel_id);
                }        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
