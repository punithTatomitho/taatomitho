<?php

namespace App\Http\Controllers\Admin\HotelOption;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use App\Models\HotelItem;
use App\Models\HotelCategory;
use App\Models\Hotel;
use Response;

class HotelItemController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request,$id)
    {
        $hotel = Hotel::findOrFail($id);
        $items = HotelItem::where('hotel_id','=',$id)->get();
        return view('admin.hotel_options.items.index',compact('items','hotel'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request,$id)
    {
        $hotel = Hotel::find($id);
        $categories = HotelCategory::where('hotel_id', $id)->where('status','=',1)->get();
        return view('admin.hotel_options.items.create',compact('hotel','categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input= $request->all();
        $data = Arr::except($request->all(), ['_token','image']);
        if($request->file('image')){
            $filename = $request->file('image');
            $filename->getClientOriginalExtension();
            $filename_p = $filename->store('public/item_image');
            $filename_path = str_replace('public/item_image/', '', $filename_p);
            $input['image'] =  $filename_path;
        }
        $input['hotel_id'] = $request->get('hotel_id');
        $input['segments'] = json_encode($request->get('segments'));
        $data = HotelItem::create($input);
        if($data){
                 Session::flash('success', 'Successfully Created!');
                 return redirect('admin/manage-hotel-item_index/'.$input['hotel_id']);
            }else{
                Session::flash('error', 'Something went wrong!');
                return redirect('admin/manage-hotel-item_index/'.$input['hotel_id']);
            }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request ,$id)
    {
        $id = $request->id;
        $item = HotelItem::findOrFail($id);
        return view('admin.hotel_options.items.show',compact('item'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        $hotel = Hotel::find($id);
        $categories = HotelCategory::where('hotel_id', $id)->where('status','=',1)->get();
        $item = HotelItem::findOrFail($request->id);
        return view('admin.hotel_options.items.edit',compact('item','hotel','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input= $request->all();
        $data = Arr::except($request->all(), ['_token','image']);
        if($request->file('image')){
            $filename = $request->file('image');
            $filename->getClientOriginalExtension();
            $filename_p = $filename->store('public/item_image');
            $filename_path = str_replace('public/item_image/', '', $filename_p);
            $input['image'] =  $filename_path;
        }
        $input['hotel_id'] = $request->get('hotel_id');
        $input['segments'] = json_encode($request->get('segments'));
        $item = HotelItem::findOrFail($id);
        $data = $item->update($input);
        if($data){
                 Session::flash('success', 'Successfully Updated!');
                 return redirect('admin/manage-hotel-item_index/'.$input['hotel_id']);
            }else{
                Session::flash('error', 'Something went wrong!');
                return redirect('admin/manage-hotel-item_index/'.$input['hotel_id']);
            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function status_update(Request $request,$id)
    {
        $item_status = HotelItem::findOrFail($id);

        if($request->status == "status"){
            if($item_status->status == 1){
                $status['status'] = 0;
                $item_status->update($status);
                return redirect()->back();
            }else if($item_status->status == 0 OR $item_status->status == NULL){
                $status['status'] = 1;
                $item_status->update($status);
                return redirect()->back();
            }
        }else if($request->status == "promote_item"){
            if($item_status->promote_item == "off" OR $item_status->promote_item == NULL){
                $status['promote_item'] = "on";
                $item_status->update($status);
                return redirect()->back();
            }else if($item_status->promote_item == "on"){
                $status['promote_item'] = "off";
                $item_status->update($status);
                return redirect()->back();
            }
        }
    }

    public function exportItems(Request $request){

        $data =  HotelItem::where('hotel_id','=',$request->hotel_id)->get();
		$hotel_name = Hotel::findOrFail($request->hotel_id);
		$filename = $hotel_name->slug.".csv";
		$handle = fopen($filename, 'w+');
		fputcsv($handle, ['category','name','description','hot','hotel_name','vegetarian','new','best_seller','promote','sort_order','calories','active','status','variants','price','uuid','image','thumbnail','tax','addon_price','price_old','segments']);
		foreach($data as $items){
			if($items->status == 1){
				$status ="Active";
			}else{
				$status = "In_Active";
			}
			fputcsv($handle, array($items->category->name,$items->name,$items->description,$items->spice_level,$items->hotel->name,$items->vegetarian,$items->new,$items->best_seller,$items->promote_item,
			$items->sort_order,$items->calories,$items->status,$status,$items->variants,$items->item_price,$items->sku_identifier,$items->image,$items->thumbnail,$items->tax,$items->addon_price,$items->item_oldprice,str_replace( array('[',']','"') , '',$items->segments)));

		}
		fclose($handle);
		$headers = array(
			'Content-Type' => 'text/csv',
		);
		return Response::download($filename,$filename,$headers);
    }

    
    // import items
    public function importItems(Request $request){
        $file = request()->file('file');
        $file = fopen($file,"r");
        $allCsv = [];
        $insertdata = [];
            while(! feof($file))
            {
                $data = fgetcsv($file);
                if($data != null){
                    $tempdata = array(
                        'category'=>$data[0],
                        'name'=>$data[1],
                        'description'=>$data[2],
                        'spice_level' => $data[3],
                        'vegetarian' => $data[4],
                        'new' => $data[5],
                        'best_seller' =>$data[6],
                        'promotion_status' => $data[7],
                        'sort_order' => $data[8],
                        'calories' => $data[9],
                        'low_fat' =>$data[10],
                        'status' =>$data[11],
                        'variants' => $data[12],
                        'item_price' => $data[13],
                        'sku_identifier' => $data[14],
                        'image' => $data[15],
                        'thumbnail_image' => $data[16],
                        'tax' => $data[17],
                        'addon_price'=>$data[18],
                        'item_oldprice' =>$data[19],
                        'segments' => explode(',', $data[20]));
                        array_push($allCsv, $tempdata);
                }
                else{
                Session::flash('error', 'Please check the csv file format!');
                    return redirect()->back();
                }
            }
            fclose($file);
            if(in_array('category',$allCsv[0]) and in_array('name',$allCsv[0])){
                    $delete_category = HotelCategory::whereIn('hotel_id',explode(",",$request->hotel_id))->delete();
                    if($delete_category){
                        $delete_hotel = HotelItem::whereIn('hotel_id',explode(",",$request->hotel_id))->delete();
                    }
                array_splice($allCsv,0,1);
                foreach ($allCsv as $csv) {
                        $check_catrgory = HotelCategory::where('name','=',$csv['category'])->where('hotel_id','=',$request->hotel_id)->first();
                        if($check_catrgory == null){
                            $category_create = new HotelCategory;
                            $categoryData['name'] = $csv['category'];
                            $categoryData['sort_order'] = 0;
                            $categoryData['hotel_id'] = $request->hotel_id;
                            $categoryData['status'] = 1;
                            $categoryData['primary_status'] = 1;
                            $category = $category_create->create($categoryData);
                        }else{
                            $category = HotelCategory::where('name','=',$csv['category'])->where('hotel_id','=',$request->hotel_id)->first();
                        }
                    
                        if($category){
                            $items = new HotelItem;
                                $itemsData['hotel_id'] = $request->hotel_id;
                                $itemsData['category_id'] = $category->id;
                                $itemsData['name'] = $csv['name'];
                                $itemsData['description'] = $csv['description'];
                                $itemsData['spice_level'] = $csv['spice_level'];
                                $itemsData['vegetarian'] = $csv['vegetarian'];
                                $itemsData['new'] = $csv['new'];
                                $itemsData['best_seller'] = $csv['best_seller'];
                                $itemsData['promotion_status'] = $csv['promotion_status'];
                                $itemsData['sort_order'] = $csv['sort_order'];
                                $itemsData['calories'] = $csv['calories'];
                                $itemsData['low_fat'] = $csv['low_fat'];
                                $itemsData['status'] = $csv['status'];
                                $itemsData['variants'] = $csv['variants'];
                                $itemsData['item_price'] = $csv['item_price'];
                                $itemsData['sku_identifier'] = $csv['sku_identifier'];
                                $itemsData['image'] = $csv['image'];
                                $itemsData['thumbnail_image'] = $csv['thumbnail_image'];
                                $itemsData['tax'] = $csv['tax'];
                                $itemsData['addon_price'] = $csv['addon_price'];
                                $itemsData['item_oldprice'] = $csv['item_oldprice'];
                                $itemsData['segments'] = json_encode($csv['segments']);
                                $items->create($itemsData);
                                array_push($insertdata,$csv);
                        }
                    }

                if(count($insertdata) > 0){
                    Session::flash('success', 'Successfully Items Added!');
                    return redirect()->back();
                }else{
                    Session::flash('error', 'Item Details are not Found ? Please check the csv file format!');
                    return redirect()->back();
                }
            }else{

                Session::flash('error', 'Please check the csv file format!');
                return redirect()->back();
            }

    }
}