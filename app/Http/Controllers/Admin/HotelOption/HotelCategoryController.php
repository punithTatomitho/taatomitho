<?php

namespace App\Http\Controllers\Admin\HotelOption;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\HotelCategory;
use App\Models\Hotel;
use Session;

class HotelCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request,$id)
    {   
        $hotel = Hotel::find($id);
        $categories = HotelCategory::where('hotel_id','=',$id)->get();
        return view('admin.hotel_options.category.index',compact('categories','hotel'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request,$id)
    {
        $hotel = Hotel::find($id);
        return view('admin.hotel_options.category.create',compact('hotel'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input= $request->all();
        $input['status'] = 1;
        $input['hotel_id'] = $request->get('hotel_id');
        $input['primary_status'] = 1;
        $data = HotelCategory::create($input);

        if($data){
                 Session::flash('success', 'Successfully Created!');
                 return redirect('admin/manage-hotel-category_index/'.$input['hotel_id']);
            }else{
                Session::flash('error', 'Something went wrong!');
                return redirect('admin/manage-hotel-category_index/'.$input['hotel_id']);
            }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        $id = $request->id;
        $category = HotelCategory::findOrFail($id);
        return view('admin.hotel_options.category.show',compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        $hotel = Hotel::find($id);
        $category = HotelCategory::findOrFail($request->id);
        return view('admin.hotel_options.category.edit',compact('category','hotel'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input= $request->all();
        $input['hotel_id'] = $request->get('hotel_id');
        $catgory = HotelCategory::findOrFail($id);
        $data = $catgory->update($input);
        if($data){
                 Session::flash('success', 'Successfully Updated!');
                 return redirect('admin/manage-hotel-category_index/'.$input['hotel_id']);
            }else{
                Session::flash('error', 'Something went wrong!');
                return redirect('admin/manage-hotel-category_index/'.$input['hotel_id']);
            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = HotelCategory::findOrFail($id)->delete();
        if($delete){
            Session::flash('success', 'Successfully deleted!');
            return redirect()->back();
        }else{
           Session::flash('error', 'Something went wrong!');
           return redirect()->back();
        }
    }

    public function status_update(Request $request,$id)
    {
        $category_status = HotelCategory::findOrFail($id);
        if($request->status == "status"){
            if($category_status->status == 1){
                $status['status'] = 0;
                $category_status->update($status);
                return redirect()->back();
            }else if($category_status->status == 0 OR $category_status->status == NULL){
                $status['status'] = 1;
                $category_status->update($status);
                return redirect()->back();
            }
        }else if($request->status == "primary_status"){
            if($category_status->primary_status == 1){
                $status['primary_status'] = 2;
                $category_status->update($status);
                return redirect()->back();
            }else if($category_status->primary_status == 2 OR $category_status->primary_status == NULL){
                $status['primary_status'] = 1;
                $category_status->update($status);
                return redirect()->back();
            }
        }
    } 
}
