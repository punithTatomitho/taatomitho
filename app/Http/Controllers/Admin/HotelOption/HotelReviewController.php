<?php

namespace App\Http\Controllers\Admin\HotelOption;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use App\Models\Hotel;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use App\Models\HotelReview;

class HotelReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request,$id)
    {
        $hotel = Hotel::findOrFail($id);
        $reviews = HotelReview::where('hotel_id','=',$id)->get();
        return view('admin.hotel_options.review.index',compact('reviews','hotel'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        $id = $request->id;
        $review = HotelReview::findOrFail($id);
        return view('admin.hotel_options.review.show',compact('review'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function status_update(Request $request,$id)
    {
        $review_status = HotelReview::findOrFail($id);
        if($request->status == "approved_status"){
            if($review_status->approved_status == 1){
                $status['approved_status'] = 0;
                $review_status->update($status);
                return redirect()->back();
            }else if($review_status->approved_status == 0 OR $review_status->approved_status == null){
                $status['approved_status'] = 1;
                $review_status->update($status);
                return redirect()->back();
            }
        }
    }
}
