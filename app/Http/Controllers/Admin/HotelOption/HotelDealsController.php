<?php

namespace App\Http\Controllers\Admin\HotelOption;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use App\Models\Item;
use App\Models\HotelCategory;
use App\Models\Hotel;
use App\Models\HotelDeal;
use App\Models\HotelDealsCategory;


class HotelDealsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $id)
    {
        $hotel = Hotel::findOrFail($id);
        $deals = HotelDeal::where('hotel_id','=',$id)->get();
        return view('admin.hotel_options.deals.index',compact('deals','hotel'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $id)
    {
        $hotel = Hotel::find($id);
        $categories = HotelCategory::all();
        return view('admin.hotel_options.deals.create',compact('hotel','categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $data = Arr::except($request->all(), ['_token','image']);
        if($request->file('image')){
            $filename = $request->file('image');
            $filename->getClientOriginalExtension();
            $filename_p = $filename->store('public/hotel_deals_image');
            $filename_path = str_replace('public/hotel_deals_image/', '', $filename_p);
            $data['image'] =  $filename_path;
         }
         $data['status'] = 1;
        $deals = HotelDeal::create($data);
        if($deals){
            foreach ($request->addmore as $key => $value) {
                $value['deal_id'] = $deals->id;
                $data = HotelDealsCategory::create($value);
            }
        }

        if($data){
            Session::flash('success', 'Successfully Created!');
            return redirect('admin/manage-hotel-deals_index/'.$input['hotel_id']);
       }else{
           Session::flash('error', 'Something went wrong!');
           return redirect('admin/manage-hotel-deals_index/'.$input['hotel_id']);
       }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = HotelDeal::findOrFail($id)->delete();
        if($delete){
            Session::flash('success', 'Successfully deleted!');
            return redirect()->back();
        }else{
           Session::flash('error', 'Something went wrong!');
           return redirect()->back();
        }
    }

    public function status_update(Request $request,$id)
    {
        $deal_status = HotelDeal::findOrFail($id);
        if($request->status == "status"){
            if($deal_status->status == 1){
                $status['status'] = 0;
                $deal_status->update($status);
                return redirect()->back();
            }else if($deal_status->status == 0 OR $deal_status->status == null){
                $status['status'] = 1;
                $deal_status->update($status);
                return redirect()->back();
            }
        }else if($request->status == "promotion_status"){
            if($deal_status->promotion_status == "on"){
                $status['promotion_status'] = "off";
                $deal_status->update($status);
                return redirect()->back();
            }else if($deal_status->promotion_status == "off" OR $deal_status->promotion_status == null){
                $status['promotion_status'] = "on";
                $deal_status->update($status);
                return redirect()->back();
            }
        }
    } 
}
