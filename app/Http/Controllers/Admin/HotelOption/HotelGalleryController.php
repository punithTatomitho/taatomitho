<?php

namespace App\Http\Controllers\Admin\HotelOption;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use App\Models\Hotel;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use App\Models\Gallery;

class HotelGalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request,$id)
    {
        $hotel = Hotel::findOrFail($id);
        $galleries = Gallery::where('hotel_id','=',$id)->get();
        return view('admin.hotel_options.gallery.index',compact('galleries','hotel'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request,$id)
    {
        $hotel = Hotel::find($id);
        return view('admin.hotel_options.gallery.create',compact('hotel'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->file('image')){
            $filename1 = $request->file('image');
            $filename1->getClientOriginalExtension();
            $filename_p1 = $filename1->store('public/hotel_gallery_image');
            $filename_path1 = str_replace('public/hotel_gallery_image/', '', $filename_p1);
            $data['image'] =  $filename_path1;
         }
         $data['hotel_id'] = $request->get('hotel_id');
         $data['sort_order'] = $request->get('sort_order');
         $data['status'] = 1;
         $create = Gallery::create($data);
         if($create){
            Session::flash('success', 'Successfully Created!');
            return redirect('admin/manage-hotel-gallery_index/'.$data['hotel_id']);
       }else{
           Session::flash('error', 'Something went wrong!');
           return redirect('admin/manage-hotel-gallery_index/'.$data['hotel_id']);
       }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Gallery::findOrFail($id)->delete();
        if($delete){
            Session::flash('success', 'Successfully deleted!');
            return redirect()->back();
        }else{
           Session::flash('error', 'Something went wrong!');
           return redirect()->back();
        }
    }

    public function status_update(Request $request,$id)
    {
        $gallery_status = Gallery::findOrFail($id);
        if($request->status == "status"){
            if($gallery_status->status == 1){
                $status['status'] = 0;
                $gallery_status->update($status);
                return redirect()->back();
            }else if($gallery_status->status == 0 OR $gallery_status->status == null){
                $status['status'] = 1;
                $gallery_status->update($status);
                return redirect()->back();
            }
        }
    } 
}
