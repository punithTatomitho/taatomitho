<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\User;
use App\Models\City;
use  App\Mail\RiderMail;
use Mail;
use Auth;
use Carbon\Carbon;
use DB;
use Session;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->get('city') != null && $request->get('start_date')){
            $data['city'] =   $request->get('city');
            $city =  $request->get('city');
            $data['city_name'] = $request->get('city');
            $data['start_date'] = $request->get('start_date');
            $data['end_date'] = $request->get('end_date');
        }else{
        $data['city'] = 'all';
        $data['city_name'] = 'all';
        $city  = City::pluck('name')->first();
        $date = Carbon::today();
        $data['start_date'] = $date->toDateString();
        $data['end_date'] = $date->toDateString();
        }
        $data['cities'] = City::distinct('name')->groupBy('name')->get();
        if($request->get('city') != null){
            if($request->get('city') == "all"){
                $data['total_recieved'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.delivery_status','=','recieved')->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->count();
                $data['total_accepted'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.delivery_status','=','accepted')->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->count();
                $data['total_read_to_delivery'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.delivery_status','=','ready_to_delivery')->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->count();
                $data['total_orders'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.order_status','=','confirmed')->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->count();
                $data['total_completed'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.delivery_status','=','completed')->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->count();
                $data['total_inprogress'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.delivery_status','=','inprogress')->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->count();
                $data['total_rejected'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.delivery_status','=','rejected')->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->count();
                $data['total_amount'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.delivery_status','=','completed')->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->sum('final_amount');
                $data['total_cash_amount'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.payment_mode','=',"Cash")->where('orders.delivery_status','=','completed')->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->sum('final_amount');
                $data['total_khalti_amount'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.payment_mode','=',"Online")->where('orders.delivery_status','=','completed')->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->sum('final_amount');
                $data['total_khalti'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.payment_mode','=',"Online")->where('orders.delivery_status','=','completed')->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->count();
                $data['total_cash'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.payment_mode','=',"Cash")->where('orders.delivery_status','=','completed')->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->count();

                $data['orders'] = DB::table('orders')->select('*', 'orders.name as customer_name','orders.updated_at as updated_at','orders.email as customer_email','orders.created_at as created_at')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.order_status','=','confirmed')->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->orderBy('orders.order_id', 'DESC')->get();
                
            }else{

                $data['total_orders'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.order_status','=','confirmed')->where('cities.slug','=',$city)->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->count();
                $data['total_completed'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.delivery_status','=','completed')->where('cities.slug','=',$city)->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->count();
                $data['total_recieved'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.delivery_status','=','recieved')->where('cities.slug','=',$city)->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->count();
                $data['total_accepted'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.delivery_status','=','accepted')->where('cities.slug','=',$city)->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->count();
                $data['total_read_to_delivery'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.delivery_status','=','ready_to_delivery')->where('cities.slug','=',$city)->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->count();
                $data['total_inprogress'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.delivery_status','=','inprogress')->where('cities.slug','=',$city)->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->count();
                $data['total_rejected'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.delivery_status','=','rejected')->where('cities.slug','=',$city)->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->count();
                $data['total_amount'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.delivery_status','=','completed')->where('cities.slug','=',$city)->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->sum('final_amount');
                $data['total_cash_amount'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.payment_mode','=',"Cash")->where('cities.slug','=',$city)->where('orders.delivery_status','=','completed')->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->sum('final_amount');
                $data['total_khalti_amount'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.payment_mode','=',"Online")->where('cities.slug','=',$city)->where('orders.delivery_status','=','completed')->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->sum('final_amount');
                $data['total_khalti'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.payment_mode','=',"Online")->where('cities.slug','=',$city)->where('orders.delivery_status','=','completed')->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->count();
                $data['total_cash'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.payment_mode','=',"Cash")->where('cities.slug','=',$city)->where('orders.delivery_status','=','completed')->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->count();

                $data['orders'] = DB::table('orders')->select('*', 'orders.name as customer_name','orders.updated_at as updated_at','orders.email as customer_email','orders.created_at as created_at')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('cities.slug','=',$city)->where('orders.order_status','=','confirmed')->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->orderBy('orders.order_id', 'DESC')->get();
                    
                }

        }else{
            $data['total_recieved'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.delivery_status','=','recieved')->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->count();
            $data['total_accepted'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.delivery_status','=','accepted')->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->count();
            $data['total_read_to_delivery'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.delivery_status','=','ready_to_delivery')->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->count();
            $data['total_orders'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.order_status','=','confirmed')->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->count();
            $data['total_completed'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.delivery_status','=','completed')->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->count();
            $data['total_inprogress'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.delivery_status','=','inprogress')->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->count();
            $data['total_rejected'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.delivery_status','=','rejected')->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->count();
            $data['total_amount'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.delivery_status','=','completed')->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->sum('final_amount');
            $data['total_cash_amount'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.payment_mode','=',"Cash")->where('orders.delivery_status','=','completed')->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->sum('final_amount');
            $data['total_khalti_amount'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.payment_mode','=',"Online")->where('orders.delivery_status','=','completed')->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->sum('final_amount');
            $data['total_khalti'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.payment_mode','=',"Online")->where('orders.delivery_status','=','completed')->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->count();
            $data['total_cash'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.payment_mode','=',"Cash")->where('orders.delivery_status','=','completed')->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->count();
            if(Auth::user()->roles[0]['name'] == 'rider'){
                $data['orders'] = DB::table('orders')->select('*', 'orders.name as customer_name','orders.updated_at as updated_at','orders.email as customer_email','orders.created_at as created_at')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.order_status','=','confirmed')->where('rider_id','=',Auth::user()->id)->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->orderBy('orders.order_id', 'DESC')->get();
            }else{
                $data['orders'] = DB::table('orders')->select('*', 'orders.name as customer_name','orders.updated_at as updated_at','orders.email as customer_email','orders.created_at as created_at')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.order_status','=','confirmed')->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->orderBy('orders.order_id', 'DESC')->get();
            }

        }
        $data['riders'] = User::whereHas(
            'roles', function($q){
                $q->where('name', 'rider');
            }
        )->get();

         return view('admin.orders.index',$data);
    }
    
    
    public function cityWiseOrders(Request $request){
        if($request->city) {
            $data['city'] = $request->city;
        } else {
            $data['city'] = 'all';
        }

        $city = $request->city;
        $data['city_name'] = $request->city;
        $data['start_date'] = date('Y-m-d', strtotime($request->start_date));
        $data['end_date'] = date('Y-m-d', strtotime($request->end_date));        
        $data['cities'] = City::distinct('name')->groupBy('name')->get();
        if($request->city == "all"){
            $data['total_recieved'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.delivery_status','=','recieved')->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->count();
            $data['total_accepted'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.delivery_status','=','accepted')->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->count();
            $data['total_read_to_delivery'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.delivery_status','=','ready_to_delivery')->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->count();
            $data['total_orders'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.order_status','=','confirmed')->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->count();
            $data['total_completed'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.delivery_status','=','completed')->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->count();
            $data['total_inprogress'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.delivery_status','=','inprogress')->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->count();
            $data['total_rejected'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.delivery_status','=','rejected')->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->count();
            $data['total_amount'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.delivery_status','=','completed')->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->sum('final_amount');
            $data['total_cash_amount'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.payment_mode','=',"Cash")->where('orders.delivery_status','=','completed')->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->sum('final_amount');
            $data['total_khalti_amount'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.payment_mode','=',"Online")->where('orders.delivery_status','=','completed')->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->sum('final_amount');
            $data['total_khalti'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.payment_mode','=',"Online")->where('orders.delivery_status','=','completed')->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->count();
            $data['total_cash'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.payment_mode','=',"Cash")->where('orders.delivery_status','=','completed')->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->count();
        
            $data['orders'] = DB::table('orders')->select('*', 'orders.name as customer_name','orders.updated_at as updated_at','orders.email as customer_email','orders.created_at as created_at')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.order_status','=','confirmed')->where('orders.delivery_status','=',$request->page)->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->orderBy('orders.order_id', 'DESC')->get();
            
        }else{
            $data['total_recieved'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.delivery_status','=','recieved')->where('cities.slug','=',$city)->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->count();
            $data['total_accepted'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.delivery_status','=','accepted')->where('cities.slug','=',$city)->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->count();
            $data['total_read_to_delivery'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.delivery_status','=','ready_to_delivery')->where('cities.slug','=',$city)->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->count();
            $data['total_orders'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.order_status','=','confirmed')->where('cities.slug','=',$city)->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->count();
            $data['total_completed'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.delivery_status','=','completed')->where('cities.slug','=',$city)->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->count();
            $data['total_inprogress'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.delivery_status','=','inprogress')->where('cities.slug','=',$city)->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->count();
            $data['total_rejected'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.delivery_status','=','rejected')->where('cities.slug','=',$city)->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->count();
            $data['total_amount'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.delivery_status','=','completed')->where('cities.slug','=',$city)->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->sum('final_amount');
            $data['total_cash_amount'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.payment_mode','=',"Cash")->where('cities.slug','=',$city)->where('orders.delivery_status','=','completed')->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->sum('final_amount');
            $data['total_khalti_amount'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.payment_mode','=',"Online")->where('cities.slug','=',$city)->where('orders.delivery_status','=','completed')->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->sum('final_amount');
            $data['total_khalti'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.payment_mode','=',"Online")->where('cities.slug','=',$city)->where('orders.delivery_status','=','completed')->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->count();
            $data['total_cash'] = DB::table('orders')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.payment_mode','=',"Cash")->where('cities.slug','=',$city)->where('orders.delivery_status','=','completed')->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->count();
            
            $data['orders'] = DB::table('orders')->select('*', 'orders.name as customer_name','orders.updated_at as updated_at','orders.email as customer_email','orders.created_at as created_at')->join('hotels','hotels.id','=','orders.hotel_id')->join('cities','cities.id','=','hotels.city_id')->where('orders.order_status','=','confirmed')->where('orders.delivery_status','=',$request->page)->where('cities.slug','=',$city)->whereDate('orders.created_at', '>=', $data['start_date'])->whereDate('orders.created_at', '<=', $data['end_date'])->orderBy('orders.order_id', 'DESC')->get();
            
        }
        $data['riders'] = User::whereHas(
            'roles', function($q){
                $q->where('name', 'rider');
            }
        )->get();

        return view('admin.orders.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        $order  = Order::findOrFail($id);
        return view('admin.orders.view',compact('order'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function deliveyStatus(Request $request){
        $deliver_status =  Order::findOrFail($request->order_id);
        $deliver_status->delivery_status = $request->deliver_status;
        $deliver_status->save();
        return response()->json(['data'=>$deliver_status]);
        
    }

    public function selectRider(Request $request){
        $user = User::findOrFail($request->rider_id);
        $rider_details =  Order::findOrFail($request->order_id);
        $rider_details->csr_id = Auth::user()->id;
        $rider_details->csr_name = Auth::user()->name;
        $rider_details->rider_name = $user->name;
        $rider_details->rider_id = $request->rider_id;
        $rider_details->save();
        $validate_email = filter_var($user->email,FILTER_VALIDATE_EMAIL);
        $mail = Mail::to($validate_email)->send(new RiderMail($rider_details));
        return redirect()->back();
    }

    public function csrComments(Request $request,$id){
        $orders = Order::findOrFail($id);
        $orders->csr_comments = $request->get('csr_comments');
        $orders->save();
        Session::flash('success', 'Successfully Sent!');
        return redirect()->back();
    }
}
