<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\City;
use Session;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cities = City::all();
        return view('admin.city.index',compact('cities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $city = City::findOrFail($id);
        return view('admin.city.show',compact('city'));   
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $city = City::findOrFail($id);
        return view('admin.city.edit',compact('city'));   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $city = City::findOrFail($id);
        if($request->file('image')){
            $filename = $request->file('image');
            $filename->getClientOriginalExtension();
            $filename_p = $filename->store('public/city_image');
            $filename_path = str_replace('public/city_image/', '', $filename_p);
            $city->image =  $filename_path;
         }
        $city->name = $request->get('name');
        $city->sort_order = $request->get('sort_order');
        $city->meta_tag = $request->get('meta_tag');
        $city->save();
        if($city) {
            Session::flash('success', 'Successfully updated!');
            return redirect('admin/manage-city');
         } else {
            Session::flash('error', 'Something went wrong!');
            return redirect('admin/manage-city');
         }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = City::findOrFail($id)->delete();
        if($delete){
            Session::flash('success', 'Successfully deleted!');
            return redirect()->back();
        }else{
           Session::flash('error', 'Something went wrong!');
           return redirect()->back();
        }
    }

    public function status_update(Request $request,$id)
    {
        $city_status = City::findOrFail($id);
        if($request->status == "status"){
            if($city_status->status == 1){
                $status['status'] = 0;
                $city_status->update($status);
                return redirect()->back();
            }else if($city_status->status == 0 OR $city_status->status == null){
                $status['status'] = 1;
                $city_status->update($status);
                return redirect()->back();
            }
        }else if($request->status == "papular_status"){
            if($city_status->papular_status == 1){
                $status['papular_status'] = 0;
                $city_status->update($status);
                return redirect()->back();
            }else if($city_status->papular_status == 0 OR $city_status->papular_status == null){
                $status['papular_status'] = 1;
                $city_status->update($status);
                return redirect()->back();
            }
        }
        
    }   
}
