<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use Exception;
use App\Models\User;
use App\Models\Role;
use App\Models\User_profile;
use Illuminate\Support\Facades\Auth;

class FacebookController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function redirectToFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }
          
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function handleFacebookCallback()
    {
        try {
        
            $user = Socialite::driver('facebook')->stateless()->user();
         
            $finduser = User::where('facebook_id', $user->id)->first();

            // Check Users Email If Already There
            $is_user = User::where('email', $user->email)->first();
            if(!$is_user){
                if($finduser){
            
                    Auth::login($finduser);
            
                    return redirect()->route('home');
            
                }else{
                    $newUser = User::create([
                        'name' => $user->name,
                        'email' => $user->email,
                        'facebook_id'=> $user->id,
                        'password' => encrypt('123456dummy')
                    ]);
                    User_profile::create([
                        'user_id' =>$newUser->id,
                        'user_name' => $newUser->name,
                    ]);
                    $userRole = Role::whereName('user')->first();
                    $newUser->attachRole($userRole);
            
                    Auth::login($newUser);
            
                    return redirect()->route('home');
                }
            }else{
                $saveUser = User::where('email',  $user->email)->update([
                    'facebook_id' => $user->id,
                ]);
                $saveUser = User::where('email', $user->email)->first();
                Auth::login($saveUser);
                return redirect()->route('home');
            }
        
        } catch (Exception $e) {
            // dd($e->getMessage());
            return redirect()->route('home');
        }
    }
}
