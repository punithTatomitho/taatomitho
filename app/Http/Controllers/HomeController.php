<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\User;
use App\Models\User_profile;
use App\Models\Order;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application console.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(Auth::user()->roles[0]['name'] != 'user') {
            return view('admin.index');
        } else {
            return redirect('/');
        }
        
    }

    public function profileDetails()
    {
        if(Auth::user()) {
            $data['user'] = User::with('user_details')->where('id', Auth::user()->id)->first();
            $data['orders'] = Order::with('hotel')->where('email', Auth::user()->email)
                                    ->orderBy('order_id', 'DESC')
                                    ->get();
            return view('my-orders', $data);
        } else {
            return redirect('/');
        }
    }

    public function profileUpdate(Request $request)
    {
        if($request->file('profile_image')){
            $filename = $request->file('profile_image');
            $filename->getClientOriginalExtension();
            $filename_p = $filename->store('public/profile_image');
            $filename_path = str_replace('public/profile_image/', '', $filename_p);
            $data = User_profile::findOrFail(Auth::user()->user_details->user_prifile_id);
            $data->profile_image =  $filename_path;
            $data->save();
            return redirect()->back()->with('upload-success', 'Profile image uploaded successfully');
        } else {
            $data = User_profile::findOrFail(Auth::user()->user_details->user_prifile_id);
            $data->user_name =  $request->user_name;
            $data->phone_number =  $request->phone_number;
            $data->user_address =  $request->user_address;
            $data->zip_code =  $request->zip_code;
            $data->save();
            return redirect()->back()->with('field-success', 'Profile updated successfully');
        }
    }

    public function userUpdate(Request $request)
    {
        $data = User::findOrFail(Auth::user()->id);
            $data->email =  $request->email;
            $data->password = Hash::make($request->password);
            $data->save();
            return redirect()->back()->with('user-success', 'User updated successfully');
    }
}
