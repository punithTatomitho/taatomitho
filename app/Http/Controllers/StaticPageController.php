<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StaticPageController extends Controller
{
    /**
     * Show the application about us page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function aboutUs()
    {
        return view('about-us');
    }

    /**
     * Show the application Careers page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function Careers()
    {
        return view('careers');
    }

    /**
     * Show the application Privacy page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function Privacy()
    {
        return view('privacy');
    }

    /**
     * Show the application Terms and condition page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function TermsCondition()
    {
        return view('terms-and-conditions');
    }

    /**
     * Show the application Contact us page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function ContactUs()
    {
        return view('contact-us');
    }

    /**
     * Show the application Business page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function Business()
    {
        return view('business');
    }

    /**
     * Show the application Advertise page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function Advertise()
    {
        return view('advertise');
    }

    /**
     * Show the application faqs page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function Faqs()
    {
        return view('faqs');
    }

    /**
     * Show the application share page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function Share()
    {
        return view('share');
    }
}
