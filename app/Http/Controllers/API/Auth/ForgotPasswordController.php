<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password; 
use App\Models\User;

class ForgotPasswordController extends Controller
{
    public function forgot() {
        
        $credentials = request()->validate(['email' => 'required|email']);
        $email_check = User::where('email','=',$credentials)->exists();
        if($email_check == true){
            $password = Password::sendResetLink($credentials);
            if($password){

                return response()->json(["message" => 'Reset password link sent on your email id.']);
            }else{

                return response()->json(["message" => 'Error','status'=> 401]);
            }
        }else{
            
            return response()->json(["message" => "We can't find a user with that email address."],400);
        }
        

       
    }

    public function reset() {
        $credentials = request()->validate([
            'email' => 'required|email',
            'token' => 'required|string',
            'password' => 'required|string|confirmed'
        ]);

        $reset_password_status = Password::reset($credentials, function ($user, $password) {
            $user->password = bcrypt($password);
            $user->save();
        });

        if ($reset_password_status == Password::INVALID_TOKEN) {
            return response()->json(["msg" => "Invalid token provided"], 400);
        }

        return response()->json(["msg" => "Password has been successfully changed"]);
    }
}
