<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class ProfileEditController extends Controller
{
    public function userUpdate(Request $request)
    {
        $data = User::findOrFail(Auth::user()->id);
        $data->email =  $request->email;
        $data->password = Hash::make($request->password);
        $data->save();
        if($request->file('profile_image')){
            $filename = $request->file('profile_image');
            $filename->getClientOriginalExtension();
            $filename_p = $filename->store('public/profile_image');
            $filename_path = str_replace('public/profile_image/', '', $filename_p);
            $data = User_profile::findOrFail(Auth::user()->user_details->user_prifile_id);
            $data->profile_image =  $filename_path;
            $data->save();
        } else {
            $data = User_profile::findOrFail(Auth::user()->user_details->user_prifile_id);
            $data->user_name =  $request->user_name;
            $data->phone_number =  $request->phone_number;
            $data->user_address =  $request->user_address;
            $data->zip_code =  $request->zip_code;
            $data->save();
        }
            return response()->json(['success', 'User updated successfully','status'=>1]);
    }
}
