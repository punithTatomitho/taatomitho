<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Role;
use App\Models\User_profile;
use Illuminate\Support\Facades\Auth;
use Validator;

class RegisterController extends Controller
{
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);
   
        if($validator->fails()){
            return response()->json(['message'=>$validator->errors()],201);  
        }
   
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $token =  $user->createToken('Taatomitho')->accessToken;
        $name =  $user->name;
        $user_profile = User_profile::create([
            'user_id' => $user->id,
            'user_name' => $user->name,
        ]);
        $userRole = Role::whereName('user')->first();
        $user->attachRole($userRole);
        if($user_profile){
            return response()->json(['message'=>'User register successfully.','status'=>1, 'token'=>$token, 'name'=>$name],200);
        }else{
            return response()->json([
	            'message' => 'Bad Request',
	            ], 400);
        }
       
    }
   
    
}
