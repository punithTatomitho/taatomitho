<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /**
     * Login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){ 
            $user = Auth::user(); 
            $success['token'] =  $user->createToken('Taatomitho')-> accessToken;
            $success['users'] =  $user;
            return response()->json(['message'=>'User login successfully.','status'=>1,'data'=>$success],200);
        } 
        else{ 
            return response()->json(['message'=>'Unauthorised.','status'=>0],201);
        }
    }
}
