<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\HotelItem;
use App\Models\HotelTiming;
use App\Models\HotelCategory;
use App\Models\Hotel;
use App\Models\Order;
use Carbon\Carbon;
use Mail;
use  App\Mail\OrderBookingMail;
use  App\Mail\AdminOrderMail;
use App\Models\User;
use Carbon\CarbonPeriod;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public function orderStore(Request $request){
        $order = new Order();
        $order->name = $request->name;
        $order->email = $request->email;
        $order->phone_number = $request->phone;
        $order->delivery_address = $request->delivery_address;
        $order->delivery_note = $request->delivery_note;
        $order->delivery_time = $request->deliveryTime;
        $order->order_date = $request->order_date;
        $order->zip_code = $request->zip_code;
        $order->tag = $request->tag;
        $order->item_details = $request->item_details;
        // need price
        $total = $request->price;
    
        // restuarant slug required
        $slug = $request->hotel_slug;
        $hotel = Hotel::where('slug', $slug)->first();
        if($hotel->service_charge != null) {
            if(is_numeric($hotel->service_charge)) {
                $serviceCharge = ($hotel->service_charge * $total) / 100;
            }
        }
        $totalAmount = $total + $serviceCharge;

        if ($totalAmount < 500){
            return response()->json(['message' =>'failed','data' => $totalAmount,'status' => 0]);
        }

        $order->hotel_name = $hotel->name;
        $order->hotel_id = $hotel->id;
        $order->final_amount = $totalAmount;
        // $order->discount_price = $hotel->discount_price;
        $order->save();
        // Do clear cart session
        // $data['orderId'] = encrypt($order->order_id);
        $data['order'] = $order;
        if($order){
            return response()->json(['message'=>'success','data' => $data,'status'=>1],200);
        }else{
            return response()->json(['message' =>'Sothming is wrong','status'=>0],201);
        }
        
    }


    public function paymentSuccess(Request $request)
    {
        $superadmiEmails = User::whereHas(
            'roles', function($q){
                $q->where('name', 'superadmin');
            }
        )->get();
        $order = Order::findOrFail($request->order_id);
        if ($request->order_type == 'cash') {
            $order->payment_mode = 'Cash';
            $order->order_status = 'confirmed';
            $order->save();
            $validate_email = filter_var($order->email,FILTER_VALIDATE_EMAIL);
            $toCustomer = Mail::to($validate_email)->send(new OrderBookingMail($order));
            foreach($superadmiEmails as $adMail){
                $adminMail = filter_var($adMail->email,FILTER_VALIDATE_EMAIL);
                $toAdmin = Mail::to($adminMail)->send(new AdminOrderMail($order));
            }
            if($toAdmin){
                return response()->json(['message' =>'Successfully Created','status'=>1],200);
            } else {
                return response()->json(['message' =>'Somethiming is wrong !.','status'=>0],201);
            }
        }

        if ($request->order_type == 'khalti_online') {
            $args = http_build_query(array(
                        'token' => $request->token,
                        'amount'  => ($request->amount)
                    ));

            $url = "https://khalti.com/api/payment/verify/";

            # Make the call using API.
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS,$args);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            $headers = ['Authorization: Key '.env('KHALTI_SECRET_KEY', '')];
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            // Response
            $response = curl_exec($ch);
            $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);
            $token = json_decode($response, TRUE);
            
            if (isset($token['idx'])&& $status_code == 200) 
            {
                $order->payment_mode = 'Online';
                $order->payment_id = $request->paymentId;
                $order->payment_status = 'success';
                $order->order_status = 'confirmed';
                $order->save();
                $validate_email = filter_var($order->email,FILTER_VALIDATE_EMAIL);
                $toCustomer = Mail::to($validate_email)->send(new OrderBookingMail($order));
                foreach($superadmiEmails as $adMail){
                    $adminMail = filter_var($adMail->email,FILTER_VALIDATE_EMAIL);
                    $toAdmin = Mail::to($adminMail)->send(new AdminOrderMail($order));
                }
                if($toAdmin) {
                    return response()->json(['message' =>'Successfully Payed'],200);
                } else {
                    return response()->json(['message' =>'Something is Wrong'],201);
                }
            } else {
                return response()->json(['message' =>'Payment verification failed, Please try again.'],201);
            }
        }
    }
}
