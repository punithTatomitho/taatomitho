<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Hotel;
use App\Models\Cuisine;
use App\Models\CuisineType;
use App\Models\HotelCategory;
use App\Models\HotelItem;
use App\Models\City;
use App\Models\Gallery;
use App\Models\Slider;
use App\Models\HotelShifTiming;
use App\Models\HotelRegisterDetail;

class RestaurantController extends Controller
{
    public function homePageData()
    {
        $data['cuisines'] = Cuisine::get();
        $data['cities'] = City::where('papular_status', 1)->groupBy('name')->get();
        $data['special_deals'] = Slider::where('slider_status', 1)->where('promotion_status', '!=', 'no')->orderBy('sort_order', 'ASC')->get();
        if($data['special_deals']){
        return response()->json(['message'=>'success','data'=>$data, 'thumbnail_image_path' => env('APP_URL').'/storage/hotel_thumbnail_image/', 'banner_image_path' => env('APP_URL').'/storage/hotel_banner_image/', 'item_image_path' => env('APP_URL').'/storage/item_image/', 'gallery_image_path' => env('APP_URL').'/storage/hotel_gallery_image/', 'cuisine_image_path' => env('APP_URL').'/storage/cuisine_image/', 'city_image_path' => env('APP_URL').'/storage/city_image/', 'profile_image_path' => env('APP_URL').'/storage/profile_image/', 'slider_image_path' => env('APP_URL').'/storage/slider_bg_image/','status'=>1],200);
        }else{
            return response()->json(['message'=>'Something is wrong','status'=>0],201);
        }
    }

    public function getAllRestaurants(Request $request)
    {
        $data['restaurants'] = Hotel::where('list_status', '=', 1)
                        ->orderBy('promotion_status', 'DESC')
                        ->orderBy('offers_status', 'DESC')
                        ->orderBy('sort_order', 'ASC')
                        ->paginate(50);
        $cuisineType = CuisineType::where('type', 'Food')->first();
        $data['cuisines'] = Cuisine::where('cuisinetype_id', $cuisineType->id)->paginate(25);
        $data['locations'] = City::select('name')->groupBy('name')->get();
        if($data['locations']){
        return response()->json(['message'=>'success','data'=>$data, 'thumbnail_image_path' => env('APP_URL').'/storage/hotel_thumbnail_image/', 'banner_image_path' => env('APP_URL').'/storage/hotel_banner_image/', 'item_image_path' => env('APP_URL').'/storage/item_image/', 'gallery_image_path' => env('APP_URL').'/storage/hotel_gallery_image/', 'cuisine_image_path' => env('APP_URL').'/storage/cuisine_image/', 'city_image_path' => env('APP_URL').'/storage/city_image/', 'profile_image_path' => env('APP_URL').'/storage/profile_image/', 'slider_image_path' => env('APP_URL').'/storage/slider_bg_image/','status'=>1],200);
        
        }else{
            return response()->json(['message'=>'Something is wrong','status'=>0],201);
        }
    }

    // hotel details
    public function restaurantDetails($slug)
    {
         $hotel = Hotel::with('timings')->where('slug', $slug)->first();
        if($hotel){
            $categories = HotelCategory::where('hotel_id', $hotel->id)->where('status', 1)->orderBy('sort_order', 'DESC')->get();
            $data['hotel_items'] = $categories->map(function($category) use($hotel) {
                $items = HotelItem::where('hotel_id', $hotel->id)->where('category_id',$category->id)->where('status', 1)->get();
                $category->items = $items;
                return $category;
            });
        $data['galleries'] = Gallery::where('hotel_id', $hotel->id)->get();
        $data['hotel_timings'] = HotelShifTiming::where('hotel_id', $hotel->id)->get();
        $data['hotel_registerdetails'] = HotelRegisterDetail::where('hotel_id', $hotel->id)->get();
        $data['hotel'] = $hotel;
        if($data['hotel']){
            return response()->json(['message'=>'success','data'=>$data, 'thumbnail_image_path' => env('APP_URL').'/storage/hotel_thumbnail_image/', 'banner_image_path' => env('APP_URL').'/storage/hotel_banner_image/', 'item_image_path' => env('APP_URL').'/storage/item_image/', 'gallery_image_path' => env('APP_URL').'/storage/hotel_gallery_image/', 'cuisine_image_path' => env('APP_URL').'/storage/cuisine_image/', 'city_image_path' => env('APP_URL').'/storage/city_image/', 'profile_image_path' => env('APP_URL').'/storage/profile_image/', 'slider_image_path' => env('APP_URL').'/storage/slider_bg_image/','status'=>1],200);
            
            }else{
                return response()->json(['message'=>'Something is wrong','status'=>0],201);
            }
        }else{
            return response()->json(['message'=>'Something is wrong','status'=>0],201);
        }
    }

    public function offers()
    {
        $offers = Slider::where('slider_status', 1)->where('promotion_status', '!=', 'no')->orderBy('sort_order', 'DESC')->get();
        if($offers){
            return response()->json(['message'=>'Success','data'=>$offers, 'thumbnail_image_path' => env('APP_URL').'/storage/hotel_thumbnail_image/', 'banner_image_path' => env('APP_URL').'/storage/hotel_banner_image/','status'=>1],200);
        }else{
            return response()->json(['message'=>'Something is wrong','status'=>0],201);
        }
        
    }

    // search restaurant
    public function searchRestaurants($query)
    {
        $restaurants = Hotel::where('name', 'like', '%'.$query.'%')
                            ->orWhere('slug', 'like', '%'.$query.'%')
                            ->where('cuisines', 'like',  '%' . $query . '%')
                            ->paginate(25);
        if($restaurants){
            return response()->json(['message'=>'success','data'=>$restaurants,'status'=>1],200);
        }else{
            return response()->json(['message'=>'Something is wrong','status'=>0],201);
        }
        
    }

    // search items
    public function searchItem($query)
    {
        $item = HotelItem::where('name', 'like', '%'.$query.'%')->paginate(25);
        if($item){
            return response()->json(['message'=>'success','data'=>$item,'status'=>1],200);
        }else{
            return response()->json(['message'=>'Something is wrong','status'=>0],201);
        }
    }

    // get by city
    public function getHotelBycity($city)
    {
        $data = Hotel::where('city', $city)
                        ->where('list_status', '=', 1)
                        ->orderBy('promotion_status', 'DESC')
                        ->orderBy('offers_status', 'DESC')
                        ->orderBy('sort_order', 'ASC')
                        ->paginate(50);
        if($data){
            return response()->json(['message'=>'success','data'=>$data,'status'=>1],200);
        }else{
            return response()->json(['message'=>'Something is wrong','status'=>0],201);
        }
        
    }
}
