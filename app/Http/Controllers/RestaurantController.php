<?php

namespace App\Http\Controllers;

// use Request;
use Illuminate\Http\Request;
use App\Models\Hotel;
use App\Models\Cuisine;
use App\Models\CuisineType;
use App\Models\HotelCategory;
use App\Models\HotelItem;
use App\Models\City;
use App\Models\Gallery;
use App\Models\Slider;

class RestaurantController extends Controller
{
    public function index()
    {
        $data['cuisines'] = Cuisine::get();
        $data['cities'] = City::where('papular_status', 1)->groupBy('name')->get();
        $data['special_deals'] = Slider::where('slider_status', 1)->where('promotion_status', '!=', 'no')->orderBy('sort_order', 'ASC')->get();
        return view('welcome', $data);
    }

    public function restaurants(Request $request)
    {
        if($request->is('restaurants')) {
            $where = '';
            $withPath = '/restaurants';
        }
        if($request->is('groceries')) {
            $where = '';
            $withPath = '/groceries';
        }
        $query = $request->get('query');
        $all = $request->get('all');

        if (isset($query)) {
            if(isset($all)) {
                $hotelsData = Hotel::with('hotel_registerdetails')
                                // ->whereJsonContains('cuisines', ucfirst(strtolower($query)))
                                // ->where('cuisines', 'like',  '%' . ucfirst(strtolower($query)) . '%')
                                ->where('cuisines', 'like',  '%' . $query . '%')
                                ->orWhere('name', 'like',  '%' . $query .'%')
                                ->orWhere('city', 'like',  '%' . $query .'%')
                                ->where('list_status', '=', 1)
                                ->orderBy('promotion_status', 'DESC')
                                ->orderBy('offers_status', 'DESC')
                                ->orderBy('sort_order', 'ASC')
                                ->paginate(50);
                $hotelsData->withPath($withPath.'?query='.$query);
            } else {
                $hotelsData = Hotel::with('hotel_registerdetails')
                                    // ->whereJsonContains('cuisines', ucfirst(strtolower($query)))
                                    // ->where('cuisines', 'like',  '%' . ucfirst(strtolower($query)) . '%')
                                    ->where('cuisines', 'like',  '%' . $query . '%')
                                    ->orWhere('name', 'like',  '%' . $query .'%')
                                    ->orWhere('city', 'like',  '%' . $query .'%')
                                    ->where('list_status', '=', 1)
                                    ->where('hotel_status', 1)
                                    ->orderBy('promotion_status', 'DESC')
                                    ->orderBy('offers_status', 'DESC')
                                    ->orderBy('sort_order', 'ASC')
                                    ->paginate(50);
                $hotelsData->withPath($withPath.'?query='.$query);
            }
        } else {
            if(isset($all)) {
                $hotelsData = Hotel::with('hotel_registerdetails')
                                    ->where('list_status', '=', 1)
                                    ->orderBy('promotion_status', 'DESC')
                                    ->orderBy('offers_status', 'DESC')
                                    ->orderBy('sort_order', 'ASC')
                                    ->paginate(50);
                $hotelsData->withPath($withPath);
            } else {
                $hotelsData = Hotel::with('hotel_registerdetails')
                                    ->where('list_status', '=', 1)
                                    ->where('hotel_status', 1)
                                    ->orderBy('promotion_status', 'DESC')
                                    ->orderBy('offers_status', 'DESC')
                                    ->orderBy('sort_order', 'ASC')
                                    ->paginate(50);
                $hotelsData->withPath($withPath);
            }
        }
        $cuisineType = CuisineType::where('type', 'Food')->first();
        $data['cuisines'] = Cuisine::where('cuisinetype_id', $cuisineType->id)->get();
        $data['locations'] = City::select('name')->groupBy('name')->get();
        $data['hotels'] = $hotelsData;
        return view('restaurants', $data);
    }

    public function restaurantDetails($slug)
    {
        $hotel = Hotel::with('timings')->where('slug', $slug)->first();
        $categories = HotelCategory::where('hotel_id', $hotel->id)->where('status', 1)->orderBy('sort_order', 'DESC')->get();
        $data['hotel_items'] = $categories->map(function($category) use($hotel) {
                $items = HotelItem::where('hotel_id', $hotel->id)->where('category_id',$category->id)->where('status', 1)->get();
                $category->items = $items;
                return $category;
            });
        $data['galleries'] = Gallery::where('hotel_id', $hotel->id)->get();
        $data['hotel'] = $hotel;
        return view('restaurant-details', $data);
    }

    public function offers()
    {
        $data['offers'] = Slider::where('slider_status', 1)->where('promotion_status', '!=', 'no')->orderBy('sort_order', 'DESC')->get();
        return view('offers', $data);
    }
}
