<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Auth;
use Illuminate\Support\Facades\Session;
use App\Models\HotelItem;
use App\Models\HotelTiming;
use App\Models\HotelCategory;
use App\Models\Hotel;
use App\Models\Order;
use Carbon\Carbon;
use Mail;
use  App\Mail\OrderBookingMail;
use  App\Mail\AdminOrderMail;
use App\Models\User;
use Carbon\CarbonPeriod;

class OrderController extends Controller
{
    public function addToCart(Request $request, $id)
    {
        $item = HotelItem::findOrFail($id);
        $category = HotelCategory::findOrFail($item->category_id);
        $hotel = Hotel::where('id', $item->hotel_id)->first();

        $previosCart = session()->get('cart');
        if($previosCart != null) {
            $prevCartHotel = null;
            foreach($previosCart as $details) {
                $prevCartHotel = $details['cart_restaurant'];
            }
            if($prevCartHotel->slug != $hotel->slug) {
                $request->session()->forget('cart');
            }
        }

        $cart = session()->get('cart', []);

        if(isset($cart[$id])) {
            $cart[$id]['quantity']++;
        } else {
            $cart[$id] = [
                "name" => $item->name,
                'category' => $category->name,
                "quantity" => 1,
                "price" => $item->item_price
            ];

            $cart[$id]['cart_restaurant'] = $hotel;
        }
          
        session()->put('cart', $cart);
        return response()->json(['data' =>$cart, 'status' => 1]);
    }

    public function updateCart(Request $request)
    {
        $cart = session()->get('cart');
        if(isset($request->id)) {
            if ($request->quantity == 0) {
                unset($cart[$request->id]);
                if(count($cart) > 0){
                    Session::put('cart',$cart);
                } else {
                    Session::forget('cart');
                }
            } else {
                $cart[$request->id]["quantity"] = $request->quantity;
            }
            session()->put('cart', $cart);
            return response()->json(['msg' =>'success', 'status' => 1]);
        }
    }

    public function clearAllCart(Request $request)
    {
        $request->session()->forget('cart');
        return response()->json(['msg' =>'success', 'status' => 1]);
    }

    public function checkout()
    {
        $carts = session()->get('cart');
        if ($carts) {
            $slug = '';
            foreach($carts as $id => $details) {
                $slug = $details['cart_restaurant'];
            }

            $hotel = Hotel::where('slug', $slug->slug)->first();
            $hotelTime = HotelTiming::where('hotel_id', $hotel->id)->first();
        } else {
            $hotel = null;
        }

        if($hotel->accept_deliveries == null) {
            $addMinute = 60;
        } else {
            $addMinute = $hotel->accept_deliveries;
        }

        $today = Carbon::now();
        // $today->hour = 20;
        // $today->minute = 00;
        // $today->second = 00;

        if ($today->toTimeString() <= '09:00:00') {
            $startTime = '09:00';
        } else {
            $startTime = $today->addMinute($addMinute, $today->toTimeString()); // for adding minutes for current time
        }

        if($hotelTime != null) {
            $tomorrow =  today()->addDays('1')->format('Y-m-d')  . " 3:00 AM";
        } else {
            $tomorrow = today()->format('Y-m-d')  . " 9:00 PM";
        }
        $period = new CarbonPeriod($startTime, $addMinute.' minutes', new Carbon($tomorrow)); // for create use 24 hours format later change format 
        $allTimes = [];
        foreach($period as $item){
            array_push($allTimes, $item->format("h:i A"));
        }

        if($hotelTime == null) {
            if($today->toTimeString() > '21:00:00') {
                $data['timings'] = array();
            } else {
                $data['timings'] = $allTimes;
            }
        } else {
            $data['timings'] = $allTimes;
        }
        $data['hotel'] = $hotel;
        $data['carts'] = $carts;
        $data['userDetails'] = Auth()->user();
        return view('checkout', $data);
    }

    public function orderConfirm(Request $request)
    {
        $order = new Order();
        $order->name = $request->name;
        $order->email = $request->email;
        $order->phone_number = $request->phone;
        $order->delivery_address = $request->delivery_address;
        $order->delivery_note = $request->delivery_note;
        $order->delivery_time = $request->deliveryTime;
        $order->order_date = $request->order_date;
        $order->zip_code = $request->zip_code;
        $order->tag = $request->tag;

        $carts = session()->get('cart');
        $slug = '';
        $total = 0;
        $serviceCharge = 0;
        foreach($carts as $id => $details) {
            $slug = $details['cart_restaurant'];
            $total += $details['price'] * $details['quantity'];
            $itemDetails[] = json_encode($details);
        }

        $order->item_details = json_encode($carts);
        $hotel = Hotel::where('slug', $slug->slug)->first();
        if($hotel->service_charge != null) {
            if(is_numeric($hotel->service_charge)) {
                $serviceCharge += ($hotel->service_charge * $total) / 100;
            }
        }
        $totalAmount = $total + $serviceCharge;

        if ($totalAmount < 500){
            return response()->json(['data' => $totalAmount, 'msg' =>'failed', 'status' => 0]);
        }

        $order->hotel_name = $hotel->name;
        $order->hotel_id = $hotel->id;
        $order->final_amount = $totalAmount;
        // $order->discount_price = $hotel->discount_price;
        $order->save();
        // Do clear cart session
        $request->session()->forget('cart');

        $data['orderId'] = encrypt($order->order_id);
        return response()->json(['data' => $data, 'msg' =>'success', 'status' => 1]);
    }

    public function paymentConfirm($orderId)
    {
        $id = decrypt($orderId);
        $order = Order::findOrFail($id);
        // Get the amount using order id
        $data['totalAmount'] = $order->final_amount;
        $data['orderId'] = $id;
        return view('payment', $data);
    }

    public function paymentSuccess(Request $request)
    {
        $superadmiEmails = User::whereHas(
            'roles', function($q){
                $q->where('name', 'superadmin');
            }
        )->get();
        $order = Order::findOrFail($request->order_id);
        if ($request->order_type == 'cash') {
            $order->payment_mode = 'Cash';
            $order->order_status = 'confirmed';
            $order->save();
            $validate_email = filter_var($order->email,FILTER_VALIDATE_EMAIL);
            $toCustomer = Mail::to($validate_email)->send(new OrderBookingMail($order));
            foreach($superadmiEmails as $adMail){
                $adminMail = filter_var($adMail->email,FILTER_VALIDATE_EMAIL);
                $toAdmin = Mail::to($adminMail)->send(new AdminOrderMail($order));
            }
            if($toAdmin){
                return response()->json(['msg' =>'success', 'status' => 1]);
            } else {
                return response()->json(['msg' =>'success', 'status' => 1]);
            }
        }

        if ($request->order_type == 'khalti_online') {
            $args = http_build_query(array(
                        'token' => $request->token,
                        'amount'  => ($request->amount)
                    ));

            $url = "https://khalti.com/api/payment/verify/";

            # Make the call using API.
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS,$args);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            $headers = ['Authorization: Key '.env('KHALTI_SECRET_KEY', '')];
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            // Response
            $response = curl_exec($ch);
            $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);
            $token = json_decode($response, TRUE);
            
            if (isset($token['idx'])&& $status_code == 200) 
            {
                $order->payment_mode = 'Online';
                $order->payment_id = $request->paymentId;
                $order->payment_status = 'success';
                $order->order_status = 'confirmed';
                $order->save();
                $validate_email = filter_var($order->email,FILTER_VALIDATE_EMAIL);
                $toCustomer = Mail::to($validate_email)->send(new OrderBookingMail($order));
                foreach($superadmiEmails as $adMail){
                    $adminMail = filter_var($adMail->email,FILTER_VALIDATE_EMAIL);
                    $toAdmin = Mail::to($adminMail)->send(new AdminOrderMail($order));
                }
                if($toAdmin) {
                    return response()->json(['msg' =>'success', 'status' => 1]);
                } else {
                    return response()->json(['msg' =>'success', 'status' => 1]);
                }
            } else {
                return response()->json(['msg' =>'Payment verification failed, Please try again.', 'status' => 0]);
            }
        }
    }

    public function OrderThanks()
    {
        return view('thanks');
    }
}
