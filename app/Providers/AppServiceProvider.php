<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\City;
use App\Models\Cuisine;
use App\Models\Alert;
use Illuminate\Pagination\Paginator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(['site_includes.footer', 'site_includes.header'], function ($view) {
            $data['popularCities'] = City::where('papular_status', 1)->groupBy('name')->get();
            $data['cuisines'] = Cuisine::where('status', 1)->get();
            $data['alert'] = Alert::where('alert_status', 1)->first();
            $view->with($data); // bind data to view
        });
        Paginator::useBootstrap();
    }
}
