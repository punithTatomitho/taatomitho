<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderBookingMail extends Mailable
{
    use Queueable, SerializesModels;
    public $orderbookings;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($orderbookings)
    {
        $this->orderbookings = $orderbookings;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Mail from Taatomitho')->markdown('emails.orderBookMail')->with('orderbookings',$this->orderbookings);
    }
}
