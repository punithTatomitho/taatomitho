<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    use HasFactory;
    protected $fillable = [
        'category_id',
        'promotion_status',
        'title',
        'description',
        'sort_order',
        'icon_image',
        'bg_color',
        'text_color',
        'background_image',
        'button_text',
        'button_color',
        'redirect_to',
        'zones',
        'slider_status'
    ];
    public function category(){
        return $this->belongsTo(Category::class,'category_id');
    }
    
}
