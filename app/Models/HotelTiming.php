<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HotelTiming extends Model
{
    use HasFactory;
    protected $fillable = ['hotel_id','monday_start_at','monday_end_at','tuesday_start_at','tuesday_end_at','wednesday_start_at','wednesday_end_at',
        'thursday_start_at','thursday_end_at','friday_start_at','friday_end_at','saturday_start_at','saturday_end_at','sunday_start_at','sunday_end_at'];

        public function hotel(){
            return $this->belongsTo(Hotel::class,'hotel_id');
        }
}
