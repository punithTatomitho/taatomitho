<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HotelRegisterDetail extends Model
{
    use HasFactory;
    protected $fillable = ['hotel_id','commission','payout_frequency','registeration_charge','accept_orders_upto',
    'start_date','end_date','comments','offers_text','groccery_details','internal_store_details'];

    public function hotel(){
        return $this->belongsTo(Hotel::class,'hotel_id');
    }

}
