<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HotelShifTiming extends Model
{
    use HasFactory;
    protected $fillable = ['hotel_id','monday_shift_start_at','monday_shift_end_at','tuesday_shift_start_at','tuesday_shift_end_at','wednesday_shift_start_at','wednesday_shift_end_at',
        'thursday_shift_start_at','thursday_shift_end_at','friday_shift_start_at','friday_shift_end_at','saturday_shift_start_at','saturday_shift_end_at','sunday_shift_start_at','sunday_shift_end_at'];

        public function hotel(){
            return $this->belongsTo(Hotel::class,'hotel_id');
        }
}
