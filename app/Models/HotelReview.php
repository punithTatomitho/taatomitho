<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HotelReview extends Model
{
    use HasFactory;
    protected $fillable = ['title','rating','body','comments',
    'approved_status'];

    public function hotel(){
        return $this->belongsTo(Hotel::class,'hotel_id');
    }
}
