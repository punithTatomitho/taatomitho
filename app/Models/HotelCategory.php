<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HotelCategory extends Model
{
    use HasFactory;
    protected $table = 'hotel_categories';
    protected $fillable = ['name','sort_order','hotel_id','meta_tag',
    'status','description','primary_status'];

    public function hotel(){
        return $this->belongsTo(Hotel::class,'hotel_id');
    }
}

