<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class User_profile extends Model
{
    use HasFactory;
    protected $primaryKey = 'user_prifile_id';
    protected $fillable = [
        'user_id',
        'user_name',
        'phone_number',
        'user_address',
        'zip_code',
        'profile_image',
        'status'
    ];
}
