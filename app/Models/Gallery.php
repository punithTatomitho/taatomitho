<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    use HasFactory;
    protected $fillable = ['name','image','hotel_id','sort_order',
    'status'];

    public function hotel(){
        return $this->belongsTo(Hotel::class,'hotel_id');
    }
}
