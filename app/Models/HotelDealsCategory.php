<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HotelDealsCategory extends Model
{
    use HasFactory;
    protected $fillable = ['deal_id','category_id','quantity'];
    
    public function hoteldeals(){
        return $this->belongsTo(HotelDeal::class,'deal_id');
    }

    public function category(){
        return $this->belongsTo(HotelCategory::class,'category_id');
    }
}
