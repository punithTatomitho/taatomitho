<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Hotel extends Model
{
    use HasFactory;
    protected $fillable = ['name','sort_order','slug','tag','description','meta_tag','phone','email','contact_name',
    'contact_phone','hotel_banner_image','hotel_thumbnail_image','allergy_indication','facebook_id','twitter_id','range',
    'latitude','longitude','google_place_id','address','street','district','ward','zip_code','city','delivery_areas','min_order_amount',
    'delivery_radius_miles','delivery_charge','free_delivery_amount','vat','service_charge','payment_details','other_details',
    'accommodation_details','promotion_status','hotel_status','list_status','offers_status','accept_deliveries',
    'accept_collections','billing_address','city_id','cuisine_id','order_details','cuisines'];

    protected static function boot()
    {
        parent::boot();

        static::created(function ($post) {
            $post->slug = $post->generateSlug($post->name);
            $post->save();
        });
    }

    private function generateSlug($name)
    {
        if (static::whereSlug($slug = Str::slug($name))->exists()) {
            $max = static::whereName($name)->latest('id')->skip(1)->value('slug');
            if (isset($max[-1]) && is_numeric($max[-1])) {
                return preg_replace_callback('/(\d+)$/', function($mathces) {
                    return $mathces[1] + 1;
                }, $max);
            }
            return "{$slug}-1";
        }

        return $slug;

    }

    public function timings()
    {
        return $this->hasOne('App\Models\HotelTiming', 'hotel_id');
    }

    public function hotel_registerdetails()
    {
        return $this->hasOne(HotelRegisterDetail::class,'hotel_id');
    }
    
    public function hotel_items()
    {
        return $this->hasOne(HotelItem::class,'hotel_id');
    }

    public function hotel_category(){
        return $this->hasOne(HotelCategory::class,'hotel_id');
    }

    public function city(){
        return $this->hasOne(City::class,'city_id');
    }
    
    public function galleries(){
        return $this->hasOne(Gallery::class,'hotel_id');
    }
}