<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HotelDeal extends Model
{
    use HasFactory;
    protected $fillable = ['name','sort_order','hotel_id',
    'status','description','promotion_status','image','tax','price'];

    public function hotel(){
        return $this->belongsTo(Hotel::class,'hotel_id');
    }
}
