<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HotelItem extends Model
{
    use HasFactory;
    protected $table = 'hotel_items';
    protected $fillable = ['name','sort_order','hotel_id',
    'status','description','image','spice_level','vegetarian','low_fat','calories','new','best_seller','segments','addons',
    'item_price','item_oldprice','sku_identifier','category_id','promote_item','variants','tax','thumbnail_image'];

    public function hotel(){
        return $this->belongsTo(Hotel::class,'hotel_id');
    }
    public function category(){
        return $this->belongsTo(HotelCategory::class,'category_id');
    }
}
