<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HotelBill extends Model
{
    use HasFactory;
    protected $primaryKey = 'hotel_bill_id';
}
