<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities', function (Blueprint $table) {
            $table->id();
            $table->string('name',150);
            $table->string("slug", 150);
            $table->integer('sort_order')->nullable();
            $table->string('image')->nullable();
            $table->longText('meta_tag')->nullable();
            $table->tinyInteger('status')->comments('1:Active;0:In-Active')->nullable();
            $table->tinyInteger('papular_status')->comments('1:popular;0:Not-Papular')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cities');
    }
}
