<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHotelRegisterDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotel_register_details', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('hotel_id')->unsigned()->nullable();
            $table->string('commission')->nullable();
            $table->string('payout_frequency')->nullable();
            $table->string('registeration_charge')->nullable();
            $table->string('accept_orders_upto')->nullable();
            $table->string('start_date')->nullable();
            $table->string('end_date')->nullable();
            $table->string('comments')->nullable();
            $table->string('offers_text')->nullable();
            $table->json('groccery_details')->nullable();
            $table->json('internal_store_details')->nullable();
            $table->timestamps();
            $table->foreign('hotel_id')->references('id')->on('hotels')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotel_register_details');
    }
}
