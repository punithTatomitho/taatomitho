<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHotelCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();
            $table->string('name',150);
            $table->integer('sort_order')->nullable();
            $table->bigInteger('hotel_id')->unsigned()->nullable();
            $table->longText('description')->nullable();
            $table->longText('meta_tag')->nullable();
            $table->tinyInteger('status')->comments('1:Active;0:In-Active')->nullable();
            $table->tinyInteger('primary_status')->comments('1:Primary;2:Secondary')->nullable();
            $table->timestamps();
            $table->foreign('hotel_id')->references('id')->on('hotels')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
