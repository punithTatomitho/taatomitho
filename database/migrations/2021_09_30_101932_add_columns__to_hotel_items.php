<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToHotelItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hotel_items', function (Blueprint $table) {
            $table->string('thumbnail_image')->after('image')->nullable();
            $table->string('variants')->after('thumbnail_image')->nullable();
            $table->integer('tax')->after('item_oldprice')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hotel_items', function (Blueprint $table) {
            //
        });
    }
}
