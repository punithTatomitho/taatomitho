<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHotelShifTimingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotel_shif_timings', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('hotel_id')->unsigned()->nullable();
             $table->time('monday_shift_start_at')->nullable();
             $table->time('monday_shift_end_at')->nullable();
             $table->time('tuesday_shift_start_at')->nullable();
             $table->time('tuesday_shift_end_at')->nullable();
             $table->time('wednesday_shift_start_at')->nullable();
             $table->time('wednesday_shift_end_at')->nullable();
             $table->time('thursday_shift_start_at')->nullable();
             $table->time('thursday_shift_end_at')->nullable();
             $table->time('friday_shift_start_at')->nullable();
             $table->time('friday_shift_end_at')->nullable();
             $table->time('saturday_shift_start_at')->nullable();
             $table->time('saturday_shift_end_at')->nullable();
             $table->time('sunday_shift_start_at')->nullable();
             $table->time('sunday_shift_end_at')->nullable();
            $table->timestamps();
            $table->foreign('hotel_id')->references('id')->on('hotels')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotel_shif_timings');
    }
}
