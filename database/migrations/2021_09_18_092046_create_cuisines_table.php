<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCuisinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuisines', function (Blueprint $table) {
            $table->id();
            $table->string('name',150);
            $table->string("slug", 150);
            $table->integer('sort_order')->nullable();
            $table->bigInteger('cuisinetype_id')->unsigned()->nullable();
            $table->longText('description')->nullable();
            $table->string('image')->nullable();
            $table->longText('meta_tag')->nullable();
            $table->tinyInteger('status')->comments('1:Active;0:In-Active')->nullable();
            $table->timestamps();
            $table->foreign('cuisinetype_id')->references('id')->on('cuisine_types')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cuisines');
    }
}
