<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGalleriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('galleries', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('hotel_id')->unsigned()->nullable();
            $table->integer('sort_order')->nullable();
            $table->string('image')->nullable();
            $table->tinyInteger('status')->comments('1:Active;0:In-Active')->nullable();
            $table->timestamps();
            $table->foreign('hotel_id')->references('id')->on('hotels')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('galleries');
    }
}
