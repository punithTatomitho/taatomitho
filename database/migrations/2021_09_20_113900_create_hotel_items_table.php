<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHotelItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->id();
            $table->string('name',150);
            $table->integer('sort_order')->nullable();
            $table->bigInteger('hotel_id')->unsigned()->nullable();
            $table->bigInteger('category_id')->unsigned()->nullable();
            $table->longText('description')->nullable();
            $table->string('image')->nullable();
            $table->string('spice_level')->nullable();
            $table->string('vegetarian')->nullable();
            $table->string('low_fat')->nullable();
            $table->string('calories')->nullable();
            $table->string('new')->nullable();
            $table->string('promote_item')->nullable();
            $table->string('best_seller')->nullable();
            $table->json('segments')->nullable();
            $table->string('addons')->nullable();
            $table->string('item_price')->nullable();
            $table->string('item_oldprice')->nullable();
            $table->string('sku_identifier')->nullable();
            $table->longText('meta_tag')->nullable();
            $table->tinyInteger('status')->comments('1:Active;0:In-Active')->nullable();
            $table->timestamps();
            $table->foreign('hotel_id')->references('id')->on('hotels')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->foreign('category_id')->references('id')->on('categories')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
