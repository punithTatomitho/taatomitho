<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHotelReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotel_reviews', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('hotel_id')->unsigned()->nullable();
            $table->string('title')->nullable();
            $table->integer('rating')->nullable();
            $table->longText('body')->nullable();
            $table->longText('comments')->nullable();
            $table->tinyInteger('approved_status')->comments('1:Approved;0:Not-Approved')->nullable();
            $table->timestamps();
            $table->foreign('hotel_id')->references('id')->on('hotels')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotel_reviews');
    }
}
