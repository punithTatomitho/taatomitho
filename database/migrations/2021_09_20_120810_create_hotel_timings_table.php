<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHotelTimingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotel_timings', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('hotel_id')->unsigned()->nullable();
             $table->time('monday_start_at')->nullable();
             $table->time('monday_end_at')->nullable();
             $table->time('tuesday_start_at')->nullable();
             $table->time('tuesday_end_at')->nullable();
             $table->time('wednesday_start_at')->nullable();
             $table->time('wednesday_end_at')->nullable();
             $table->time('thursday_start_at')->nullable();
             $table->time('thursday_end_at')->nullable();
             $table->time('friday_start_at')->nullable();
             $table->time('friday_end_at')->nullable();
             $table->time('saturday_start_at')->nullable();
             $table->time('saturday_end_at')->nullable();
             $table->time('sunday_start_at')->nullable();
             $table->time('sunday_end_at')->nullable();
            $table->timestamps();
            $table->foreign('hotel_id')->references('id')->on('hotels')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotel_timings');
    }
}
