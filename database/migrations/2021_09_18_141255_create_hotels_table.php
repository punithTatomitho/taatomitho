<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHotelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotels', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('slug');
            $table->string('tag')->nullable();;
            $table->bigInteger('cuisine_id')->unsigned()->nullable();
            $table->longText('description')->nullable();
            $table->longText('meta_tag')->nullable();
            $table->longText('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('contact_name')->nullable();
            $table->string('contact_phone')->nullable();
            $table->string('hotel_banner_image')->nullable();
            $table->string('hotel_thumbnail_image')->nullable();
            $table->longText('allergy_indication')->nullable();
            $table->string('facebook_id')->nullable();
            $table->string('twitter_id')->nullable();
            $table->string('range')->nullable();
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->string('google_place_id')->nullable();
            $table->longText('address')->nullable();
            $table->string('street')->nullable();
            $table->string('district')->nullable();
            $table->string('ward')->nullable();
            $table->string('zip_code')->nullable();
            $table->string('city')->nullable();
            $table->string('delivery_areas')->nullable();
            $table->string('min_order_amount')->nullable();
            $table->string('delivery_radius_miles')->nullable();
            $table->string('delivery_charge')->nullable();
            $table->string('free_delivery_amount')->nullable();
            $table->string('vat')->nullable();
            $table->string('service_charge')->nullable();
            $table->json('payment_details')->nullable();
            $table->json('other_details')->nullable();
            $table->json('order_details')->nullable();
            $table->json('accommodation_details')->nullable();
            $table->string('accept_deliveries')->nullable();
            $table->string('accept_collections')->nullable();
            $table->string('billing_address')->nullable();
            $table->tinyInteger('promotion_status')->comments('1:Promotion_On;0:Promotion_Off')->nullable();
            $table->tinyInteger('hotel_status')->comments('1:Hotel_Open;0:Hotel_Close')->nullable();
            $table->tinyInteger('list_status')->comments('1:Listed;0:Not_Listed')->nullable();
            $table->tinyInteger('offers_status')->comments('1:Offers;0:No_Offers')->nullable();
            $table->bigInteger('city_id')->unsigned()->nullable();
            $table->timestamps();
            $table->foreign('city_id')->references('id')->on('cities')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->foreign('cuisine_id')->references('id')->on('cuisines')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotels');
    }
}
