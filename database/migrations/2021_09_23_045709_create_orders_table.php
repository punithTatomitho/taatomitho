<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('order_id');
            $table->string('name', 100);
            $table->string('email', 100);
            $table->string('phone_number', 50);
            $table->longText('delivery_address');
            $table->longText('delivery_instruction')->nullable();
            $table->string('zip_code', 100)->nullable();
            $table->string('tag', 100)->nullable();
            $table->string('delivery_note')->nullable();
            $table->string('delivery_time', 50);
            $table->date('order_date');
            $table->longText('item_details');
            $table->string('hotel_name', 200);
            $table->integer('hotel_id');
            $table->string('currency_type', 10)->nullable();
            $table->integer('final_amount');
            $table->integer('discount_price')->nullable();
            $table->string('payment_mode', 50)->nullable();
            $table->string('payment_id', 100)->nullable();
            $table->string('payment_status', 100)->nullable();
            $table->string('order_status', 100)->nullable();
            $table->string('delivery_status', 100)->nullable();
            $table->date('delivery_date')->nullable();
            $table->string('rider_name', 150)->nullable();
            $table->string('csr_name', 150)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
