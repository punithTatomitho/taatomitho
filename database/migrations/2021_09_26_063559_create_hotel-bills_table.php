<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHotelBillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotel_bills', function (Blueprint $table) {
            $table->bigIncrements('hotel_bill_id');
            $table->integer('hotel_id');
            $table->string('hotel_slug', 100);
            $table->string('hotel_name', 100);
            $table->date('bill_from_date');
            $table->date('bill_to_date');
            $table->integer('total_amount');
            $table->integer('commission');
            $table->integer('commission_amount');
            $table->integer('pay_out');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotel_bills');
    }
}
