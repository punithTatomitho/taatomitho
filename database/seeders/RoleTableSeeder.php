<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = Role::create([
            'name' => 'admin',
            'display_name' => 'Admin', // optional
            'description' => 'User is the owner of a given project', // optional
        ]);

        $users = Role::create([
            'name' => 'user',
            'display_name' => 'User ', // optional
            'description' => 'User is allowed to manage and edit other users', // optional
        ]);
    }
}
