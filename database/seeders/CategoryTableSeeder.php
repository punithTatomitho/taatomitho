<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Category;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = Category::create([
            'category_name' => 'Restaurants'
        ]);

        $users = Category::create([
            'category_name' => 'Groceries'
        ]);
    }
}
