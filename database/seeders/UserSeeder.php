<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Role;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminRole = Role::whereName('admin')->first();
        $userRole = Role::whereName('user')->first();

        $user = User::create(array(
            'name' => 'Admin',
            'email' => 'admin@tatomitho.com',
            'password'    => Hash::make('Tatomitho@21!'),
        ));
        $user->attachRole($adminRole);

        $user = User::create(array(
            'name'      => 'Shashi',
            'email'     => 'Shashi.ga248@gmail.com',
            'password'  => Hash::make('12345678'),

            //'activated'     => true
        ));
        $user->attachRole($userRole);
    }
}
