<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\CuisineType;

class CuisinesTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = CuisineType::create([
            'type' => 'Food'
        ]);

        $users = CuisineType::create([
            'type' => 'Groceries'
        ]);
    }
}
