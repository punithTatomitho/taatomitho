<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

'facebook' => [
        'client_id' => '613600249811667',
        'client_secret' => '949b07d03eb9f36f9cb04ab4bd39a3c3',
        'redirect' => 'https://taatomitho.com/auth/facebook/callback',
    ],

    'google' => [
        'client_id' => '569464570059-vrbjmbhfu4o5l7hv2qm4phfl40mlr1m2.apps.googleusercontent.com', //USE FROM Google DEVELOPER ACCOUNT
        'client_secret' => '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', //USE FROM Google DEVELOPER ACCOUNT
        'redirect' => 'https://taatomitho.com/google/callback/'
],

];
